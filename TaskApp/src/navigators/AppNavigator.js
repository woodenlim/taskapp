import {
  createBottomTabNavigator,
  createAppContainer,
  createStackNavigator
} from 'react-navigation';
import { View, Text, Platform, SafeAreaView } from 'react-native'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Task from '@Components/Task';
import Member from '@Components/Member';
import SharedTask from '@Components/SharedTask';
import NewMember from '@Components/NewMember';
import React from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '@Utils/scalingUtils';

// const TaskNavigator = createStackNavigator({
//   Task: Task
// }, {
//   defaultNavigationOptions: ({ navigation }) => ({
//     headerTitle: 'All Task'
//   })
// })
//
// const MemberNavigator = createStackNavigator({
//   Member: Member
// })

const TabNavigator = createBottomTabNavigator({
  TaskTab: Task,
  MemberTab: Member
},{
  initialRouteName: 'TaskTab',
  defaultNavigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
      const { routeName } = navigation.state;
      let iconName;
      let color;
      switch(routeName){
        case "TaskTab":
          iconName = "tasks";
          color = focused ? "#70a1ff" : "";
        break;
        case "MemberTab":
          iconName = "users";
          color = focused ? "#70a1ff" : "";
        break;
      }
      // You can return any component that you like here! We usually use an
      // icon component from react-native-vector-icons
      return (<FontAwesome5 name={iconName} color={tintColor} size={hp(3)}/>)
    },
  }),
  tabBarOptions: {
    activeTintColor: '#7B1FA2',
    inactiveTintColor: '#BDBDBD',
    showLabel: false,
    style:{
      backgroundColor: '#EEEEEE',
      borderTopColor: 'transparent',
      elevation: 15
    }
  }
})

const AppNavigator = createStackNavigator({
  Home: TabNavigator,
  SharedTask: SharedTask,
  NewMember: NewMember
}, {
  defaultNavigationOptions: ({navigation}) => ({
    title: 'TaskApp'
  }),
  headerBackTitleVisible: false
  // header: null,
  // headerMode: 'none',
})

export default createAppContainer(AppNavigator)
