export const getDayName = (type, index) => {
  fullDayNameList = [  "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  shortDayNameList = [ "Sun","Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  return type == "full" ? fullDayNameList[index] : shortDayNameList[index];
}

export const getMonthName = (type, index) => {
  fullMonthNameList = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
  shortMonthNameList = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
  return type == "full" ? fullMonthNameList[index] : shortMonthNameList[index];
}

export const getMonthInTwoDigit = (month) => {
  return month < 10 ? '0' + month : '' + month; // ('' + month) for string result
}

export const getDateInTwoDigit = (date) => {
  return date < 10 ? '0' + date : '' + date; // ('' + month) for string result
}

export const getDateOrdinal = (date) => {
  if(date > 3 && date < 21) return 'th';
  switch(date % 10) {
    case 1:  return "st";
    case 2:  return "nd";
    case 3:  return "rd";
    default: return "th";
  }
}
