import axios from 'axios';

export const getMethod = (apiUrl, params) => {
  console.log(apiUrl + params);
  return axios.get(apiUrl + params, {
      // headers: {
      //   'Authorization': accessToken
      // }
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return error.response
    });
}


export const postMethod = (apiUrl, body) => {
  let postMethod = axios.post(apiUrl, body);
  return postMethod.then((response) => {
    console.log(response);
        return response.data;
      }).catch((error) => {
          console.log(error);
          return error
        });
}


export const deleteMethod = (apiUrl, body) => {
  return axios.delete(
        apiUrl,
        {
          data: body,
          // headers: {
          //   'Authorization': accessToken
          // }
        }).then((response) => {
        //console.log(response);
        return response.data;
      }).catch((error) => {
          console.log(error.response);
          return error.response
        });
}

export const putMethod = (apiUrl, body) => {
  return axios.put(
        apiUrl, body, {
          // headers: {
          //   'Authorization': accessToken
          // }
        }).then((response) => {
        //console.log(response);
        return response.data;
      }).catch((error) => {
          console.log(JSON.parse(JSON.stringify(error)));
          return error
        });
}
