import * as actionTypes from '@Actions/actionTypes';

export function memberListRequest(data) {
  return {
    type: actionTypes.MEMBER_LIST.REQUEST,
    data
  }
}

export function memberListSuccess(response) {
  return {
    type: actionTypes.MEMBER_LIST.SUCCESS,
    response
  }
}

export function memberListFail(error) {
  return {
    type: actionTypes.MEMBER_LIST.FAILED,
    error
  }
}
