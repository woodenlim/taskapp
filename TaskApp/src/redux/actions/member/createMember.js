import * as actionTypes from '@Actions/actionTypes';

export function createMemberRequest(data) {
  return {
    type: actionTypes.CREATE_MEMBER.REQUEST,
    data
  }
}

export function createMemberSuccess(response) {
  return {
    type: actionTypes.CREATE_MEMBER.SUCCESS,
    response
  }
}

export function createMemberFail(error) {
  return {
    type: actionTypes.CREATE_MEMBER.FAILED,
    error
  }
}
