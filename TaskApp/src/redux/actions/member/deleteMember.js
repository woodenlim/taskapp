import * as actionTypes from '@Actions/actionTypes';

export function deleteMemberRequest(data) {
  return {
    type: actionTypes.DELETE_MEMBER.REQUEST,
    data
  }
}

export function deleteMemberSuccess(response) {
  return {
    type: actionTypes.DELETE_MEMBER.SUCCESS,
    response
  }
}

export function deleteMemberFail(error) {
  return {
    type: actionTypes.DELETE_MEMBER.FAILED,
    error
  }
}
