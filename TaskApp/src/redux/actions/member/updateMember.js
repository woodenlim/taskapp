import * as actionTypes from '@Actions/actionTypes';

export function updateMemberRequest(data) {
  return {
    type: actionTypes.UPDATE_MEMBER.REQUEST,
    data
  }
}

export function updateMemberSuccess(response) {
  return {
    type: actionTypes.UPDATE_MEMBER.SUCCESS,
    response
  }
}

export function updateMemberFail(error) {
  return {
    type: actionTypes.UPDATE_MEMBER.FAILED,
    error
  }
}
