import * as actionTypes from '@Actions/actionTypes';

export function updateTaskRequest(data) {
  return {
    type: actionTypes.UPDATE_TASK.REQUEST,
    data
  }
}

export function updateTaskSuccess(response) {
  return {
    type: actionTypes.UPDATE_TASK.SUCCESS,
    response
  }
}

export function updateTaskFail(error) {
  return {
    type: actionTypes.UPDATE_TASK.FAILED,
    error
  }
}
