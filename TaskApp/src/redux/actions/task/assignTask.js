import * as actionTypes from '@Actions/actionTypes';

export function assignTaskRequest(data) {
  return {
    type: actionTypes.ASSIGN_TASK.REQUEST,
    data
  }
}

export function assignTaskSuccess(response) {
  return {
    type: actionTypes.ASSIGN_TASK.SUCCESS,
    response
  }
}

export function assignTaskFail(error) {
  return {
    type: actionTypes.ASSIGN_TASK.FAILED,
    error
  }
}
