import * as actionTypes from '@Actions/actionTypes';

export function taskListRequest(data) {
  return {
    type: actionTypes.TASK_LIST.REQUEST,
    data
  }
}

export function taskListSuccess(response) {
  return {
    type: actionTypes.TASK_LIST.SUCCESS,
    response
  }
}

export function taskListFail(error) {
  return {
    type: actionTypes.TASK_LIST.FAILED,
    error
  }
}
