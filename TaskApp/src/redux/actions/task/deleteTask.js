import * as actionTypes from '@Actions/actionTypes';

export function deleteTaskRequest(data) {
  return {
    type: actionTypes.DELETE_TASK.REQUEST,
    data
  }
}

export function deleteTaskSuccess(response) {
  return {
    type: actionTypes.DELETE_TASK.SUCCESS,
    response
  }
}

export function deleteTaskFail(error) {
  return {
    type: actionTypes.DELETE_TASK.FAILED,
    error
  }
}
