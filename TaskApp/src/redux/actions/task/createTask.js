import * as actionTypes from '@Actions/actionTypes';

export function createTaskRequest(data) {
  return {
    type: actionTypes.CREATE_TASK.REQUEST,
    data
  }
}

export function createTaskSuccess(response) {
  return {
    type: actionTypes.CREATE_TASK.SUCCESS,
    response
  }
}

export function createTaskFail(error) {
  return {
    type: actionTypes.CREATE_TASK.FAILED,
    error
  }
}
