const REQUEST = 'REQUEST';
const SUCCESS = 'SUCCESS';
const FAILED = 'FAILED';
const CREATE = 'CREATE';
const READ = 'READ';
const UPDATE = 'UPDATE';
const DELETE = 'DELETE';

function createRequestTypes(base) {
  const res = {};
  [REQUEST, SUCCESS, FAILED, CREATE, READ, UPDATE, DELETE].forEach(type => res[type] = `${base}_${type}`);
  return res;
}

export const CREATE_TASK = createRequestTypes('CREATE_TASK');
export const UPDATE_TASK = createRequestTypes('UPDATE_TASK');
export const DELETE_TASK = createRequestTypes('DELETE_TASK');
export const TASK_LIST = createRequestTypes('TASK_LIST');
export const CREATE_MEMBER = createRequestTypes('CREATE_MEMBER');
export const UPDATE_MEMBER = createRequestTypes('UPDATE_MEMBER');
export const DELETE_MEMBER = createRequestTypes('DELETE_MEMBER');
export const MEMBER_LIST = createRequestTypes('MEMBER_LIST');
export const ASSIGN_TASK = createRequestTypes('ASSIGN_TASK');

export const INITIAL_STATE = {
  message: "",
  status: "",
  response: {
    data:[]
  }
};
