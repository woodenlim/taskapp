import {
  call,
  put,
  fork,
  take
} from 'redux-saga/effects'
import {
  createTaskSuccess,
  createTaskFail
} from '@TaskActions/createTask';
import * as actionTypes from '@Actions/actionTypes';
import { postMethod } from '@Utils/apiUtils'
import { API_URL } from '@Constants/apiUrl';

export function* createTask() {
  while (true) {
    const { data } = yield take(actionTypes.CREATE_TASK.REQUEST);
    try {
      const responseData = yield call(postMethod, API_URL.TASK, data);
      console.log('create task saga');
      console.log(responseData);
      if(responseData.status == "success"){
        yield put(createTaskSuccess(responseData));
      }else{
        yield put(createTaskFail(responseData));
      }

    } catch (error) {
      yield put(createTaskFail(error));
    }
  }
}

export default function* root() {
  yield fork(createTask);
}
