import {
  call,
  put,
  fork,
  take
} from 'redux-saga/effects'
import {
  deleteTaskSuccess,
  deleteTaskFail
} from '@TaskActions/deleteTask';
import * as actionTypes from '@Actions/actionTypes';
import { deleteMethod } from '@Utils/apiUtils'
import {API_URL} from '@Constants/apiUrl';

export function* deleteTask() {
  while (true) {
    const { data } = yield take(actionTypes.DELETE_TASK.REQUEST);
    try {
      const responseData = yield call(deleteMethod, API_URL.DELETE_TASK(data.id), data);

      if(responseData.status == "success"){
        yield put(deleteTaskSuccess(responseData));
      }else{
        yield put(deleteTaskFail(responseData));
      }

    } catch (error) {
      yield put(deleteTaskFail(error));
    }
  }
}

export default function* root() {
  yield fork(deleteTask);
}
