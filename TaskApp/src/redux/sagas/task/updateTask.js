import {
  call,
  put,
  fork,
  take
} from 'redux-saga/effects'
import {
  updateTaskSuccess,
  updateTaskFail
} from '@TaskActions/updateTask';
import * as actionTypes from '@Actions/actionTypes';
import { putMethod } from '@Utils/apiUtils'
import {API_URL} from '@Constants/apiUrl';

export function* updateTask() {
  while (true) {
    const { data } = yield take(actionTypes.UPDATE_TASK.REQUEST);
    try {
      const responseData = yield call(putMethod, API_URL.UPDATE_TASK(data.id), data);
      console.log(responseData);
      if(responseData.status == "success"){
        yield put(updateTaskSuccess(responseData));
      }else{
        yield put(updateTaskFail(responseData));
      }

    } catch (error) {
      yield put(updateTaskFail(error));
    }
  }
}

export default function* root() {
  yield fork(updateTask);
}
