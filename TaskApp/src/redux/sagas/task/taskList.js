import {
  call,
  put,
  fork,
  take
} from 'redux-saga/effects'
import {
  taskListSuccess,
  taskListFail
} from '@TaskActions/taskList';
import * as actionTypes from '@Actions/actionTypes';
import { getMethod } from '@Utils/apiUtils'
import { API_URL } from '@Constants/apiUrl';

export function* taskList() {
  while (true) {
    const { data } = yield take(actionTypes.TASK_LIST.REQUEST);
    try {
      const responseData = yield call(getMethod, API_URL.TASK, '');
      console.log(responseData);
      if(responseData.status == "success"){
        yield put(taskListSuccess(responseData));
      }else{
        yield put(taskListFail(responseData));
      }

    } catch (error) {
      yield put(taskListFail(error));
    }
  }
}

export default function* root() {
  yield fork(taskList);
}
