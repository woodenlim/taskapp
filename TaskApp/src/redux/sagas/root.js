import createMember from '@MemberSagas/createMember';
import updateMember from '@MemberSagas/updateMember';
import deleteMember from '@MemberSagas/deleteMember';
import memberList from '@MemberSagas/memberList';

import taskList from '@TaskSagas/taskList';
import createTask from '@TaskSagas/createTask';
import deleteTask from '@TaskSagas/deleteTask';
import updateTask from '@TaskSagas/updateTask';

import { all } from 'redux-saga/effects';

export default function* rootSaga() {
  yield all([
    createMember(),
    updateMember(),
    deleteMember(),
    memberList(),
    taskList(),
    createTask(),
    deleteTask(),
    updateTask()
  ]);
}
