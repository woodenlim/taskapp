import {
  call,
  put,
  fork,
  take
} from 'redux-saga/effects'
import {
  deleteMemberSuccess,
  deleteMemberFail
} from '@MemberActions/deleteMember';
import * as actionTypes from '@Actions/actionTypes';
import { deleteMethod } from '@Utils/apiUtils'
import {API_URL} from '@Constants/apiUrl';

export function* deleteMember() {
  while (true) {
    const { data } = yield take(actionTypes.DELETE_MEMBER.REQUEST);
    try {
      const responseData = yield call(deleteMethod, API_URL.DELETE_MEMBER(data.id), data);

      if(responseData.status == "success"){
        yield put(deleteMemberSuccess(responseData));
      }else{
        yield put(deleteMemberFail(responseData));
      }

    } catch (error) {
      yield put(deleteMemberFail(error));
    }
  }
}

export default function* root() {
  yield fork(deleteMember);
}
