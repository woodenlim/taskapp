import {
  call,
  put,
  fork,
  take
} from 'redux-saga/effects'
import {
  createMemberSuccess,
  createMemberFail
} from '@MemberActions/createMember';
import * as actionTypes from '@Actions/actionTypes';
import { postMethod } from '@Utils/apiUtils'
import { API_URL } from '@Constants/apiUrl';

export function* createMember() {
  while (true) {
    const { data } = yield take(actionTypes.CREATE_MEMBER.REQUEST);
    try {
      const responseData = yield call(postMethod, API_URL.MEMBER, data);
      if(responseData.status == "success"){
        yield put(createMemberSuccess(responseData));
      }else{
        yield put(createMemberFail(responseData));
      }

    } catch (error) {
      yield put(createMemberFail(error));
    }
  }
}

export default function* root() {
  yield fork(createMember);
}
