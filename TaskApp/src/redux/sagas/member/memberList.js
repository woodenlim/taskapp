import {
  call,
  put,
  fork,
  take
} from 'redux-saga/effects'
import {
  memberListSuccess,
  memberListFail
} from '@MemberActions/memberList';
import * as actionTypes from '@Actions/actionTypes';
import { getMethod } from '@Utils/apiUtils'
import { API_URL } from '@Constants/apiUrl';

export function* memberList() {
  while (true) {
    const { data } = yield take(actionTypes.MEMBER_LIST.REQUEST);
    try {
      const responseData = yield call(getMethod, API_URL.MEMBER, '');
      if(responseData.status == "success"){
        yield put(memberListSuccess(responseData));
      }else{
        yield put(memberListFail(responseData));
      }

    } catch (error) {
      yield put(memberListFail(error));
    }
  }
}

export default function* root() {
  yield fork(memberList);
}
