import {
  call,
  put,
  fork,
  take
} from 'redux-saga/effects'
import {
  updateMemberSuccess,
  updateMemberFail
} from '@MemberActions/updateMember';
import * as actionTypes from '@Actions/actionTypes';
import { putMethod } from '@Utils/apiUtils'
import {API_URL} from '@Constants/apiUrl';

export function* updateMember() {
  while (true) {
    const { data } = yield take(actionTypes.UPDATE_MEMBER.REQUEST);
    try {
      const responseData = yield call(putMethod, API_URL.UPDATE_MEMBER(data.id), data);

      if(responseData.status == "success"){
        yield put(updateMemberSuccess(responseData));
      }else{
        yield put(updateMemberFail(responseData));
      }

    } catch (error) {
      yield put(updateMemberFail(error));
    }
  }
}

export default function* root() {
  yield fork(updateMember);
}
