import { combineReducers } from 'redux'

import createMember from '@MemberReducers/createMember';
import updateMember from '@MemberReducers/updateMember';
import deleteMember from '@MemberReducers/deleteMember';
import memberList from '@MemberReducers/memberList';

import createTask from '@TaskReducers/createTask';
import updateTask from '@TaskReducers/updateTask';
import deleteTask from '@TaskReducers/deleteTask';
import taskList from '@TaskReducers/taskList';
import assignTask from '@TaskReducers/assignTask';

const reducers = {
  createMember, updateMember, deleteMember, memberList,
  createTask, updateTask, deleteTask, taskList, assignTask
}

const appReducer = combineReducers(reducers);

export default rootReducer = (state, action) => {
  return appReducer(state, action)
}
