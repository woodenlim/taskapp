import * as actionTypes from '@Actions/actionTypes';

const initialState = actionTypes.INITIAL_STATE

export default memberList = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.MEMBER_LIST.REQUEST:
      return {
        ...state
      }
    case actionTypes.MEMBER_LIST.SUCCESS:
      return {
        ...state,
        message: action.response.message,
        status: action.response.status,
        response: {
          data: action.response.response.data
        }
      }
    case actionTypes.MEMBER_LIST.FAILED:
      return {
        ...state,
        message: action.error.message,
        status: action.error.status,
        response: {data:action.error.response.data}
      }
    default:
      return state;
  }
}
