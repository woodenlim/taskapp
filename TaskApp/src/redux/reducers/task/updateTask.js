import * as actionTypes from '@Actions/actionTypes';

const initialState = actionTypes.INITIAL_STATE

export default updateTask = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.UPDATE_TASK.REQUEST:
      return {
        ...state
      }
    case actionTypes.UPDATE_TASK.SUCCESS:
      return {
        ...state,
        message: action.response.message,
        status: action.response.status,
        response: { data: action.response.response.data }
      }
    case actionTypes.UPDATE_TASK.FAILED:
      return {
        ...state,
        message: action.error.message,
        status: action.error.status,
        response: {data:action.error.response.data}
      }
    default:
      return initialState;
  }
}
