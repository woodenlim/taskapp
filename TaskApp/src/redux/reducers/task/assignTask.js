import * as actionTypes from '@Actions/actionTypes';

const initialState = actionTypes.INITIAL_STATE

export default assignTask = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ASSIGN_TASK.REQUEST:
      return {
        ...state
      }
    case actionTypes.ASSIGN_TASK.SUCCESS:
      return {
        ...state,
        message: action.response.message,
        status: action.response.status,
        response: { data: action.response.response.data }
      }
    case actionTypes.ASSIGN_TASK.FAILED:
      return {
        ...state,
        message: action.error.message,
        status: action.error.status,
        response: {data:action.error.response.data}
      }
    default:
      return initialState;
  }
}
