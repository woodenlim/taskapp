import * as actionTypes from '@Actions/actionTypes';

const initialState = actionTypes.INITIAL_STATE

export default taskList = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.TASK_LIST.REQUEST:
      return {
        ...state
      }
    case actionTypes.TASK_LIST.SUCCESS:
      return {
        ...state,
        message: action.response.message,
        status: action.response.status,
        response: {
          data: action.response.response.data
        }
      }
    case actionTypes.TASK_LIST.FAILED:
      return {
        ...state,
        message: action.error.message,
        status: action.error.status,
        response: {data:action.error.response.data}
      }
    default:
      return state;
  }
}
