import {compose, applyMiddleware, createStore} from 'redux';
import {createLogger} from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import devTools from 'remote-redux-devtools';
import rootReducer from '@Reducers/root';
import rootSaga from '@Sagas/root';
import { persistReducer, persistStore } from 'redux-persist'
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web and AsyncStorage for react-native

const isDebuggingInChrome = __DEV__ && !!window.navigator.userAgent;

const logger = createLogger({
  predicate: (getState, action) => isDebuggingInChrome,
  collapsed: true,
  duration: true,
  diff: true,
});

// const navigationMiddleware = createReactNavigationReduxMiddleware(
//   "root",
//   state => state.nav,
// );


const persistConfig = {
  key: 'root',
  storage,
  //whitelist: ['']
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

const sagaMiddleware = createSagaMiddleware();

const middlewares = [
  sagaMiddleware, logger
]

const enhancers = [
  applyMiddleware(...middlewares),
  devTools()
]

// const store = createStore(persistedReducer, undefined, compose(...enhancers))
// sagaMiddleware.run(rootSaga);

export default () => {
  if (!__DEV__) {
    console.log = () => {};
  }
  let store = createStore(persistedReducer, undefined, compose(...enhancers))
  let persistor = persistStore(store, null, ()=>{/**do Something after REHYRDATE**/ })
  sagaMiddleware.run(rootSaga);
  return { store, persistor }
}
