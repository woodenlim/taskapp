import { Dimensions } from 'react-native';

export const DEVICE_WIDTH = Dimensions.get('window').width
export const DEVICE_HEIGHT = Dimensions.get('window').height
export const SHORT_DATE_NAME = "short";
export const FULL_DATE_NAME = "full";
