var BASE_URL = 'http://localhost:5000'

export const API_URL = {
  TASK: BASE_URL + '/task',
  MEMBER: BASE_URL + '/member',
  UPDATE_TASK: (taskId) => BASE_URL + '/task/'+taskId,
  DELETE_TASK: (taskId) => BASE_URL + '/task/'+taskId,
  UPDATE_MEMBER: (memberId) => BASE_URL + '/member/'+memberId,
  DELETE_MEMBER: (memberId) => BASE_URL + '/member/'+memberId,
  ASSIGN_TASK: (taskId, memberId) => BASE_URL+'/task/'+taskId+'/member/'+memberId
}
