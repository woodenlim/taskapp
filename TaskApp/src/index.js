import React, {PureComponent} from 'react';
import AppNavigator from '@Navigators/AppNavigator';
import { Provider } from 'react-redux';
import storeConfig from '@Store/storeConfig';
import {Text, View} from 'react-native';
import { PersistGate } from 'redux-persist/integration/react';
const { store, persistor } = storeConfig();

export default class App extends PureComponent {

  constructor() {
    super();
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;
    const _DEV_ = true;
    if (!__DEV__) {
      console.log = () => { };
      console.warn = () => { };
      console.info = () => { };
      console.error = () => { };
    }
  }

  componentDidMount(){
    //SplashScreen.hide()
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate /**loading={<Text>Loading</Text>}**/ persistor={persistor}>
          <AppNavigator />
        </PersistGate>
      </Provider>
    );
  }
}
