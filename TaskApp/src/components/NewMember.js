import React, { PureComponent } from "react";
import { View, Text, SafeAreaView, FlatList, TextInput, TouchableOpacity } from "react-native";
import * as createMember from "@MemberActions/createMember";
import * as memberList from "@MemberActions/memberList";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '@Utils/scalingUtils';
import LottieView from 'lottie-react-native';
import DatePicker from 'react-native-datepicker'


class NewMember extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
    }
  }

  componentDidMount(){
    //console.log(this.props.actions);
    //this.props.actions.taskListRequest()
  }

  componentDidUpdate(prevProps) {
    if(this.props.createMember !== prevProps.createMember){
      const {response, status, message} = this.props.createMember;
      console.log(response);
      if(status == 'success'){
        //this.props.navigation.navigate('NewMember', {title: 'New Member'})
        this.props.actions.memberListRequest()
        this.props.navigation.goBack();
      }

    }
  }

  renderMemberListItem = ({item, index}) => {
    return (
      <View>
        <Text>{item.name}</Text>
        <Text>{item.email}</Text>
      </View>
    )
  }

  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.title,
    headerTitleStyle : {
      textAlign: 'center',
      alignSelf:'center'
    },
    headerStyle:{
      backgroundColor:'white',
    }
  });

  addNewMember = () => {
    const { name, email } = this.state
    if(name == "" || email == ""){
      alert('Errors')
      return
    }
    this.props.actions.createMemberRequest({
      name: name,
      email: email
    })
  }

  render() {
    return (
      <SafeAreaView  style={{ flex: 1 }}>
        <View style={{flex:1, paddingLeft: wp(5),paddingRight: wp(5), paddingLeft: wp(5)}}>
          <View style={{paddingTop: hp(2), paddingBottom:hp(2)}}>
            <Text style={{fontSize: hp(2), color:'#BDBDBD'}}>Name</Text>
          </View>
          <TextInput
            style={{borderColor: 'gray', borderBottomWidth: 1, fontSize: hp(2), marginBottom: hp(2)}}
            onChangeText={(name) => this.setState({name})}
            value={this.state.name}
          />

          <View style={{paddingTop: hp(2), paddingBottom:hp(2)}}>
            <Text style={{fontSize: hp(2), color:'#BDBDBD'}}>Email</Text>
          </View>
          <TextInput
            style={{borderColor: 'gray', borderBottomWidth: 1, fontSize: hp(2)}}
            onChangeText={(email) => this.setState({email})}
            value={this.state.email}
          />
        </View>

        <View style={{paddingRight: wp(5), paddingLeft: wp(5)}}>
          <TouchableOpacity onPress={this.addNewMember} style={{backgroundColor: '#81C784', borderRadius:hp(1),paddingTop: hp(2), paddingBottom: hp(2), paddingLeft: wp(3),paddingRight: wp(3)}}>
            <Text style={{fontSize: hp(2), textAlign:'center', color: 'white', letterSpacing: 1}}>Add New Member</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}


mapStateToProps = (state) => {
  const { createMember } = state;
  return {
    createMember
  }
}

mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Object.assign({},
      createMember, memberList), dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewMember);
