import React, { PureComponent } from "react";
import { View, Text, SafeAreaView, FlatList, TouchableOpacity, Alert } from "react-native";
import * as taskList from "@TaskActions/taskList";
import * as updateTask from "@TaskActions/updateTask";
import * as deleteTask from "@TaskActions/deleteTask";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '@Utils/scalingUtils';
import {getMonthName, getDayName, getMonthInTwoDigit, getDateInTwoDigit, getDateOrdinal } from '@Utils/dateUtils';
import { SHORT_DATE_NAME,FULL_DATE_NAME } from '@Constants/constant';
import LottieView from 'lottie-react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import ActionSheet from 'react-native-actionsheet';

class Task extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      taskList: [],
      taskDetails: null
    }
  }

  componentDidMount(){
    //console.log(this.props.actions);
    this.props.actions.taskListRequest()
  }

  componentDidUpdate(prevProps) {
    if(this.props.deleteTask !== prevProps.deleteTask){
      const {response, status, message} = this.props.deleteTask;
      if(status == 'success'){
        this.props.actions.taskListRequest()
      }
    }
  }

  renderTaskListItem = ({item, index}) => {
    const memberName = item.member[0] ? item.member[0].name : "" ;
    return (
      <View style={{
        flex:1,
        flexDirection: 'row',
        backgroundColor:'white',
        marginLeft: wp(3),
        marginRight: wp(3),
        marginTop: hp(0.5),
        marginBottom: hp(0.5),
        paddingLeft:wp(3),
        paddingRight:wp(3),
        paddingTop:hp(1),
        paddingBottom:hp(1),
        borderColor:'#BDBDBD',
        borderWidth: 1,
        borderRadius: hp(1)}}>
        <View style={{flex: 0.2, alignItems:'center', justifyContent:'center'}}>
          <Text style={{fontSize: hp(2),alignSelf: 'center', color: 'black'}}>{new Date(item.dueDate).getDate()}</Text>
          <Text style={{fontSize: hp(2),alignSelf: 'center', color: 'black'}}>{getMonthName(SHORT_DATE_NAME, new Date(item.dueDate).getMonth())}</Text>
          {// <Text style={{fontSize: hp(2),alignSelf: 'center', color: '#757575'}}>{getDayName(SHORT_DATE_NAME, new Date(item.dueDate).getDay())}</Text>
          }
        </View>
        <View style={{paddingRight: wp(4), paddingLeft: wp(4)}}>
          <View style={{width: 1, height:'100%', backgroundColor:'#E0E0E0'}}/>
        </View>
        <View style={{flex:1}}>
          <Text style={{fontSize: hp(2), paddingBottom: hp(1),color:'#616161', fontWeight:'600'}}>{item.name}</Text>
          <Text style={{fontSize: hp(1.5),paddingBottom: hp(1),color:'#616161'}}>{item.description}</Text>
          <View style={{flex: 1, flexDirection: 'row', alignItems:'center'}}>
            <FontAwesome5 name={'user'} size={hp(1.5)}/>
            <Text style={{paddingLeft: wp(2), fontSize: hp(1.5), color:'#616161'}}>{memberName}</Text>
          </View>
        </View>
        <FontAwesome5 name={'ellipsis-h'} color={'black'} onPress={() => this.showActionSheet(item)}/>
      </View>
    )
  }

  showActionSheet = (item) => {
    this.setState({
      taskId: item._id,
      taskDetails: item,
    }, () => {
      this.ActionSheet.show()
    })
  }

  actionSheetAction = (index) => {
    //index 0 = Edit, index 1 = Delete
    switch (index) {
      case 0:{
        this.props.navigation.navigate('SharedTask', {
          title: 'Edit Task',
          taskDetails: this.state.taskDetails
        })
      }
      break;
      case 1: {
        Alert.alert(
          'Delete Task?',
          'Are you sure want to delete?',
          [
            {
              text: 'CANCEL',
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel',
            },
            {text: 'DELETE', style: 'destructive', onPress: () => {
              this.props.actions.deleteTaskRequest({
                id: this.state.taskId
              })
            }},
          ],
          {cancelable: false},
        );
      }
      default:
    }
  }

  render() {
    const { taskList } = this.state

    return (
      <SafeAreaView  style={{ flex: 1 }}>
      <ActionSheet
        ref={o => this.ActionSheet = o}
        title={'What you want to do?'}
        options={['Edit', 'Delete', 'Cancel']}
        cancelButtonIndex={2}
        destructiveButtonIndex={1}
        onPress={(index) => this.actionSheetAction(index)}
      />
      <View style={{ flex: 1 }}>
        <View style={{alignItems:'center', flexDirection: 'row', paddingLeft: wp(5), paddingRight: wp(5), paddingTop: hp(2), paddingBottom:hp(2), borderBottomWidth: 1, borderColor:'#BDBDBD'}}>
          <Text style={{flex:1,fontWeight: 'bold', fontSize: hp(3)}}>Tasks</Text>
          <FontAwesome5 name={'plus'} color={'black'} size={hp(2)} onPress={() => this.props.navigation.navigate('SharedTask', {title: 'New Task'})}/>
        </View>
       <View style={{flex: 1}}>
        {
          this.props.taskList.response.data.length > 0 ? <FlatList
            data={this.props.taskList.response.data}
            style={{flex: 1, flexGrow: 1}}
            renderItem={this.renderTaskListItem}
            keyExtractor={(item, index) => item+index}
            showsVerticalScrollIndicator={false}
          /> :
          <View style={{flex:1, alignItems: 'center', justifyContent:'center'}}>
            <LottieView
              loop
              autoPlay
              speed={0.5}
              style={{width: wp(35), height: hp(35)}}
              source={require('@Lottie/empty_status.json')}/>
              <Text style={{fontSize: hp(2), color: 'grey', letterSpacing: 1, marginBottom: hp(1)}}>Uh-ohhh, no task found</Text>
              <View style={{backgroundColor:'#81C784', borderRadius:hp(1),paddingTop: hp(1), paddingBottom: hp(1), paddingLeft: wp(3),paddingRight: wp(3)}}>
                <Text onPress={() => this.props.navigation.navigate('SharedTask', {title: 'New Task'})} style={{fontSize: hp(2), color: 'white', letterSpacing: 1}}>Add New Task</Text>
              </View>
          </View>
        }
        </View>
      </View>
      </SafeAreaView>
    );
  }
}


mapStateToProps = (state) => {
  const { taskList, updateTask, deleteTask } = state;
  return {
    taskList, updateTask, deleteTask
  }
}

mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Object.assign({},
      taskList, updateTask, deleteTask), dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Task);
