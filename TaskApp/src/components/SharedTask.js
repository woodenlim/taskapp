import React, { PureComponent } from "react";
import { View, Text, SafeAreaView, FlatList, TextInput, TouchableOpacity } from "react-native";
import * as createTask from "@TaskActions/createTask";
import * as updateTask from "@TaskActions/updateTask";
import * as taskList from "@TaskActions/taskList";
import * as memberList from "@MemberActions/memberList";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '@Utils/scalingUtils';
import LottieView from 'lottie-react-native';
import DatePicker from 'react-native-datepicker';
import Modal from "react-native-modal";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';


class SharedTask extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      taskName: '',
      taskDescription: '',
      height: 0,
      dueDate: null,
      isModalVisible: false,
      memberList: [],
      selectedMemberId: '',
      assignedMemberName: '',
      taskDetails: this.props.navigation.getParam('taskDetails', null),
    }
  }

  componentDidMount(){
    //console.log(this.props.actions);
    this.props.actions.memberListRequest()
    console.log(this.state.taskDetails);
    if(this.state.taskDetails !== null){
      this.setState({
        taskName: this.state.taskDetails.name,
        taskDescription: this.state.taskDetails.description,
        dueDate: new Date(this.state.taskDetails.dueDate).toISOString().slice(0,10),
        assignedMemberName: this.state.taskDetails.member.length > 0 ? this.state.taskDetails.member[0].name : '',
        selectedMemberId: this.state.taskDetails.member.length > 0 ? this.state.taskDetails.member[0]._id : ''
      })
    }

  }

  componentDidUpdate(prevProps) {
    if(this.props.createTask !== prevProps.createTask){
      const {response, status, message} = this.props.createTask;
      if(status == 'success'){
        //this.props.navigation.navigate('NewMember', {title: 'New Member'})
        this.props.actions.taskListRequest()
        this.props.navigation.goBack();
      }
    }

    if(this.props.memberList !== prevProps.memberList){
      const {response} = this.props.memberList;
      if(response.length > 0){
        this.setState({
          memberList: response
        })
      }
    }

    if(this.props.updateTask !== prevProps.updateTask){
      const {response, status, message} = this.props.updateTask;
      if(status == 'success'){
        //this.props.navigation.navigate('NewMember', {title: 'New Member'})
        this.props.actions.taskListRequest()
        this.props.navigation.goBack();
      }
    }
  }

  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.title,
    headerTitleStyle : {
      textAlign: 'center',
      alignSelf:'center'
    },
    headerStyle:{
      backgroundColor:'white',
    }
  });

  toggleModal = () => this.setState({ isModalVisible: !this.state.isModalVisible });

  submitTask = () => {
    const { taskName, taskDescription, dueDate } = this.state
    if(taskName == "" || taskDescription == "" || dueDate == null){
      alert('Title, Description and Due Date is REQUIRED.')
      return
    }

    if(this.state.taskDetails == null){
      this.props.actions.createTaskRequest({
        name: this.state.taskName,
        description: this.state.taskDescription,
        dueDate: new Date(this.state.dueDate),
        memberId: this.state.selectedMemberId
      })
    }else{
      this.props.actions.updateTaskRequest({
        id: this.state.taskDetails._id,
        name: this.state.taskName,
        description: this.state.taskDescription,
        dueDate: new Date(this.state.dueDate),
        memberId: this.state.selectedMemberId
      })
    }

  }

  assignTo = (id, name) => {
    this.setState({
      selectedMemberId: id,
      assignedMemberName: name
    }, () => {
      this.toggleModal()
    })
  }

  renderMemberListItem = ({item, index}) => {
    return (
      <TouchableOpacity onPress={() => this.assignTo(item._id, item.name)} style={{flex:1, marginLeft: wp(3), marginRight: wp(3), marginTop: hp(0.5), marginBottom: hp(0.5), paddingLeft:wp(3), paddingRight:wp(3), paddingTop:hp(1), paddingBottom:hp(1), borderColor:'#BDBDBD', borderWidth: 1, borderRadius: hp(1)}}>
        <Text style={{color: '#616161', fontWeight:'600', fontSize: hp(2)}}>{item.name}</Text>
      </TouchableOpacity>
    )
  }

  render() {
    const { memberList, taskDetails } = this.state

    return (
      <SafeAreaView  style={{ flex: 1 }}>
        <Modal isVisible={this.state.isModalVisible}>
          <SafeAreaView style={{flex:1}}>
            <View style={{ flex: 1, backgroundColor:'white', borderRadius:hp(1), paddingTop:hp(1)}}>
              <View style={{flex:1}}>
                <FlatList
                  data={this.props.memberList.response.data}
                  style={{flex: 1}}
                  renderItem={this.renderMemberListItem}
                  keyExtractor={(item, index) => item+index}
                  showsVerticalScrollIndicator={false}
                />
              </View>
              <View style={{paddingBottom: hp(1)}}>
                <TouchableOpacity onPress={this.toggleModal} style={{alignItems:'center', marginLeft:wp(3), marginRight:wp(1.5), backgroundColor:'#BDBDBD', borderRadius:hp(1),paddingTop: hp(1), paddingBottom: hp(1), paddingLeft: wp(3),paddingRight: wp(3)}}>
                  <Text style={{fontSize: hp(2), color: 'white', letterSpacing: 1}}>Cancel</Text>
                </TouchableOpacity>
              </View>
            </View>
          </SafeAreaView>
        </Modal>

        <View style={{flex: 1, paddingRight: wp(5), paddingLeft: wp(5)}}>
          <View style={{paddingTop: hp(2), paddingBottom:hp(2)}}>
            <Text style={{fontSize: hp(2), color:'#616161'}}>Title</Text>
          </View>
          <TextInput
            style={{borderColor: 'gray', borderBottomWidth: 1, fontSize: hp(2), marginBottom: hp(2)}}
            onChangeText={(taskName) => {
              this.setState({taskName})
            }}
            value={this.state.taskName}
          />

          <View style={{paddingTop: hp(2), paddingBottom:hp(2)}}>
            <Text style={{fontSize: hp(2), color:'#616161'}}>Description</Text>
          </View>
          <TextInput
            multiline={true}
            //numberOfLines={5}
            style={{maxHeight:hp(15), borderColor: 'gray', borderBottomWidth: 1, fontSize: hp(2), marginBottom: hp(2)}}
            onChangeText={(taskDescription) => this.setState({taskDescription})}
            onContentSizeChange={(event) => {
              this.setState({ height: event.nativeEvent.contentSize.height })
            }}
            value={this.state.taskDescription}
          />

          <View style={{paddingTop: hp(2), paddingBottom:hp(2)}}>
            <Text style={{fontSize: hp(2), color:'#616161'}}>Due Date:</Text>
          </View>
          <DatePicker
            showIcon={false}
            style={{width: wp(90), marginBottom: hp(2)}}
            date={this.state.dueDate}
            mode="date"
            placeholder=' '
            format="YYYY-MM-DD"
            minDate={new Date()}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateInput: {
                borderLeftWidth: 0,
                borderRightWidth: 0,
                borderTopWidth: 0,
                borderBottomWidth: 1,
                borderColor: 'gray'
              },
              dateText: {
                fontSize: hp(2),
                alignSelf: 'flex-start'
              },
              placeholderText: {
                fontSize: hp(2),
                alignSelf: 'flex-start'
              }
            }}
            onDateChange={(dueDate) => {
              this.setState({dueDate})
            }}
          />

          <View style={{flexDirection:'row', paddingTop: hp(2), paddingBottom:hp(2), alignItems:'center'}}>
            <Text style={{flex: 1, fontSize: hp(2), color:'#616161'}}>Assign To</Text>
          </View>
          <TouchableOpacity onPress={this.toggleModal} style={{height: hp(3),borderColor: 'gray', borderBottomWidth: 1, fontSize: hp(2), marginBottom: hp(2)}}>
            <Text style={{flex: 1, fontSize: hp(2), color:'#616161'}}>{this.state.assignedMemberName}</Text>
          </TouchableOpacity>


        </View>
        <View style={{paddingRight: wp(5), paddingLeft: wp(5)}}>
          <TouchableOpacity onPress={this.submitTask} style={{backgroundColor: '#81C784', borderRadius:hp(1),paddingTop: hp(2), paddingBottom: hp(2), paddingLeft: wp(3),paddingRight: wp(3)}}>
            <Text style={{fontSize: hp(2), textAlign:'center', color: 'white', letterSpacing: 1}}>{this.state.taskDetails !== null ? 'Update Task' : 'Add New Task'}</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}


mapStateToProps = (state) => {
  const { createTask, memberList, updateTask } = state;
  return {
    createTask,
    memberList,
    updateTask
  }
}

mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Object.assign({},
      createTask, memberList, taskList, updateTask), dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SharedTask);
