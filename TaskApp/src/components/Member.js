import React, { PureComponent } from "react";
import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  TextInput,
  Alert
} from "react-native";
import * as memberList from "@MemberActions/memberList";
import * as updateMember from "@MemberActions/updateMember";
import * as deleteMember from "@MemberActions/deleteMember";
import * as taskList from "@TaskActions/taskList";
import * as updateTask from "@TaskActions/updateTask";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "@Utils/scalingUtils";
import LottieView from "lottie-react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import ActionSheet from "react-native-actionsheet";
import Modal from "react-native-modal";

class Member extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      memberList: [],
      isModalVisible: false,
      memberId: "",
      memberName: "",
      memberEmail: "",
      taskDetails: ""
    };
  }

  componentDidMount() {
    //console.log(this.props.actions);
    this.props.actions.memberListRequest();
  }

  componentDidUpdate(prevProps) {
    if (this.props.updateMember !== prevProps.updateMember) {
      const { response, status, message } = this.props.updateMember;
      if (status == "success") {
        //this.props.navigation.navigate('NewMember', {title: 'New Member'})
        this.props.actions.memberListRequest();
      }
    }

    if (this.props.deleteMember !== prevProps.deleteMember) {
      const { response, status, message } = this.props.deleteMember;
      if (status == "success") {
        //this.props.navigation.navigate('NewMember', {title: 'New Member'})
        this.props.actions.memberListRequest();
        this.props.actions.updateTaskRequest({
          id: this.state.taskDetails._id,
          name: this.state.taskDetails.name,
          description: this.state.taskDetails.description,
          dueDate: new Date(this.state.taskDetails.dueDate),
          memberId: ''
        })
        this.props.actions.taskListRequest();
      }
    }
  }

  toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });

  renderMemberListItem = ({ item, index }) => {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          marginLeft: wp(3),
          marginRight: wp(3),
          marginTop: hp(0.5),
          marginBottom: hp(0.5),
          paddingLeft: wp(3),
          paddingRight: wp(3),
          paddingTop: hp(1),
          paddingBottom: hp(1),
          borderColor: "#BDBDBD",
          borderWidth: 1,
          borderRadius: hp(1)
        }}
      >
        <View style={{ flex: 1 }}>
          <Text
            style={{
              paddingBottom: hp(1),
              color: "#616161",
              fontWeight: "600"
            }}
          >
            {item.name}
          </Text>
          <Text style={{ color: "#616161" }}>{item.email}</Text>
        </View>
        <FontAwesome5
          name={"ellipsis-h"}
          color={"black"}
          onPress={() => this.showActionSheet(item)}
        />
      </View>
    );
  };

  showActionSheet = item => {
    this.setState(
      {
        memberName: item.name,
        memberEmail: item.email,
        memberId: item._id
      },
      () => {
        this.ActionSheet.show();
      }
    );
  };

  deleteMember = () => {
    this.props.taskList.response.data.map(item => {
      if (item.member.length > 0) {
        if (item.member[0]._id == this.state.memberId) {
          this.setState({
            taskDetails: item
          })
          Alert.alert(
            "Delete Member?",
            this.state.memberName +
              " is assigned to "+
              item.name+". Are you sure want to delete?",
            [
              {
                text: "CANCEL",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              {
                text: "DELETE",
                style: "destructive",
                onPress: () => {
                  this.props.actions.deleteMemberRequest({
                    id: this.state.memberId
                  })
                }
              }
            ],
            { cancelable: false }
          );
        }
      }
    });

    Alert.alert(
      "Delete Member?",
      "Are you sure want to delete " + this.state.memberName,
      [
        {
          text: "CANCEL",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "DELETE",
          style: "destructive",
          onPress: () => {
            this.props.actions.deleteMemberRequest({
              id: this.state.memberId
            });
          }
        }
      ],
      { cancelable: false }
    );

  };

  actionSheetAction = index => {
    //index 0 = Edit, index 1 = Delete
    switch (index) {
      case 0:
        {
          this.toggleModal();
        }
        break;
      case 1: {
        this.deleteMember();
      }
      default:
    }
  };

  updateMemberDetail = () => {
    this.props.actions.updateMemberRequest({
      id: this.state.memberId,
      name: this.state.memberName,
      email: this.state.memberEmail
    });
    this.toggleModal();
  };

  render() {
    const { memberList } = this.state;
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <ActionSheet
          ref={o => (this.ActionSheet = o)}
          title={"What you want to do?"}
          options={["Edit", "Delete", "Cancel"]}
          cancelButtonIndex={2}
          destructiveButtonIndex={1}
          onPress={index => this.actionSheetAction(index)}
        />

        <Modal
          isVisible={this.state.isModalVisible}
          onBackdropPress={this.toggleModal}
        >
          <View
            style={{
              flex: 0.3,
              justifyContent: "center",
              backgroundColor: "white",
              borderRadius: hp(1),
              paddingLeft: wp(2),
              paddingRight: wp(2),
              paddingTop: hp(2),
              paddingBottom: hp(2)
            }}
          >
            <View style={{ flex: 1, paddingBottom: hp(1) }}>
              <View style={{ paddingTop: hp(1), paddingBottom: hp(1) }}>
                <Text style={{ fontSize: hp(2), color: "#BDBDBD" }}>Name</Text>
              </View>
              <TextInput
                style={{
                  borderColor: "gray",
                  borderBottomWidth: 1,
                  fontSize: hp(2),
                  marginBottom: hp(1)
                }}
                onChangeText={memberName => {
                  this.setState({ memberName });
                }}
                defaultValue={this.state.memberName}
                value={this.state.memberName}
              />

              <View style={{ paddingTop: hp(1), paddingBottom: hp(1) }}>
                <Text style={{ fontSize: hp(2), color: "#BDBDBD" }}>Email</Text>
              </View>
              <TextInput
                style={{
                  borderColor: "gray",
                  borderBottomWidth: 1,
                  fontSize: hp(2),
                  marginBottom: hp(1)
                }}
                onChangeText={memberEmail => {
                  this.setState({ memberEmail });
                }}
                defaultValue={this.state.memberEmail}
                value={this.state.memberEmail}
              />
            </View>
            <View style={{ flex: 0.2, flexDirection: "row" }}>
              <TouchableOpacity
                onPress={this.toggleModal}
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: "#BDBDBD",
                  marginRight: wp(1),
                  borderRadius: hp(1)
                }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    color: "white",
                    letterSpacing: 1
                  }}
                >
                  Cancel
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={this.updateMemberDetail}
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: "#81C784",
                  borderRadius: hp(1)
                }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    color: "white",
                    letterSpacing: 1
                  }}
                >
                  Update
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <View style={{ flex: 1 }}>
          <View
            style={{
              alignItems: "center",
              flexDirection: "row",
              paddingLeft: wp(5),
              paddingRight: wp(5),
              paddingTop: hp(2),
              paddingBottom: hp(2),
              borderBottomWidth: 1,
              borderColor: "#BDBDBD"
            }}
          >
            <Text style={{ flex: 1, fontWeight: "bold", fontSize: hp(3) }}>
              Members
            </Text>
            <FontAwesome5
              name={"plus"}
              color={"black"}
              size={hp(2)}
              onPress={() =>
                this.props.navigation.navigate("NewMember", {
                  title: "New Member"
                })
              }
            />
          </View>
          <View style={{ flex: 1 }}>
            {this.props.memberList.response.data.length > 0 ? (
              <FlatList
                data={this.props.memberList.response.data}
                style={{ flex: 1 }}
                renderItem={this.renderMemberListItem}
                keyExtractor={(item, index) => item + index}
                showsVerticalScrollIndicator={false}
              />
            ) : (
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <LottieView
                  loop
                  autoPlay
                  speed={0.5}
                  style={{ width: wp(35), height: hp(35) }}
                  source={require("@Lottie/empty_status.json")}
                />
                <Text
                  style={{
                    fontSize: hp(2),
                    color: "grey",
                    letterSpacing: 1,
                    marginBottom: hp(1)
                  }}
                >
                  Uh-ohhh, no member found
                </Text>
                <View
                  style={{
                    backgroundColor: "#81C784",
                    borderRadius: hp(1),
                    paddingTop: hp(1),
                    paddingBottom: hp(1),
                    paddingLeft: wp(3),
                    paddingRight: wp(3)
                  }}
                >
                  <Text
                    onPress={() =>
                      this.props.navigation.navigate("NewMember", {
                        title: "New Member"
                      })
                    }
                    style={{
                      fontSize: hp(2),
                      color: "white",
                      letterSpacing: 1
                    }}
                  >
                    Add New Member
                  </Text>
                </View>
              </View>
            )}
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

mapStateToProps = state => {
  const { memberList, updateMember, deleteMember, taskList, updateTask } = state;
  return {
    memberList,
    updateMember,
    deleteMember,
    taskList,
    updateTask
  };
};

mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      Object.assign({}, memberList, updateMember, deleteMember, taskList, updateTask),
      dispatch
    )
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Member);
