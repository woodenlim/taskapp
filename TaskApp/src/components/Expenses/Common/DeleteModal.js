import React, { PureComponent } from "react";
import {
  View,
  Text,
  FlatList,
  Dimensions,
  TouchableOpacity,
  Animated
} from "react-native";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Modal from "react-native-modal";
import { CalendarList } from 'react-native-calendars';
import {getMonthName, getDayName, getMonthInTwoDigit, getDateInTwoDigit } from '@Utils/dateUtils';
import {GRADIENT_COLOR} from '@Constants/constant';
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import {DEVICE_WIDTH,DEVICE_HEIGHT,SHORT_DATE_NAME,FULL_DATE_NAME} from '@Constants/constant';
import * as deleteTransactionAction from "@ExpensesAction/deleteTransaction";
import * as dailyTransactionAction from "@ExpensesAction/dailyTransaction";
import * as monthlyTransactionAction from "@ExpensesAction/monthlyTransaction";
import * as monthListAction from "@ExpensesAction/monthList";
import * as dailyCategoryTransactionAction from "@ExpensesAction/dailyCategoryTransaction";
import * as monthlyCategoryTransactionAction from "@ExpensesAction/monthlyCategoryTransaction";
import * as recentTransactionAction from "@ExpensesAction/recentTransaction";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import realm from '@Schema/schema';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '@Utils/scalingUtils';

class DeleteModal extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentYear: new Date().getFullYear(),
      currentMonth: getMonthName(SHORT_DATE_NAME, new Date().getMonth()),
      currentDate: new Date().getDate(),
      currentDay: getDayName(SHORT_DATE_NAME, new Date().getDay()),
      markedTodayDate: new Date().getFullYear() + "-" + getMonthInTwoDigit((new Date().getMonth()+1)) + "-" + getDateInTwoDigit(new Date().getDate()),
      selectedDate: new Date().setHours(0, 0, 0, 0),
      transactionId: ''
    }
  }

  deleteTransaction = () => {
    this.props.actions.deleteTransactionRequest({
      id: this.state.transactionId
    })
    this.props.onBackdropPress();
  }

  componentDidUpdate(prevProps){
    if(prevProps.transactionDetail !== this.props.transactionDetail){
      const {transactionDetail} = this.props
      const id = transactionDetail ? transactionDetail.id : null
      this.setState({
        transactionId: id
      })
    }

    if(prevProps.deleteTransaction !== this.props.deleteTransaction){
      const {transactionDetail} = this.props
      const {currentYear, currentDate} = this.state;
      let currentMonth = (new Date().getMonth() + 1) + '';
      let day = currentDate + '';

      if (currentMonth.length < 2) currentMonth = '0' + currentMonth;
      if (day.length < 2) day = '0' + day;

      const date = currentYear + '-' + currentMonth + '-' + day;
      const month = new Date().getFullYear() + '-' + getMonthInTwoDigit((new Date().getMonth()+1))
      if(this.props.deleteTransaction.message == 'SUCCESS'){
        if(date == transactionDetail.transactionDate){
          this.props.actions.recentTransactionRequest({date: transactionDetail.transactionDate});
        }
        this.props.actions.dailyTransactionRequest({date: transactionDetail.transactionDate});
        this.props.actions.monthlyTransactionRequest({month});
        this.props.actions.monthListRequest({month});
        this.props.actions.dailyCategoryTransactionRequest({
          month: transactionDetail.transactionMonth,
          date: transactionDetail.transactionDate,
          categoryName: transactionDetail.categoryName,
          categoryIcon: transactionDetail.categoryIcon
        })
        this.props.actions.monthlyCategoryTransactionRequest({
          month: transactionDetail.transactionMonth,
          name: transactionDetail.categoryName,
          icon: transactionDetail.categoryIcon
        })
      }
    }
  }

  render() {
    const {currentYear,currentMonth,currentDate,currentDay,markedTodayDate, selectedDate,
           flatListData, carouselData, expensesBorderColor, incomeBorderColor, chartData, categoryData} = this.state
    return (
      <Modal
        animationIn={'zoomIn'}
        animationOut={'zoomOut'}
        onBackdropPress={this.props.onBackdropPress}
        onBackButtonPress={this.props.onButtonPress}
        isVisible={this.props.showModal}>
        <View style={{
          flex: 0.3,
          backgroundColor: 'white',
          borderRadius: wp(3),
          borderColor: 'white'}}>
          <View style={{flex: 1}}>
            <Text style={{
              fontSize: hp(3),
              letterSpacing: 1,
              color:'black',
              paddingTop: hp(2),
              paddingBottom: hp(2),
              paddingLeft: wp(4),
              paddingRight: wp(4)}}>Delete Record?</Text>
            <Text style={{fontSize: hp(2), letterSpacing: 1, color:'black', paddingBottom: hp(3), paddingLeft: wp(4), paddingRight: wp(4)}}>Deleting a record will permanently remove it.</Text>
            <View style={{flex: 1, flexDirection: 'row', alignItems:'flex-end', justifyContent:'flex-end'}}>
              <TouchableOpacity
                onPress={this.props.onBackdropPress}
                style={{
                  flex:1,
                  backgroundColor:'#E0E0E0',
                  borderRadius: hp(1),
                  justifyContent:'center',
                  alignItems:'center',
                  marginTop: hp(3),
                  marginBottom: hp(2),
                  marginLeft: wp(4),
                  marginRight: wp(2)}}>
                <Text style={{fontSize: hp(2), letterSpacing: 1, color:'black', paddingTop: hp(1), paddingBottom:hp(1)}}>CANCEL</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={this.deleteTransaction}
                style={{
                  flex:1,
                  backgroundColor:'#ED5E68',
                  borderRadius: hp(1),
                  justifyContent:'center',
                  alignItems:'center',
                  marginTop: hp(3),
                  marginBottom: hp(2),
                  marginLeft: wp(2),
                  marginRight: wp(4)}}>
                <Text style={{fontSize: hp(2), letterSpacing: 1, color:'white', paddingTop: hp(1), paddingBottom:hp(1)}}>DELETE</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

mapStateToProps = (state) => {
  const { dailyTransaction, monthlyTransaction,monthList, deleteTransaction, dailyCategoryTransaction, recentTransaction } = state;
  return {
    deleteTransaction,
    dailyTransaction,
    monthlyTransaction,
    monthList,
    dailyCategoryTransaction,
    monthlyCategoryTransaction,
    recentTransaction
  }
}

mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Object.assign({},
      deleteTransactionAction, dailyTransactionAction, monthlyTransactionAction,
      monthListAction, dailyCategoryTransactionAction, monthlyCategoryTransactionAction,
      recentTransactionAction), dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DeleteModal);
