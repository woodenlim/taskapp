import React, { PureComponent } from "react";
import {
  View,
  Text,
  FlatList,
  Dimensions,
  TouchableOpacity,
  Animated
} from "react-native";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Modal from "react-native-modal";
import { CalendarList } from 'react-native-calendars';
import {getMonthName, getDayName, getMonthInTwoDigit, getDateInTwoDigit } from '@Utils/dateUtils';
import {GRADIENT_COLOR} from '@Constants/constant';
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import {DEVICE_WIDTH,DEVICE_HEIGHT,SHORT_DATE_NAME,FULL_DATE_NAME} from '@Constants/constant';

class TransactionDetailModal extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentYear: '',
      currentMonth: '',
      currentDate: '',
      currentDay: '',
      markedTodayDate: new Date().getFullYear() + "-" + getMonthInTwoDigit((new Date().getMonth()+1)) + "-" + getDateInTwoDigit(new Date().getDate()),
      selectedDate: new Date().setHours(0, 0, 0, 0),
    }
  }

  navigateTo = (transactionDetail) => {
    this.props.toggleModal();
    this.props.navigation.navigate('NewExpenses', {transactionDetail: transactionDetail})
  }

  componentDidUpdate(prevProps){
    if(prevProps.transactionDetail !== this.props.transactionDetail){
      const {transactionDetail} = this.props
      const date = transactionDetail.response ? transactionDetail.response.transactionDate : null
      this.setState({
        currentYear: new Date(date).getFullYear(),
        currentMonth: getMonthName(FULL_DATE_NAME, new Date(date).getMonth()),
        currentDate: new Date(date).getDate(),
        currentDay: getDayName(FULL_DATE_NAME, new Date(date).getDay()),
      })
    }
  }

  render() {
    const {currentYear,currentMonth,currentDate,currentDay,markedTodayDate, selectedDate,
           flatListData, carouselData, expensesBorderColor, incomeBorderColor, chartData, categoryData} = this.state
    const {transactionDetail} = this.props
    //console.log(transactionDetail.response);
    return (
      <Modal
        onBackdropPress={this.props.onBackdropPress}
        onBackButtonPress={this.props.onBackButtonPress}
        isVisible={this.props.showModal}
        onModalHide={this.props.onModalHide}
        animationIn={'zoomIn'}
        animationOut={'zoomOut'}>
        <View style={{
          overflow:'hidden',
          flex: 0.7,
          backgroundColor: 'white',
          borderRadius: 10,
          borderColor: 'white'}}>
            <LinearGradient
              colors={GRADIENT_COLOR}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{flex:1, flexDirection:'row', padding: 20, paddingBottom: 0, borderBottomWidth: 0.5}}>
              <View style={{flex:1}}>
                <Text style={{fontSize: 20, color: 'white'}}>{currentYear}</Text>
                <Text style={{fontSize: 25, color: 'white'}}>{currentDay}, {currentDate} {currentMonth}</Text>
              </View>
              <TouchableOpacity onPress={this.props.toggleModal} style={{flexDirection:'row', justifyContent:'flex-end'}}>
                <FontAwesome5 name={'times-circle'} color="white" size={20} />
              </TouchableOpacity>
            </LinearGradient>

            <Animatable.View
              style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <Text style={{width:"100%",textAlign:"center", padding:5, color: 'black', fontSize: 35, letterSpacing: 1}}>
                $ {transactionDetail.response ? transactionDetail.response.amount : null}
              </Text>
            </Animatable.View>

            <Animatable.View
              animation={''}
              style={{flex:1, flexDirection:'row', alignItems: 'center', justifyContent: 'center', paddingRight: 20, paddingLeft: 20}}>
              <View
                style={{alignItems: 'center', justifyContent: 'center', width: 40, height: 40}}>
                <FontAwesome5 name={transactionDetail.response ? transactionDetail.response.categoryIcon : null} color="rgba(0,0,0, 0.5)" size={20}/>
              </View>
              <View
                style={{flex: 1, alignItems: 'flex-start', justifyContent: 'center', paddingLeft: 20}}>
                <Text style={{fontSize: 18, color: 'black'}}>{transactionDetail.response ? transactionDetail.response.categoryName : null}</Text>
              </View>
            </Animatable.View>

            <Animatable.View
              style={{flex:1, flexDirection:'row', alignItems: 'center', justifyContent: 'center', paddingRight: 20, paddingLeft: 20}}>
              <View
                style={{alignItems: 'center', justifyContent: 'center', width: 40, height: 40}}>
                <FontAwesome5 name='pen' color="rgba(0,0,0, 0.5)" size={20}/>
              </View>
              <View
                style={{flex: 1, alignItems: 'flex-start', justifyContent: 'center', paddingLeft: 20, }}>
                <Text style={{fontSize: 18, color: 'black'}}>
                  {transactionDetail.response ? transactionDetail.response.notes ? transactionDetail.response.notes:'Add Notes' : null}
                </Text>
              </View>
            </Animatable.View>

            <View style={{flexDirection:'row', justifyContent:'flex-end', padding:20}}>
              <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center', paddingRight: 20}}  onPress={this.props.toggleDeleteModal}>
                <FontAwesome5 name={'trash-alt'} color="rgba(0,0,0, 0.5)" size={20} />
                <Text style={{fontSize: 10, letterSpacing: 1, color:'black', padding:10 }}>DELETE</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center'}} onPress={()=>this.navigateTo(transactionDetail.response)}>
                <FontAwesome5 name={'edit'} color="rgba(0,0,0, 0.5)" size={20} />
                <Text style={{fontSize: 10, letterSpacing: 1, color:'black', padding:10 }}>EDIT</Text>
              </TouchableOpacity>
            </View>

          </View>
      </Modal>
    );
  }
}

export default TransactionDetailModal;
