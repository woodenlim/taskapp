import React, { PureComponent } from "react";
import {
  View,
  Text,
  FlatList,
  Dimensions,
  TouchableOpacity,
  Animated,
  Platform
} from "react-native";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Modal from "react-native-modal";
import { CalendarList, Calendar } from 'react-native-calendars';
import {getMonthName, getDayName, getMonthInTwoDigit, getDateInTwoDigit } from '@Utils/dateUtils';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '@Utils/scalingUtils';
import {GRADIENT_COLOR} from '@Constants/constant';
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import {DEVICE_WIDTH,DEVICE_HEIGHT,SHORT_DATE_NAME,FULL_DATE_NAME} from '@Constants/constant';

class CalendarModal extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentYear: new Date().getFullYear(),
      currentMonth: getMonthName(FULL_DATE_NAME, new Date().getMonth()),
      currentDate: new Date().getDate(),
      currentDay: getDayName(SHORT_DATE_NAME, new Date().getDay()),
      markedTodayDate: new Date().getFullYear() + "-" + getMonthInTwoDigit((new Date().getMonth()+1)) + "-" + getDateInTwoDigit(new Date().getDate()),
      selectedDate: new Date().setHours(0, 0, 0, 0),
    }
  }

  componentDidUpdate(prevProps){
    if(prevProps.selectedDate !== this.props.selectedDate){
      this.setState({
        currentYear: new Date(this.props.selectedDate).getFullYear(),
        currentMonth: getMonthName(FULL_DATE_NAME, new Date(this.props.selectedDate).getMonth()),
        currentDate: new Date(this.props.selectedDate).getDate(),
        currentDay: getDayName(SHORT_DATE_NAME, new Date(this.props.selectedDate).getDay()),
      })
    }
  }

  render() {
    const {currentYear,currentMonth,currentDate,currentDay,markedTodayDate, selectedDate,
           flatListData, carouselData, expensesBorderColor, incomeBorderColor, chartData, categoryData} = this.state
           //alert(this.props.selectedDate)
    return (
      <Modal
        animationIn={'zoomIn'}
        animationOut={'zoomOut'}
        onBackdropPress={this.props.onBackdropPress}
        onBackButtonPress={this.props.onBackButtonPress}
        isVisible={this.props.showModal}>
        <View style={{
          overflow: 'hidden',
          flex:0.8,
          backgroundColor: 'white',
          borderRadius: wp(3),
          borderColor: 'white'}}>
          <LinearGradient
            colors={GRADIENT_COLOR}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            style={{
              flex:2,
              padding: wp(4),
              paddingTop: hp(2),
              paddingBottom: 0,
              borderBottomWidth: 0.5}}>
            <Text style={{fontSize: hp(3.5), color: 'white'}}>{currentYear}</Text>
            <Text style={{fontSize: hp(3), color: 'white'}}>{currentDay}, {currentDate} {currentMonth} </Text>
          </LinearGradient>
          <Calendar
            current={this.props.selectedDate}
            style={{flex: 10}}
            futureScrollRange={0}
            maxDate={new Date()}
            horizontal={true}
            pagingEnabled={true}
            calendarWidth={DEVICE_WIDTH}
            scrollEnabled={true}
            hideArrows={false}
            hideExtraDays={false}
            onDayPress={this.props.onDayPress}
            markedDates={{[this.props.selectedDate]: {selected: true, selectedColor: '#4568dc'}}}
            theme={{
              'stylesheet.calendar.header': {
                header: {
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingLeft: wp(3),
                  paddingRight: wp(3),
                  alignItems: 'center'
                },
                monthText: {
                  fontSize: hp(2),
                  fontFamily: 'System',
                  fontWeight: '300',
                  //color: 'black',
                  margin: wp(3),
                  marginTop: 0,
                  marginBottom: hp(1)
                },
                // arrow: {
                //   padding: wp(3),
                //   paddingTop: hp(1),
                //   paddingBottom: hp(1)
                // },
                week: {
                  marginTop: 0,
                  flexDirection: 'row',
                  justifyContent: 'space-around'
                },
                dayHeader: {
                  marginTop: 0,
                  marginBottom: hp(1),
                  flex: 1,
                  textAlign: 'center',
                  fontSize: hp(2),
                  fontFamily: 'System',
                  //color: 'black'
                },
              },
              'stylesheet.day.basic': {
                base: {
                  height: hp(4),
                  width: hp(4),
                  alignItems: 'center',
                  justifyContent: 'center'
                },
                text: {
                  fontSize: hp(2.5),
                  fontFamily: 'System',
                  fontWeight: '300',
                  //color: 'black',
                  backgroundColor: 'rgba(255, 255, 255, 0)'
                },
                // alignedText: {
                //   marginTop: Platform.OS === 'android' ? 4 : 6
                // },
                selected: {
                  //backgroundColor: appStyle.selectedDayBackgroundColor,
                  borderRadius: hp(2)
                },
              },
              'stylesheet.calendar.main': {
                week: {
                  marginTop: hp(1),
                  marginBottom: hp(1),
                  flexDirection: 'row',
                  justifyContent: 'space-around'
                },
              }
            }}/>
          <View style={{flex: 1, flexDirection: 'row', alignItems:'flex-end', justifyContent:'flex-end'}}>
              <TouchableOpacity
                onPress={this.props.toggleModal}
                style={{
                  backgroundColor:'#E0E0E0',
                  borderRadius: hp(1),
                  marginTop: hp(3),
                  marginBottom: hp(3),
                  marginLeft: wp(4),
                  marginRight: wp(4)}}>
                <Text style={{fontSize: hp(2), color:'black', paddingLeft: wp(3), paddingRight: wp(3), paddingTop: hp(1), paddingBottom: hp(1)}}>CANCEL</Text>
              </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }
}

export default CalendarModal;
