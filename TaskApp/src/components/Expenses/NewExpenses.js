import React, { PureComponent } from "react";
import {
  View,
  ScrollView,
  Text,
  TextInput,
  Image,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  FlatList,
  Alert,
  Keyboard,
  Platform,
  KeyboardAvoidingView
} from "react-native";
import SwitchSelector from 'react-native-switch-selector';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { CalendarList } from 'react-native-calendars';
import Modal from "react-native-modal";
import {getMonthName, getDayName, getMonthInTwoDigit, getDateInTwoDigit,getDateOrdinal } from '@Utils/dateUtils';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '@Utils/scalingUtils';
import * as addTransactionAction from "@ExpensesAction/addTransaction";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import {GRADIENT_COLOR, GRADIENT_START, GRADIENT_END, EXPENSE_GRADIENT_COLOR, INCOME_GRADIENT_COLOR,
        DEVICE_WIDTH,DEVICE_HEIGHT,SHORT_DATE_NAME,FULL_DATE_NAME,
        EXPENSES_CATEGORY, INCOME_CATEGORY} from '@Constants/constant';
import CalendarModal from '@ExpensesCommonComponents/CalendarModal';
import * as dailyTransactionAction from "@ExpensesAction/dailyTransaction";
import * as monthlyTransactionAction from "@ExpensesAction/monthlyTransaction";
import * as monthListAction from "@ExpensesAction/monthList";
import * as updateTransactionAction from "@ExpensesAction/updateTransaction";
import * as recentTransactionAction from "@ExpensesAction/recentTransaction";
import * as monthlyCategoryTransactionAction from "@ExpensesAction/monthlyCategoryTransaction";
import uuidv1 from 'uuid/v1';

class NewExpenses extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      type: "expense",
      categoryIcon: this.props.navigation.getParam('categoryIcon', 'tags'),
      categoryName: this.props.navigation.getParam('categoryName', 'Category'),
      categoryType: this.props.navigation.getParam('categoryType', null),
      transactionDate: this.props.navigation.getParam('transactionDate', null),
      expensesCategoryIcon: EXPENSES_CATEGORY,
      incomeCategoryIcon: INCOME_CATEGORY,
      keyboardDisplayText: [{value: '1'},{value: '2'},{value: '3'},{value: '4'},{value: '5'},{value: '6'},{value: '7'},{value: '8'},{value: '9'}],
      keyboardInputValue: '',
      notes: "",
      amount: '',
      isCalendarModalVisible: false,
      isIconModalVisible: false,
      isKeyboardModalVisible: false,
      currentYear: new Date().getFullYear(),
      currentMonth: getMonthName(SHORT_DATE_NAME, new Date().getMonth()),
      currentDate: new Date().getDate(),
      currentDay: getDayName(SHORT_DATE_NAME, new Date().getDay()),
      markedTodayDate: new Date().getFullYear() + "-" + getMonthInTwoDigit((new Date().getMonth()+1)) + "-" + getDateInTwoDigit(new Date().getDate()),
      selectedDate: new Date().setHours(0, 0, 0, 0),
      isKeyboardHidden: false,
      textInputHeight: 0,
      amountShakeAnimation: '',
      categoryShakeAnimation: '',
      keyboardSubmitText: 'OK',
      transactionId: '',
      keyboardHeight: 0, //for android only
      transactionType: '', //for daily transaction request when direct to AllTransaction screen
      isUpdate: false,
    }
  }
  
  componentDidMount () {
    const {currentYear, currentDate, transactionDate} = this.state;
    let currentMonth = (new Date().getMonth() + 1) + '';
    let day = currentDate + '';

    if (currentMonth.length < 2) currentMonth = '0' + currentMonth;
    if (day.length < 2) day = '0' + day;

    const date = transactionDate ? transactionDate : currentYear + '-' + currentMonth + '-' + day;
    this.setState({
      selectedDate: date
    })
    if(Platform.OS == 'android'){
      this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
      this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }
    const transactionDetail = this.props.navigation.getParam('transactionDetail', null)
    if(transactionDetail){
      this.setState({
        transactionId: transactionDetail.id,
        amount: transactionDetail.amount,
        notes: transactionDetail.notes,
        categoryIcon: transactionDetail.categoryIcon,
        categoryName: transactionDetail.categoryName,
        selectedDate: new Date(transactionDetail.transactionDate).setHours(0, 0, 0, 0),
        keyboardSubmitText: 'DONE',
        isUpdate: true,
      })
    }
  }

  componentWillUnmount () {
    if(Platform.OS == 'android'){
      this.keyboardDidShowListener.remove();
      this.keyboardDidHideListener.remove();
    }
  }

  componentDidUpdate(prevProps){
    const {currentYear, currentDate, selectedDate, transactionType} = this.state;
    let currentMonth = (new Date().getMonth() + 1) + '';
    let day = currentDate + '';

    if (currentMonth.length < 2) currentMonth = '0' + currentMonth;
    if (day.length < 2) day = '0' + day;

    const today = currentYear + '-' + currentMonth + '-' +  day;

    const date = selectedDate == new Date().setHours(0, 0, 0, 0) ?
        new Date().getFullYear() + "-" + getMonthInTwoDigit((new Date().getMonth()+1)) + "-" + getDateInTwoDigit(new Date().getDate()) :
        new Date(selectedDate).getFullYear() + "-" + getMonthInTwoDigit((new Date(selectedDate).getMonth()+1)) + "-" + getDateInTwoDigit(new Date(selectedDate).getDate())

    if((prevProps.addTransaction !== this.props.addTransaction) && this.props.addTransaction.status == 'SUCCESS'){
      //const month = getMonthName(SHORT_DATE_NAME, new Date(date).getMonth()) + ' ' + new Date(date).getFullYear();
      const month = new Date().getFullYear() + '-' + getMonthInTwoDigit((new Date().getMonth()+1))
      if(date == today){//Update Dashboard only when TODAY
        this.props.actions.recentTransactionRequest({date: date});
      }
      this.props.actions.monthlyTransactionRequest({month});
      this.props.actions.monthListRequest({month});

      if(selectedDate == today){
        //this.props.actions.recentTransactionRequest({date}) //only update dashboard when is TODAY
        this.props.navigation.popToTop();
      }else{
        this.props.navigation.navigate('AllTransaction', {
          selectedDate: date,
          transactionType: transactionType,
          backToHome: true,
        });
      }
    }

    if(prevProps.updateTransaction !== this.props.updateTransaction){
      //alert(JSON.stringify(this.props.updateTransaction))
      const { updateTransaction} = this.props;
      if(updateTransaction.message == 'SUCCESS'){
        //this.props.navigation.navigate('Dashboard');
        this.props.actions.dailyTransactionRequest({date:updateTransaction.response.transactionDate});
        this.props.actions.monthlyTransactionRequest({month:updateTransaction.response.transactionMonth});
        this.props.actions.monthListRequest({month:updateTransaction.response.transactionMonth});
        //if(date == today){
          this.props.actions.recentTransactionRequest({date: today});
        //}
        this.props.actions.monthlyCategoryTransactionRequest({
          month: updateTransaction.response.transactionMonth,
          name: updateTransaction.response.categoryName,
          icon: updateTransaction.response.categoryIcon
        })
        this.props.navigation.goBack();
      }
    }
  }

  _keyboardDidShow = (e) => {
    const {isKeyboardHidden} = this.state
    if(Platform.OS == 'android'){
      this.setState({
        isKeyboardHidden: !isKeyboardHidden,
        keyboardHeight: e.endCoordinates.height
      })
    }
  }

  _keyboardDidHide = () => {
    const {isKeyboardHidden} = this.state
    if(Platform.OS == 'android'){
      this.setState({
        isKeyboardHidden: !isKeyboardHidden
      })
    }
  }

  onAnimationEnd = () => {
    this.setState({
      amountShakeAnimation: '',
      categoryShakeAnimation: ''
    })
  }

  submitTransaction = () => {
    const { type, amount, categoryIcon, categoryName,
            selectedDate, notes, amountShakeAnimation, categoryShakeAnimation,
            keyboardSubmitText,transactionId } = this.state;
    let isAmountShake = amount == 0 ? 'shake':'';
    let isCategoryShake = (categoryName == 'Category' || categoryIcon == 'tags') ? 'shake':'';

    if(isAmountShake || isCategoryShake){
      this.setState({
        amountShakeAnimation: isAmountShake,
        categoryShakeAnimation: isCategoryShake
      })
      return;
    }

    let date = selectedDate == new Date().setHours(0, 0, 0, 0) ?
        new Date().getFullYear() + "-" + getMonthInTwoDigit((new Date().getMonth()+1)) + "-" + getDateInTwoDigit(new Date().getDate()) :
        new Date(selectedDate).getFullYear() + "-" + getMonthInTwoDigit((new Date(selectedDate).getMonth()+1)) + "-" + getDateInTwoDigit(new Date(selectedDate).getDate())
    this.setState({transactionType: type})
    if(keyboardSubmitText == 'OK'){
      this.props.actions.addTransactionRequest({
        id: uuidv1(),
        transactionType: type,
        amount: parseFloat(amount).toFixed(2),
        categoryIcon: categoryIcon,
        categoryName: categoryName,
        transactionDate: date,
        transactionMonth: new Date(date).getFullYear() + '-' + getMonthInTwoDigit((new Date(date).getMonth()+1)),
        notes: notes
      })
    }else{//if is UPDATE
      this.props.actions.updateTransactionRequest({
        id: transactionId,
        transactionType: type,
        amount: parseFloat(amount).toFixed(2),
        categoryIcon: categoryIcon,
        categoryName: categoryName,
        transactionDate: date,
        transactionMonth: new Date(date).getFullYear() + '-' + getMonthInTwoDigit((new Date(date).getMonth()+1)),
        notes: notes
      })
    }

  }

  toggleCalendarModal = () => this.setState({ isCalendarModalVisible: !this.state.isCalendarModalVisible });
  onCalendarBackdropPress = () => this.setState({ isCalendarModalVisible: !this.state.isCalendarModalVisible });
  onCalendarBackButtonPress = () => this.setState({ isCalendarModalVisible: !this.state.isCalendarModalVisible });

  toggleIconModal = () => this.setState({ isIconModalVisible: !this.state.isIconModalVisible });
  onIconBackdropPress = () => this.setState({ isIconModalVisible: !this.state.isIconModalVisible });
  onIconBackButtonPress = () => this.setState({ isIconModalVisible: !this.state.isIconModalVisible });

  toggleKeyboardView = () => {
    const { isKeyboardHidden }  = this.state
    //alert(isKeyboardHidden)
    // if(!isKeyboardHidden){
    //   this.setState({isKeyboardHidden: !isKeyboardHidden});
    // }
    Keyboard.dismiss();
  }

  toggleKeyboardModal = () => this.setState({ isKeyboardModalVisible: !this.state.isKeyboardModalVisible });
  onKeyboardBackdropPress = () => this.setState({ isKeyboardModalVisible: !this.state.isKeyboardModalVisible });
  onKeyboardBackButtonPress = () => this.setState({ isKeyboardModalVisible: !this.state.isKeyboardModalVisible });

  onDayPress = (day) => {
    console.log(day);
    this.setState({
      selectedDate: day.dateString,
      isCalendarModalVisible: !this.state.isCalendarModalVisible})
  }

  onChangeText = (text) => { this.setState({amount: text})}

  selectType = (value) => {
    const {type} = this.state
    if(value !== type){
      this.setState({categoryIcon: 'tags', categoryName: 'Category'})
    }
    this.setState({type:value})
  }

  selectCategory = (item) => {
    this.setState({
      categoryIcon: item.icon,
      categoryName: item.name,
      isIconModalVisible: !this.state.isIconModalVisible})
  }

  formatData = (data, numColumns) => {
    const numberOfFullRows = Math.floor(data.length / numColumns);

    let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
    while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
      data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true });
      numberOfElementsLastRow++;
    }

    return data;
  };

  formatSelectedDateDisplay = (selectedDate) => {
    let currentDate = new Date().setHours(0, 0, 0, 0);
    let displayDay = new Date(selectedDate).setHours(0, 0, 0, 0) == currentDate ? "Today" : getDayName(FULL_DATE_NAME, (new Date(selectedDate).getDay()));
    let displayMonth = new Date(selectedDate).getMonth();
    let displayDate = new Date(selectedDate).getDate();
    return displayDay + ", " + displayDate + " " + getMonthName(FULL_DATE_NAME, displayMonth);
  }

  renderItem = ({ item, index }) => {
    let backgroundColor = this.state.type == "expense" ? EXPENSE_GRADIENT_COLOR : INCOME_GRADIENT_COLOR;
    let colors = item.icon ? backgroundColor : ['white','white']
    return (
      <TouchableOpacity style={{flex: 1, height: hp(50)/3, alignItems: 'center', justifyContent: 'center'}} onPress={()=>this.selectCategory(item)}>
        <LinearGradient
          colors={colors}
          style={{marginBottom: hp(1), alignItems: 'center', justifyContent: 'center', width: wp(14), height: wp(14), borderRadius: wp(7)}}>
          <FontAwesome5 name={item.icon} color="white" size={hp(3)}/>
        </LinearGradient>
        <Text style={{fontSize:hp(1.5), color:'black'}}>{item.name}</Text>
      </TouchableOpacity>
    );
  };

  renderNumber = ({item, index}) => {
    return (
      <TouchableOpacity style={{flex: 1 , height: hp(10), alignItems: 'center', justifyContent: 'center'}}
        onPress={(value)=>this.selectKeyboardInputValue(item.value)}>
        <Text style={{fontSize: hp(4), color:'white'}}>{item.value}</Text>
      </TouchableOpacity>
    )
  }

  selectKeyboardInputValue = (value) => {
    const {keyboardInputValue, amount, isKeyboardHidden, keyboardSubmitText, isUpdate } = this.state;

    let result = isUpdate ? value == 'backspace' ? '' : value : value == "backspace" ? amount.slice(0, amount.length - 1) : amount + value;
    if(isUpdate){
      this.setState({
        isUpdate: false,
        amount: value == 'backspace' ? '' : value
      });
    }

    if(result.length > 13){
      return;
    }

    //Only 2 characters allow after dot
    if(result.includes('.')){
      let afterDot = result.split('.')[1]
      if(afterDot && afterDot.length > 2){
        return;
      }
    }

    //Reset Amount
    if(result == '0'){
      this.setState({
        amount: ''
      })
      return
    }

    //Prevent enter multiple dot
    if(value == '.' && amount.includes('.')){
      return;
    }

    //Handle when user 1st type 0 or dot
    if(value == '.' && amount == ''){
      this.setState({
        amount: '0'+value
      });
      return;
    }

    this.setState({
      amount: result
    });
  }

  render() {
    const {currentYear,currentMonth,currentDate,currentDay,markedTodayDate,
           selectedDate,expensesCategoryIcon,incomeCategoryIcon,
           categoryIcon,categoryName, amount, isKeyboardHidden, keyboardDisplayText,
           amountShakeAnimation, categoryShakeAnimation, keyboardSubmitText, keyboardHeight, categoryType} = this.state
    const transactionDetail = this.props.navigation.getParam('transactionDetail', null)
    console.log(transactionDetail);
    return (
      <LinearGradient
        start={GRADIENT_START} end={GRADIENT_END}
        colors={GRADIENT_COLOR} style={{flex: 1}}>

        <CalendarModal
          toggleModal={this.toggleCalendarModal}
          onBackdropPress={this.onCalendarBackdropPress}
          onBackButtonPress={this.onCalendarBackButtonPress}
          showModal={this.state.isCalendarModalVisible}
          onDayPress={this.onDayPress}
          selectedDate={selectedDate}/>

        <Modal
          onBackdropPress={this.onIconBackdropPress}
          onBackButtonPress={this.onIconBackButtonPress}
          isVisible={this.state.isIconModalVisible}>
          <View style={{
            overflow: 'hidden',
            flex: 0.7,
            backgroundColor: 'white',
            borderRadius: wp(3),
            borderColor: 'white'}}>
            <LinearGradient
              colors={this.state.type == "expense" ? EXPENSE_GRADIENT_COLOR : INCOME_GRADIENT_COLOR}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{
                paddingTop: hp(2),
                paddingBottom: hp(2),
                paddingLeft: wp(4),
                paddingRight: wp(4),
                borderBottomWidth: 0.5}}>
                <Text style={{fontSize: hp(3), color: 'white', letterSpacing: 1}}>{this.state.type == "expense" ? "EXPENSES": "INCOME"}</Text>
            </LinearGradient>
            <View style={{flex: 1}}>
              <FlatList
                data={this.formatData(this.state.type == "expense" ? expensesCategoryIcon : incomeCategoryIcon, 3)}
                style={{flex: 1}}
                renderItem={this.renderItem}
                numColumns={3}
                keyExtractor={(item, index) => index}
              />
            </View>
          </View>
        </Modal>

        <Animatable.View animation={'fadeInUp'} style={{flex:9}}>
          <View style={{flex:1}}>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', paddingRight: wp(10), paddingLeft: wp(10)}}>
              {
                (!transactionDetail && !categoryType) &&
                <SwitchSelector
                  fontSize={hp(3)}
                  animationDuration={50}
                  initial={0}
                  onPress={this.selectType}
                  textColor={'black'} //'#7a44cf'
                  selectedColor={'white'}
                  buttonColor={this.state.type == "expense" ? '#FF416C' : '#02aab0'}
                  backgroundColor={'rgba(255,255,255, 0.2)'}
                  borderColor={'rgba(255,255,255, 0.2)'}
                  hasPadding
                  options={[
                      { label: 'EXPENSE', value: 'expense'},
                      { label: 'INCOME', value: 'income'}
                  ]} />
              }
              {
                (transactionDetail && categoryType) &&
                  <Text style={{fontSize: hp(4), color: 'white', letterSpacing: 3}}>EDIT {transactionDetail.transactionType.toUpperCase()}</Text>
              }
              {
                categoryType &&
                  <Text style={{fontSize: hp(4), color: 'white', letterSpacing: 3}}>{categoryType.toUpperCase()}</Text>
              }
            </View>

            <TouchableOpacity onPress={this.toggleKeyboardView} style={{flex: 1, alignItems: 'center', justifyContent: 'center', paddingTop: hp(1),  paddingBottom: hp(1)}}>
              <Animatable.View
                onAnimationEnd={this.onAnimationEnd}
                animation={amountShakeAnimation}
                style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{width:"100%",textAlign:"center", color: 'white', fontSize: hp(4)}}>
                  $ {amount == '' ? '0' : amount}
                </Text>
              </Animatable.View>
            </TouchableOpacity>

            <TouchableOpacity onPress={this.toggleIconModal} style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingLeft: wp(3), paddingBottom: hp(1)}}>
              <Animatable.View
                onAnimationEnd={this.onAnimationEnd}
                animation={categoryShakeAnimation}
                style={{alignItems: 'center', justifyContent: 'center', width: wp(15)}}>
                <FontAwesome5 name={categoryIcon} color="#F0F8FF" size={hp(3)}/>
              </Animatable.View>
              <Animatable.View
                onAnimationEnd={this.onAnimationEnd}
                animation={categoryShakeAnimation}
                style={{flex: 1, alignItems: 'flex-start', justifyContent: 'center', paddingLeft: wp(2)}}>
                <Text style={{fontSize: hp(3), color: 'white'}}>{categoryName}</Text>
              </Animatable.View>
            </TouchableOpacity>

            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingLeft: wp(3), paddingBottom: hp(1)}}>
              <View style={{alignItems: 'center', justifyContent: 'center', width: wp(15)}}>
                <FontAwesome5 name={'calendar-alt'} color="white" size={hp(3)}/>
              </View>
              <TouchableOpacity style={{flex: 1, alignItems: 'flex-start', justifyContent: 'center', paddingLeft: wp(2)}} onPress={this.toggleCalendarModal}>
                <Text style={{fontSize: hp(3), color:'white'}}>{this.formatSelectedDateDisplay(selectedDate)}</Text>
              </TouchableOpacity>
            </View>

            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingLeft: wp(3), paddingBottom: hp(2)}}>
              <View style={{alignItems: 'center', justifyContent: 'center', width: wp(15)}}>
                <FontAwesome5 name={'pen'} color="white" size={hp(3)}/>
              </View>
              <View style={{flex: 1, alignItems: 'flex-start', justifyContent: 'center', paddingLeft: wp(2)}}>
                <TextInput
                  // autoCapitalize={'characters'}
                  multiline={true}
                  placeholder={"Extra Notes"}
                  placeholderTextColor={"rgba(255,255,255, 0.5)"}
                  placeholderStyle={{alignSelf:'center'}}
                  style={{fontSize: hp(3), color:'white', width:"100%", height: Math.max(hp(5), this.state.height)}}
                  maxLength={50}
                  onChangeText={(text) => this.setState({notes: text})}
                  onContentSizeChange={(event) => {
                      this.setState({ height: event.nativeEvent.contentSize.height })
                  }}
                  returnKeyType='done'
                  value={this.state.notes}
                  blurOnSubmit={true} //to dismiss keyboard on ios
                  underlineColorAndroid={"transparent"}
                />
              </View>
              {//Implement later
            /*<TouchableOpacity style={{alignItems: 'center', justifyContent: 'center', width: 25, height: 25}} onPress={this.selectPhoto}>
                <FontAwesome5 name={'image'} solid size={25}/>
              </TouchableOpacity>*/}
            </View>

            {// <View style={{ flex: 5, paddingTop: 0, alignItems: 'center', justifyContent: 'flex-start'}}>
            //   {this.state.imageSource === "" ? null :
            //     <ImageBackground resizeMode="cover" style={{flex: 1, width: DEVICE_WIDTH * 0.97, height: DEVICE_HEIGHT * 0.24}} source={this.state.imageSource}>
            //       <TouchableOpacity style={{alignItems:'flex-end', justifyContent:'center', padding: 5}} onPress={this.deletePhoto}>
            //         <FontAwesome5 style={{color:'white'}} name={'times-circle'} regular size={25}/>
            //       </TouchableOpacity>
            //     </ImageBackground>}
            // </View>
            }
          </View>
        </Animatable.View>

        <Animatable.View animation={'fadeInUp'} style={{borderTopRightRadius: 20, borderTopLeftRadius: 20, height: DEVICE_HEIGHT * 0.4, flexDirection: 'row', borderWidth: 0.1}}>
          <View style={{flex: 4, backgroundColor:'rgba(255,255,255, 0.2)'}}>
            <View style={{flex: 3}}>
              <FlatList
                data={this.state.keyboardDisplayText}
                renderItem={this.renderNumber}
                style={{flex:1}}
                numColumns={3}
                showsVerticalScrollIndicator={false}
                keyExtractor={(item, index) => item+index}/>
            </View>
            <View style={{flex: 1}}>
              <View style={{flex: 1,flexDirection: 'row', justifyContent:'center', alignItems:'center'}}>
                <TouchableOpacity onPress={()=>this.selectKeyboardInputValue('0')} style={{flex: 1 }}>
                  <Text style={{textAlign:'center', fontSize: hp(4), color:'white'}}>0</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>this.selectKeyboardInputValue('.')} style={{flex: 1}}>
                  <Text style={{textAlign:'center', fontSize: hp(4), color:'white'}}>.</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={{flex:1, backgroundColor:'rgba(255,255,255, 0.2)'}}>
            <TouchableOpacity
              onPress={()=>this.selectKeyboardInputValue('backspace')}
              onLongPress={()=>this.setState({
                amount: '',
                keyboardInputValue: ''
              })}
              style={{flex:1, alignItems: 'center', justifyContent: 'center'}}>
              <FontAwesome5 name="backspace" size={hp(4)} color='white'/>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.submitTransaction} style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <Text style={{textAlign:'center', fontSize: hp(3), color:'white'}}>{keyboardSubmitText}</Text>
            </TouchableOpacity>
          </View>
        </Animatable.View>
      </LinearGradient>
    );
  }
}

mapStateToProps = (state) => {
  const { addTransaction, updateTransaction, dailyTransaction, monthlyTransaction,monthList,recentTransaction,monthlyCategoryTransaction } = state;
  return {
    addTransaction,
    updateTransaction,
    dailyTransaction,
    monthlyTransaction,
    monthList,
    recentTransaction,
    monthlyCategoryTransaction
  }
}

mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Object.assign({},
      addTransactionAction,dailyTransactionAction, monthlyTransactionAction,
      monthListAction,updateTransactionAction, recentTransactionAction,
      monthlyCategoryTransactionAction), dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewExpenses);
