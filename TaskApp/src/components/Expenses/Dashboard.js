import React, { PureComponent } from "react";
import {
  View,
  Text,
  FlatList,
  Dimensions,
  TouchableOpacity,
  Animated,
  Platform,
  ScrollView
} from "react-native";
import Carousel from 'react-native-snap-carousel';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AnimatedBar from './AnimatedBar';
import Modal from "react-native-modal";
import { CalendarList } from 'react-native-calendars';
import {getMonthName, getDayName, getMonthInTwoDigit, getDateInTwoDigit, getDateOrdinal } from '@Utils/dateUtils';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '@Utils/scalingUtils';
import {GRADIENT_COLOR, GRADIENT_START, GRADIENT_END, DEVICE_WIDTH,DEVICE_HEIGHT,SHORT_DATE_NAME,FULL_DATE_NAME, EXPENSES_CATEGORY, INCOME_CATEGORY} from '@Constants/constant';
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import CalendarModal from '@ExpensesCommonComponents/CalendarModal';
import DeleteModal from '@ExpensesCommonComponents/DeleteModal';
import TransactionDetailModal from '@ExpensesCommonComponents/TransactionDetailModal';
import Swipeable from 'react-native-swipeable';
import hasNoRecord from '@HOC/hasNoRecord';
import realm from '@Schema/schema';
import * as monthlyTransactionAction from "@ExpensesAction/monthlyTransaction";
import * as monthListAction from "@ExpensesAction/monthList";
import * as transactionDetailAction from "@ExpensesAction/transactionDetail";
import * as dailyTransactionAction from "@ExpensesAction/dailyTransaction";
import * as recentTransactionAction from "@ExpensesAction/recentTransaction";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

const NoRecordDefaultView = hasNoRecord(View);
const AnimatableTouchableOpacity = Animatable.createAnimatableComponent(TouchableOpacity);

class Dashboard extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      expensesBorderColor: '#F06292',
      incomeBorderColor: null,
      isCalendarModalVisible: false,
      currentYear: new Date().getFullYear(),
      currentMonth: getMonthName(SHORT_DATE_NAME, new Date().getMonth()),
      currentDate: new Date().getDate(),
      currentDay: getDayName(SHORT_DATE_NAME, new Date().getDay()),
      markedTodayDate: new Date().getFullYear() + "-" + getMonthInTwoDigit((new Date().getMonth()+1)) + "-" + getDateInTwoDigit(new Date().getDate()),
      selectedDate: new Date().setHours(0, 0, 0, 0),
      activeSlide:0,
      isOptionModalVisible: false,
      isDeleteModalVisible: false,
      isDeleteTrigger: false,
      monthlyTransaction: [],
      monthList: [],
      totalMonthlySpent: 0,
      totalMonthlyIncome: 0,
      totalMonthlySpentLineBar: 0,
      totalMonthlyIncomeLineBar: 0,
      selectedMonth: '',
      transactionDetail: {},
      touchableOpacityAnimation: 'zoomIn',
      currentlyOpenSwipeable: null,
      isSwiping: true
    }
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerRight: (<FontAwesome5 onPress={()=>navigation.navigate('Hamburger')} name={'bars'} color={'white'} size={25} style={{padding: 20}}/>),
    };
  };

  componentDidMount() {
    const {currentYear, currentDate} = this.state;
    let currentMonth = (new Date().getMonth() + 1) + '';
    let day = currentDate + '';

    if (currentMonth.length < 2) currentMonth = '0' + currentMonth;
    if (day.length < 2) day = '0' + day;

    const date = currentYear + '-' + currentMonth + '-' +  day;

    //const month = getMonthName(SHORT_DATE_NAME, new Date(date).getMonth()) + ' ' + new Date(date).getFullYear();
    const month = new Date().getFullYear() + '-' + getMonthInTwoDigit((new Date().getMonth()+1))
    this.setState({
      selectedDate: date
    })
    //console.log(month);
    //this.props.actions.dailyTransactionRequest({date});
    this.props.actions.monthlyTransactionRequest({month});
    this.props.actions.monthListRequest({month});
    this.props.actions.recentTransactionRequest({date})
  }

    componentDidUpdate(prevProps){
      if(prevProps.monthList !== this.props.monthList){
      const monthList = this.props.monthList.response.sort((a,b) => (a.transactionMonth > b.transactionMonth) ? 1 : ((b.transactionMonth > a.transactionMonth) ? -1 : 0));
      this.setState({
        monthList: monthList,
        selectedMonth: this.props.monthList.response[this.props.monthList.response.length-1].transactionMonth,
      })
      //snap to last item
      setTimeout(() => {this.monthCarousel.snapToItem(monthList.length,false)},500 );
    }

    if (prevProps.monthlyTransaction !== this.props.monthlyTransaction){
      const { response, totalMonthlySpent, totalMonthlyIncome } = this.props.monthlyTransaction;
      console.log('response:', Array.from(response));
      this.setState({
        monthlyTransaction: response,
        totalMonthlySpent: totalMonthlySpent,
        totalMonthlyIncome: totalMonthlyIncome,
        totalMonthlySpentLineBar: (parseFloat(totalMonthlySpent)/(parseFloat(totalMonthlySpent)+parseFloat(totalMonthlyIncome)))*100,
        totalMonthlyIncomeLineBar: (parseFloat(totalMonthlyIncome)/(parseFloat(totalMonthlySpent)+parseFloat(totalMonthlyIncome)))*100,
      }, () => {
        this.categoryListRef.scrollToOffset({animated: true, offset: 0});
      })
    }

    if(prevProps.transactionDetail !== this.props.transactionDetail){
      //alert(JSON.stringify(this.props.transactionDetail))
      this.setState({
        transactionDetail: this.props.transactionDetail.response
      })
    }

    // if(prevProps.dailyTransaction !== this.props.dailyTransaction){
    //   const {response} = this.props.dailyTransaction;
    //   if(response.length > 0){
    //     this.setState({
    //       selectedDate: response[0].transactionDate
    //     })
    //   }
    // }
  }

  toggleTransactionDetailModal = (item) => {
    this.setState({ isOptionModalVisible: !this.state.isOptionModalVisible });
    this.props.actions.transactionDetailRequest({
      id: item.id
    })
  }
  onTransactionDetailBackdropPress = () => this.setState({ isOptionModalVisible: !this.state.isOptionModalVisible });
  onTransactionDetailBackButtonPress = () => this.setState({ isOptionModalVisible: !this.state.isOptionModalVisible });
  onTransactionDetailModalHide = () => {
    if(this.state.isDeleteTrigger){
      this.setState({
        isDeleteModalVisible: !this.state.isDeleteModalVisible
      })
    }
  }

  //for swipe to delete
  confirmDelete = (item) => {
    const { currentlyOpenSwipeable, isDeleteModalVisible } = this.state;
    this.setState({
      isDeleteModalVisible: !isDeleteModalVisible,
      transactionDetail: item
    }, () => {
      currentlyOpenSwipeable.recenter();
    });
  }
  //for swipe to edit
  editTransaction = (item) => {
    const { currentlyOpenSwipeable } = this.state;
    this.props.navigation.navigate('NewExpenses', {transactionDetail: item})
    currentlyOpenSwipeable.recenter();
  }

  handleScroll = () => {
    const { currentlyOpenSwipeable } = this.state;

    if (currentlyOpenSwipeable) {
      currentlyOpenSwipeable.recenter();
    }
  }

  toggleDeleteModal = () => {
    this.setState({
      isOptionModalVisible: !this.state.isOptionModalVisible, //to trigger option modal hide and show delete modal
      isDeleteTrigger: !this.state.isDeleteTrigger
    });
  }
  onDeleteBackdropPress = () => {
    this.setState({
      isDeleteModalVisible: !this.state.isDeleteModalVisible,
      isDeleteTrigger: false
    });
  }
  onDeleteBackButtonPress = () => this.setState({
    isDeleteModalVisible: !this.state.isDeleteModalVisible,
    isDeleteTrigger: false
  });

  toggleCalendarModal = () => this.setState({ isCalendarModalVisible: !this.state.isCalendarModalVisible });
  onCalendarBackdropPress = () => this.setState({ isCalendarModalVisible: !this.state.isCalendarModalVisible });
  onCalendarBackButtonPress = () => this.setState({ isCalendarModalVisible: !this.state.isCalendarModalVisible });

  // onDayPress = (day) => {
  //   this.setState({
  //     selectedDate: day.dateString,
  //     isCalendarModalVisible: !this.state.isCalendarModalVisible
  //   })
  //   this.props.actions.dailyTransactionRequest({
  //     date: day.dateString
  //   });
  // }

  renderCarouselItem = ({ item, index }) => {
    const month = getMonthName(SHORT_DATE_NAME, new Date(item.transactionMonth).getMonth()) + ' ' + new Date(item.transactionMonth).getFullYear();
    return (
      <Animatable.View animation={'slideInLeft'}>
        <Text style={{fontSize: hp(3), letterSpacing: 0.5,textAlign: 'center', color: 'white'}}>{month}</Text>
      </Animatable.View>
    );
  };

  renderCarouselCategoryItem = ({ item, index }) => {
    const {totalMonthlySpent, totalMonthlyIncome, selectedMonth} = this.state
    let item0LineBar = 0;
    let item1LineBar = 0;
    let item2LineBar = 0;
    let item3LineBar = 0;
    const total = parseFloat(totalMonthlySpent)+parseFloat(totalMonthlyIncome)
    if(total > 0){
      item0LineBar = (typeof item[0] === 'undefined') ? null : item[0].amount/total*100;
      item1LineBar = (typeof item[1] === 'undefined') ? null : item[1].amount/total*100;
      item2LineBar = (typeof item[2] === 'undefined') ? null : item[2].amount/total*100;
      item3LineBar = (typeof item[3] === 'undefined') ? null : item[3].amount/total*100;
    }

    const item0BorderColor = (typeof item[0] === 'undefined') ? null : item[0].transactionType == 'expense' ? 'red' : '#00cdac';
    const item1BorderColor = (typeof item[1] === 'undefined') ? null : item[1].transactionType == 'expense' ? 'red' : '#00cdac';
    const item2BorderColor = (typeof item[2] === 'undefined') ? null : item[2].transactionType == 'expense' ? 'red' : '#00cdac';
    const item3BorderColor = (typeof item[3] === 'undefined') ? null : item[3].transactionType == 'expense' ? 'red' : '#00cdac';

    return (
      <Animatable.View animation={'slideInRight'} style={{flex: 1}}>
        <View style={{flex: 1, flexDirection: 'row'}}>
          { item[0] &&
            <View delay={0} style={{flex: 0.48, backgroundColor: '#EA5455', marginBottom: 10, borderRadius: 10}}>
              <TouchableOpacity
                onPress={()=>this.props.navigation.navigate(
                  'MonthlyCategoryDetails', {
                    monthlyCategoryDetail: item[0],
                    selectedMonth: selectedMonth,
                  })}>
                <View style={{flexDirection: 'row', padding: 15, paddingBottom: 0}}>
                  <View style={{width: item0LineBar+'%', borderTopWidth: 1, borderBottomWidth: 1, borderColor:item0BorderColor}} />
                  <View style={{width: (100-item0LineBar)+'%', borderTopWidth: 1, borderBottomWidth: 1, borderColor: '#F0F8FF'}} />
                </View>
                <Text style={{fontSize: 15, textAlign:'left', paddingLeft: 15, paddingTop: 10, paddingBottom: 15, color:'black'}}>$ {item[0].amount}</Text>
                <View style={{flexDirection: 'row', alignItems:'center', paddingLeft: 15}}>
                  <FontAwesome5 name={item[0].icon} color="black" size={12} style={{paddingRight:5}}/>
                  <Text style={{flex: 1, fontSize: 10, textAlign:'left', letterSpacing: 1, color:'black'}}>{item[0].name}</Text>
                </View>
              </TouchableOpacity>
            </View>
          }
          <View style={{flex:0.04}}/>
          { item[1] &&
            <View style={{flex: 0.48, backgroundColor: '#28C76F', marginBottom: 10, borderRadius: 10}}>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('MonthlyCategoryDetails', {monthlyCategoryDetail: item[1],selectedMonth: selectedMonth})}>
              <View style={{flexDirection: 'row', padding: 15, paddingBottom: 0}}>
                <View style={{width: item1LineBar+'%', borderTopWidth: 1, borderBottomWidth: 1, borderColor:item1BorderColor}} />
                <View style={{width: (100-item1LineBar)+'%', borderTopWidth: 1, borderBottomWidth: 1, borderColor: '#F0F8FF'}} />
              </View>
              <Text style={{fontSize: 15, textAlign:'left', paddingLeft: 15, paddingTop: 10, paddingBottom: 15, color:'black'}}>$ {item[1].amount}</Text>
              <View style={{flexDirection: 'row', alignItems:'center', paddingLeft: 15}}>
                <FontAwesome5 name={item[1].icon} color="black" size={12} style={{paddingRight:5}}/>
                <Text style={{flex: 1, fontSize: 10, textAlign:'left', letterSpacing: 1, color:'black'}}>{item[1].name}</Text>
              </View>
            </TouchableOpacity>
            </View>
          }
        </View>
        <View style={{flex: 1, flexDirection: 'row'}}>
          { item[2] &&
            <View style={{flex: 0.48, backgroundColor: '#F8D800', marginBottom: 10, borderRadius: 10}}>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('MonthlyCategoryDetails', {monthlyCategoryDetail: item[2],selectedMonth: selectedMonth})}>
              <View style={{flexDirection: 'row', padding: 15, paddingBottom: 0}}>
                <View style={{width: item2LineBar+'%', borderTopWidth: 1, borderBottomWidth: 1, borderColor:item2BorderColor}} />
                <View style={{width: (100-item2LineBar)+'%', borderTopWidth: 1, borderBottomWidth: 1, borderColor: '#F0F8FF'}} />
              </View>
              <Text style={{fontSize: 15, textAlign:'left', paddingLeft: 15, paddingTop: 10, paddingBottom: 15, color:'black'}}>$ {item[2].amount}</Text>
              <View style={{flexDirection: 'row', alignItems:'center', paddingLeft: 15}}>
                <FontAwesome5 name={item[2].icon} color="black" size={12} style={{paddingRight:5}}/>
                <Text style={{flex: 1, fontSize: 10, textAlign:'left', letterSpacing: 1, color:'black'}}>{item[2].name}</Text>
              </View>
            </TouchableOpacity>
            </View>
          }
          <View style={{flex:0.04}} />
          { item[3] &&
            <View style={{flex: 0.48, backgroundColor: '#0396FF', marginBottom: 10, borderRadius: 10}}>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('MonthlyCategoryDetails', {monthlyCategoryDetail: item[3],selectedMonth: selectedMonth})}>
              <View style={{flexDirection: 'row', padding: 15, paddingBottom: 0}}>
                <View style={{width: item3LineBar+'%', borderTopWidth: 1, borderBottomWidth: 1, borderColor:item3BorderColor}} />
                <View style={{width: (100-item3LineBar)+'%', borderTopWidth: 1, borderBottomWidth: 1, borderColor: '#F0F8FF'}} />
              </View>
              <Text style={{fontSize: 15, textAlign:'left', paddingLeft: 15, paddingTop: 10, paddingBottom: 15, color:'black'}}>$ {item[3].amount}</Text>
              <View style={{flexDirection: 'row', alignItems:'center', paddingLeft: 15}}>
                <FontAwesome5 name={item[3].icon} color="black" size={12} style={{paddingRight:5}}/>
                <Text style={{flex: 1, fontSize: 10, textAlign:'left', letterSpacing: 1, color:'black'}}>{item[3].name}</Text>
              </View>
            </TouchableOpacity>
            </View>
          }
        </View>
      </Animatable.View>
    )
  }

  onRightButtonsOpenRelease = (event, gestureState, swipeable) => {
    const { currentlyOpenSwipeable } = this.state;
    if (currentlyOpenSwipeable && currentlyOpenSwipeable !== swipeable) {
      currentlyOpenSwipeable.recenter();
    }
    this.setState({currentlyOpenSwipeable: swipeable});
  }

  onRightButtonsCloseRelease = () => {this.setState({currentlyOpenSwipeable: null})}

  renderTransactionItem = ({ item, index }) => {
    const symbol = item.transactionType == 'expense' ? '-' : '+';
    return (
      <Swipeable
        onRef={ref => this.swipeable = ref}
        onRightButtonsOpenRelease={this.onRightButtonsOpenRelease}
        onRightButtonsCloseRelease ={this.onRightButtonsCloseRelease}
        onSwipeStart={() => this.setState({isSwiping: false})}
        onSwipeRelease={() => this.setState({isSwiping: true})}
        rightButtons={[
          <View
            style={{
              flex: 1,
              backgroundColor:'transparent',
              flexDirection: 'row',
              alignItems:'center',
              marginBottom: hp(1),
            }}>
            <TouchableOpacity
              onPress={()=>this.editTransaction(item)}
              style={{
                alignItems: 'center',
                justifyContent:'center',
                marginLeft: wp(3),
                width: wp(15),
                height: wp(15),
                borderRadius: wp(7.5),
                backgroundColor:'white'}}>
              <FontAwesome5 name={'edit'} size={hp(3)} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={()=>this.confirmDelete(item)}
              style={{
                alignItems: 'center',
                justifyContent:'center',
                marginLeft: wp(3),
                width: wp(15),
                height: wp(15),
                borderRadius: wp(7.5),
                backgroundColor:'#ED5E68'}}>
              <FontAwesome5 name={'trash-alt'} size={hp(3)} color={'white'}/>
            </TouchableOpacity>
          </View>
        ]}
        rightButtonWidth={Math.floor(wp(40))}>
        <TouchableOpacity
          onLongPress={()=>this.setState({isOptionModalVisible: true})}
          style={{
            elevation: 5,
            flex:1,
            flexDirection: 'row',
            backgroundColor:'white',
            paddingTop: hp(2),
            paddingBottom: hp(2),
            paddingLeft: wp(4),
            paddingRight: wp(4),
            borderRadius: wp(3),
            marginBottom: hp(1)}}>
          <View style={{paddingBottom:hp(2)}}>
            <Text style={{fontSize: hp(2.5), alignSelf: 'center', color: 'black'}}>{new Date(item.transactionDate).getDate()}</Text>
            <Text style={{fontSize: hp(2.5), alignSelf: 'center', color: '#757575'}}>{getDayName(SHORT_DATE_NAME, new Date(item.transactionDate).getDay())}</Text>
          </View>
          <View style={{paddingRight: wp(4), paddingLeft: wp(4)}}>
            <View style={{width: 1, height:'100%', backgroundColor:'#E0E0E0'}}/>
          </View>
          <View style={{flex: 1}}>
            <View style={{flex:1, flexDirection: 'row', marginBottom: hp(1), alignItems: 'center', justifyContent:'center', }}>
              <FontAwesome5 name={item.categoryIcon} color= 'black' size={hp(3)} style={{paddingRight: wp(4)}}/>
              <Text style={{flex: 1, fontSize: hp(2), color: 'black'}}>{item.categoryName}</Text>
              {//<Text style={{fontSize: 15, color: item.transactionType == 'expense' ? '#f44336' : '#4CAF50'}}>{symbol}$ {item.amount}</Text>
              }
              <Text style={{fontSize: hp(2), color: item.transactionType == 'expense' ? '#f44336' : '#4CAF50'}}>$ {item.amount}</Text>
            </View>
            <View style={{flex:1, flexDirection: 'row', alignItems: 'center', justifyContent:'center', }}>
              <Text style={{flex: 1, fontSize: hp(2), color: '#757575'}}>{item.notes}</Text>
              <View style={{flex: 0.3}}/>
            </View>
          </View>
        </TouchableOpacity>
      </Swipeable>
    )
  }

  formatSelectedDateDisplay = (selectedDate) => {
    let currentDate = new Date().setHours(0, 0, 0, 0);
    let displayDay = new Date(selectedDate).setHours(0, 0, 0, 0) == currentDate ? "Today" : getDayName(FULL_DATE_NAME, (new Date(selectedDate).getDay()));
    let displayMonth = new Date(selectedDate).getMonth();
    let displayDate = new Date(selectedDate).getDate();
    return displayDay + ", " + displayDate + " " + getMonthName(FULL_DATE_NAME, displayMonth);
  }

  categoryRecord = (type) => {
    this.setState({
      expensesBorderColor: type == 'expenses' ? '#F06292' : null,
      incomeBorderColor: type == 'expenses' ? null : '#4DB6AC'
    })
  }

  convertToAbbrev = (number, precision) => {
    const abbrev = ['', 'k', 'm', 'b', 't'];
    const unrangifiedOrder = Math.floor(Math.log10(Math.abs(number)) / 3)
    const order = Math.max(0, Math.min(unrangifiedOrder, abbrev.length -1 ))
    const suffix = abbrev[order];
    return (number / Math.pow(10, order * 3)).toFixed(precision) + suffix;
  }

  renderCategory = ({item, index}) => {
    const { monthlyTransaction, totalMonthlySpent, totalMonthlyIncome, selectedMonth, touchableOpacityAnimation} = this.state
    return (
        <AnimatableTouchableOpacity
          animation={touchableOpacityAnimation} delay={index < 6 ? index*100 : 0}
          onPress={()=>this.props.navigation.navigate(
            'MonthlyCategoryDetails', {
              monthlyCategoryDetail: item,
              selectedMonth: selectedMonth,
            })}
          onAnimationEnd={() => {
            this.setState({
              touchableOpacityAnimation: ''
            })
          }}
          style = {{
            //borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
            borderRadius: wp(9),
            flex: 1,
            width: wp(23),
            height: wp(23),
            backgroundColor: 'white',
            justifyContent: 'center',
            alignItems: 'center',
            marginRight: index == monthlyTransaction.length-1 ? 0 : wp(2),
            elevation: 5
          }}>
            <FontAwesome5 style={{paddingBottom: hp(1), alignSelf:'center'}} name={item.icon} color="#616161" size={hp(3)}/>
            <Text style={{fontSize: hp(2), alignSelf:'center', color: 'black'}}>$ {this.convertToAbbrev(parseFloat(item.amount),2)}</Text>
      </AnimatableTouchableOpacity>
    )
  }

  getItemLayout = (data, index) => (
    { length: wp(23), offset: wp(23) * index, index }
  )

  toggleOptionModal = () => {
    this.setState({
      isOptionModalVisible: !this.state.isOptionModalVisible
    })
  }

  render() {
    const {currentYear,currentMonth,currentDate,currentDay,markedTodayDate, selectedDate,
           expensesBorderColor, incomeBorderColor, activeSlide, data, categoryData,monthList,
           monthlyTransaction, totalMonthlySpent, totalMonthlyIncome, isSwiping, isOptionModalVisible} = this.state
    const { recentTransaction } = this.props
    return (
      <LinearGradient
        start={GRADIENT_START} end={GRADIENT_END}
        colors={GRADIENT_COLOR} style={{flex: 1}}>

        {// <CalendarModal
        //   toggleModal={this.toggleCalendarModal}
        //   onBackdropPress={this.onCalendarBackdropPress}
        //   onBackButtonPress={this.onCalendarBackButtonPress}
        //   showModal={this.state.isCalendarModalVisible}
        //   onDayPress={this.onDayPress}
        //   selectedDate={selectedDate}
        // />
        }

        <DeleteModal
          toggleModal={this.toggleDeleteModal}
          onBackdropPress={this.onDeleteBackdropPress}
          onBackButtonPress={this.onDeleteBackButtonPress}
          showModal={this.state.isDeleteModalVisible}
          transactionDetail={this.state.transactionDetail}
        />

        {// <TransactionDetailModal
        //   toggleModal={this.onTransactionDetailBackdropPress}
        //   onBackdropPress={this.onTransactionDetailBackdropPress}
        //   onBackButtonPress={this.onTransactionDetailBackButtonPress}
        //   showModal={this.state.isOptionModalVisible}
        //   toggleDeleteModal={this.toggleDeleteModal}
        //   onModalHide={this.onTransactionDetailModalHide}
        //   transactionDetail={this.state.transactionDetail}
        //   {...this.props}
        // />
        }

        <Modal
          animationIn={'slideInUp'}
          onBackdropPress={this.toggleOptionModal}
          onBackButtonPress={this.toggleOptionModal}
          isVisible={isOptionModalVisible}
          swipeDirection='down'
          onSwipe={() => this.setState({isOptionModalVisible: !isOptionModalVisible})}
          style={{justifyContent: "flex-end", margin: 0}}>
          <View style={{backgroundColor:'white', height: hp(20), padding: hp(2)}}>
            <TouchableOpacity style={{flexDirection: 'row', paddingBottom: hp(2)}} onPress={()=>this.editTransaction(item)}>
              <FontAwesome5 name={'edit'} color="rgba(0,0,0, 0.7)" size={hp(3)} style={{paddingRight: wp(4)}}/>
              <Text style={{fontSize: hp(2), letterSpacing: 1, color:'black' }}>EDIT</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{flexDirection: 'row', paddingBottom: hp(2)}} onPress={this.props.toggleDeleteModal}>
              <FontAwesome5 name={'trash-alt'} color="rgba(0,0,0, 0.7)" size={hp(3)} style={{paddingRight: wp(4)}}/>
              <Text style={{fontSize: hp(2), letterSpacing: 1, color:'black'}}>DELETE</Text>
            </TouchableOpacity>
          </View>
        </Modal>

        <View>
          <Carousel
            initialNumToRender={monthList.length}
            ref={(c) => { this.monthCarousel = c; }}
            data={monthList}
            renderItem={this.renderCarouselItem}
            sliderWidth={wp(100)}
            itemWidth={wp(28)}
            layout={'default'}
            inactiveSlideOpacity={0.3}
            enableMomentum={true}
            onSnapToItem={(index) => {
                this.setState({selectedMonth: monthList[index].transactionMonth})
                this.props.actions.monthlyTransactionRequest({
                  month: monthList[index].transactionMonth,
                  activeSlide: index
                });
                //this.setState({ activeSlide: index })
              }
            }
            //firstItem={renderMonth.length-1}
          />
        </View>

        <Animatable.View animation={'zoomIn'} style={{paddingTop: hp(1), paddingLeft: wp(3), paddingRight: wp(3), paddingBottom: hp(1)}}>
          <TouchableOpacity
            onPress={()=>this.props.navigation.navigate('AllTransaction', {
              selectedDate: selectedDate,
              transactionType: 'expense' //default is Expense
            })}
            style={{
              paddingTop: hp(2),
              paddingBottom: hp(2),
              paddingLeft: wp(4),
              paddingRight: wp(4),
              borderRadius: wp(3),
              backgroundColor:'white',
              elevation: 5,
              alignItems: 'center',
              justifyContent: 'center'}}>
            {// <View style={{flexDirection: 'row'}}>
            //   <View style={{width: this.state.totalMonthlySpentLineBar+'%', borderTopWidth: 1, borderBottomWidth: 1, borderColor:'red'}} />
            //   <View style={{width: this.state.totalMonthlyIncomeLineBar+'%', borderTopWidth: 1, borderBottomWidth: 1, borderColor:'#00cdac'}} />
            // </View>
            }
            <View style={{flexDirection: 'row', backgroundColor:''}}>
              <View style={{flex:1}}>
                <Text style={{fontSize: hp(2), textAlign:'left', color:'black'}}>EXPENSES</Text>
                <Text style={{fontSize: hp(3), textAlign:'left', color:'black'}}>$ {parseFloat(totalMonthlySpent).toFixed(2)}</Text>
              </View>
              <View style={{flex:1}}>
                <Text style={{fontSize: hp(2), textAlign:'right', color:'black'}}>INCOME</Text>
                <Text style={{fontSize: hp(3), textAlign:'right', color:'black'}}>$ {parseFloat(totalMonthlyIncome).toFixed(2)}</Text>
              </View>
            </View>
          </TouchableOpacity>
        </Animatable.View>

        <View>
          {// <Carousel
          //   ref={(c) => { this._carousel = c; }}
          //   data={monthlyTransaction}
          //   renderItem={this.renderCarouselCategoryItem}
          //   sliderWidth={DEVICE_WIDTH}
          //   itemWidth={DEVICE_WIDTH}
          //   layout={'default'}
          //   inactiveSlideOpacity={0.3}
          //   inactiveSlideShift={20}
          //   slideStyle={{
          //     paddingRight: 15,
          //     paddingLeft: 15
          //   }}
          //   //onSnapToItem={(index) => this.setState({ activeSlide: index }) }
          // />
          }
          <FlatList
            ref={(ref) => { this.categoryListRef = ref; }}
            getItemLayout={this.getItemLayout}
            contentContainerStyle={{
              paddingLeft: wp(3),
              paddingRight: wp(3),
              paddingBottom: hp(1),
              justifyContent: 'center',
            }}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            data={monthlyTransaction}
            renderItem={this.renderCategory}
            keyExtractor={(item, index) => item+index}
          />
        </View>

        <View>
          <View style={{height: 1, width:'100%', backgroundColor:'white'}}/>
        </View>

        <Animatable.View
          animation={'slideInUp'}
          delay={500}
          style={{
            flexDirection: 'row',
            backgroundColor:'transparent',
            padding: wp(3),
            paddingTop: hp(1),
            paddingBottom: 0}}>
          {// <Text style={{fontSize: 15, paddingBottom: 20, paddingTop: 10, fontWeight: 'bold', color: 'white'}}>{this.formatSelectedDateDisplay(selectedDate)}</Text>
          }
          <Text style={{fontSize: hp(2), fontWeight: 'bold', color: 'white'}}>Today { recentTransaction.response.length > 1 ? 'Activities' : 'Activity'}</Text>
          <View style={{flex: 1, alignItems:'flex-end'}}>
            {// <FontAwesome5 name={'calendar-alt'} color= 'white' size={20}/>
            }
            <Text
              onPress={() => this.props.navigation.navigate('AllTransaction', {
                selectedDate: selectedDate,
                transactionType: 'expense' //default is Expense
              })}
              style={{fontSize: hp(2), fontWeight: 'bold', color: 'white'}}>See All</Text>
          </View>
        </Animatable.View>

        <Animatable.View
          animation={recentTransaction.response.length > 0 ? 'slideInUp':'zoomIn'}
          delay={recentTransaction.response.length > 0 ? 500:1200}
          style={{
            flex: 3,
            paddingTop: hp(1),
            paddingLeft: wp(3),
            paddingRight: wp(3)}}>

            <FlatList
              scrollEnabled={isSwiping}
              onScroll = {this.handleScroll}
              data={recentTransaction.response}
              style={{flex: 1}}
              renderItem={this.renderTransactionItem}
              keyExtractor={(item, index) => item+index}
              showsVerticalScrollIndicator={false}
            />
        </Animatable.View>

        {// <TouchableOpacity onPress={()=>this.props.navigation.navigate('NewExpenses')} style={{marginBottom: 10, position:'absolute', bottom: 0, justifyContent:'center', alignSelf:'center', backgroundColor:'white', borderRadius: DEVICE_WIDTH/2, height: DEVICE_WIDTH*0.15, width: DEVICE_WIDTH*0.15}}>
        //   <Animatable.View animation={'zoomIn'} >
        //     <FontAwesome5 name={'plus'} color= 'black' size={25} style={{alignSelf:'center'}} />
        //   </Animatable.View>
        // </TouchableOpacity>
        }


      </LinearGradient>
    );
  }
}

mapStateToProps = (state) => {
  const { dailyTransaction, monthlyTransaction, monthList, transactionDetail, recentTransaction} = state;
  return {
    monthlyTransaction,
    monthList,
    transactionDetail,
    dailyTransaction,
    recentTransaction
  }
}

mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Object.assign({},
      monthlyTransactionAction, monthListAction,
      transactionDetailAction, dailyTransactionAction,
      recentTransactionAction), dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
