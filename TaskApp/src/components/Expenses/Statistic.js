import React, { PureComponent } from "react";
import {
  View,
  Text,
  FlatList,
  Dimensions,
  TouchableOpacity,
  Animated
} from "react-native";
import Carousel from 'react-native-snap-carousel';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AnimatedBar from './AnimatedBar';

class Statistic extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      flatListData:[{iconName: 'utensils', iconLabel: 'Foods and Drinks', amount: '20.00'},
                      {iconName: 'shopping-cart', iconLabel: 'Shopping', amount: '20.00'},
                      {iconName: 'shopping-cart', iconLabel: 'Shopping', amount: '20.00'},
                      {iconName: 'shopping-cart', iconLabel: 'Shopping', amount: '20.00'},
                      {iconName: 'shopping-cart', iconLabel: 'Shopping', amount: '20.00'},
                      {iconName: 'shopping-cart', iconLabel: 'Shopping', amount: '20.00'},
                      {iconName: 'shopping-cart', iconLabel: 'Shopping', amount: '20.00'},
                      {iconName: 'shopping-cart', iconLabel: 'Shopping', amount: '20.00'},
                      {iconName: 'shopping-cart', iconLabel: 'Shopping', amount: '20.00'},
                      {iconName: 'shopping-cart', iconLabel: 'Shopping', amount: '20.00'},
                      {iconName: 'utensils', iconLabel: 'Foods and Drinks', amount: '20.00'},
                      {iconName: 'utensils', iconLabel: 'Foods and Drinks', amount: '20.00'}],
      carouselData: [{a:'January'},{a:'February'},{a:'March'},{a:'April'},{a:'May'},{a:'June'},
                     {a:'July'},{a:'August'},{a:'September'},{a:'October'},{a:'November'},{a:'December'}],
      expensesBorderColor: '#F06292',
      incomeBorderColor: null,
      chartData: [
        {label: 'Groceries', amount: 500, color: '#F48FB1'},
        {label: 'Shopping', amount: 50, color: '#CE93D8'},
        {label: 'Food', amount: 120, color: '#B39DDB'},
        {label: 'Entertaiment', amount: 80, color: '#9FA8DA'},
        {label: 'Bills', amount: 251, color: '#90CAF9'},
        {label: 'Sport', amount: 250, color: '#81D4FA'},
      ]
    }
  }

  renderCarouselItem = ({ item, index }) => {
    return (
      <View style={{padding: 10}} key={index}>
        <Text style={{fontSize: 15, letterSpacing: 0.5,textAlign: 'center'}}>{item.a} 2018</Text>
        <Text style={{fontSize: 25, letterSpacing: 1.5, fontWeight: 'bold', textAlign: 'center'}}>MYR 200.00</Text>
      </View>
    );
  };

  renderItem = ({ item, index }) => {
    return (
      <View style={{}} key={index}>
       <Text style={{fontSize: 15, letterSpacing: 0.5, padding: 5, fontWeight: 'bold'}}>{item.label}</Text>
       <AnimatedBar value={item.amount} color={item.color} delay={0} />
      </View>
    )
  }

  categoryRecord = (type) => {
    this.setState({
      expensesBorderColor: type == 'expenses' ? '#F06292' : null,
      incomeBorderColor: type == 'expenses' ? null : '#4DB6AC'
    })
  }

  render() {
    const {flatListData, carouselData, expensesBorderColor, incomeBorderColor, chartData} = this.state
    return (
      <View style={{flex: 1}}>
        <View style={{}}>
          <Carousel
            ref={(c) => { this._carousel = c; }}
            data={carouselData}
            renderItem={this.renderCarouselItem}
            sliderWidth={DEVICE_WIDTH}
            itemWidth={DEVICE_WIDTH * 0.5}
            layout={'default'}
            inactiveSlideOpacity={0.3}
          />
        </View>
        <View style={{flex: 7.5}}>
        <View style={{flex: 1}}>
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
              onPress={()=>this.categoryRecord('expenses')}
              style={{flex:1, alignItems: 'center', justifyContent: 'center', padding: 10, borderBottomWidth: expensesBorderColor ? 3 : 0, borderBottomColor: expensesBorderColor}}>
                <Text style={{fontSize: 15, textAlign: 'center'}}>Expenses</Text>
                <Text style={{fontSize: 15, textAlign: 'center'}}>MYR 88.00</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={()=>this.categoryRecord('income')}
              style={{flex:1, alignItems: 'center', justifyContent: 'center', padding: 10, borderBottomWidth: incomeBorderColor ? 3 : 0, borderBottomColor: incomeBorderColor}}>
                <Text style={{fontSize: 15, textAlign: 'center'}}>Income</Text>
                <Text style={{fontSize: 15, textAlign: 'center'}}>MYR 0.00</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{flex: 6.5, padding: 5}}>
          <FlatList
            data={chartData}
            renderItem={this.renderItem}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item, index) => item+index}
          />
        </View>
        </View>
      </View>
    );
  }
}

export default Statistic;
