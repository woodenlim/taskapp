import React, { PureComponent } from "react";
import {
  View,
  Text,
  FlatList,
  Switch,
  TouchableOpacity,
  Alert
} from "react-native";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import LinearGradient from 'react-native-linear-gradient';
import {GRADIENT_COLOR,DEVICE_HEIGHT} from '@Constants/constant';
import * as Animatable from 'react-native-animatable';
import FingerprintScanner from 'react-native-fingerprint-scanner';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '@Utils/scalingUtils';

class Fingerprint extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      keyboardDisplayText: [
        {value: '1'},{value: '2'},{value: '3'},
        {value: '4'},{value: '5'},{value: '6'},
        {value: '7'},{value: '8'},{value: '9'},
        {value: 'fingerprint'},{value: '0'},{value: 'backspace'}],
      fingerprintErrorMessage: 'TOUCH SENSOR',
      shakeAnimation: ''
    }
  }

  //different OS different Fingerprint method
  componentDidMount(){
    const { navigate } = this.props.navigation
    FingerprintScanner
      .authenticate({ onAttempt: this.handleAuthenticationAttempted })
      .then(() => {
        navigate('Dashboard')
      })
      .catch((error) => {
        FingerprintScanner.release();//To fixed fingerprint not release if AuthenticationFailed
        if(error.name == 'DeviceLocked' || error.name == 'AuthenticationFailed'){
          this.setState({
            fingerprintErrorMessage: 'PLEASE TRY AGAIN LATER',
          })
        }else{
          this.setState({
            fingerprintErrorMessage: 'TOUCH SENSOR',
          })
        }
      });
  }

  componentWillUnmount() {
    FingerprintScanner.release();
  }

  handleAuthenticationAttempted = (error) => {
    let errorMsg = ''
    if(error.name == 'DeviceLocked' || error.name == 'AuthenticationFailed'){
      errorMsg = 'PLEASE TRY AGAIN LATER'
    }else if(error.name == 'AuthenticationNotMatch'){
      errorMsg = 'NO MATCH'
    }

    this.setState({
      fingerprintErrorMessage: errorMsg,
      shakeAnimation: 'shake'
    })
  };

  onAnimationEnd = () => {
    this.setState({
      shakeAnimation: '',
    })
  }

  render() {
    const { fingerprintErrorMessage, shakeAnimation } = this.state;
    return (
      <LinearGradient
        colors={GRADIENT_COLOR} style={{flex: 1}}>
        <Animatable.View animation={'zoomIn'} style={{flex:6, alignItems:'center',justifyContent:'center'}}>
          <View style={{width: wp(60), height: wp(60), alignItems:'center', justifyContent:'center', backgroundColor:'rgba(255,255,255, 0.2)', borderRadius: wp(30)}}>
            <View style={{width: wp(50), height: wp(50), alignItems:'center', justifyContent:'center', backgroundColor:'rgba(255,255,255, 0.8)', borderRadius: wp(25)}}>
              <MaterialCommunityIcons name={'fingerprint'} size={hp(20)} color='black'/>
            </View>
          </View>
        </Animatable.View>
        <Animatable.View animation={'slideInUp'} style={{flex:2, alignItems: 'center', justifyContent: 'center'}}>
          <Animatable.Text
            onAnimationEnd={this.onAnimationEnd}
            animation={shakeAnimation}
            style={{fontSize: hp(3), color:'white', letterSpacing: 1}}>{fingerprintErrorMessage}</Animatable.Text>
        </Animatable.View>
        <Animatable.View animation={'slideInUp'} style={{flex:2, alignItems: 'center', justifyContent: 'center'}}>
          <Text onPress={()=>this.props.navigation.navigate('Passcode')} style={{fontSize: hp(2), color:'white', letterSpacing: 2}}>Enter your passcode?</Text>
        </Animatable.View>
      </LinearGradient>
    );
  }
}

export default Fingerprint;
