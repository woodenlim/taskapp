import React, { PureComponent } from "react";
import {
  View,
  Text,
  FlatList,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  Alert
} from "react-native";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Carousel from 'react-native-snap-carousel';
import LinearGradient from 'react-native-linear-gradient';
import {GRADIENT_COLOR} from '@Constants/constant';
import * as Animatable from 'react-native-animatable';
import {getMonthName, getDayName, getMonthInTwoDigit, getDateInTwoDigit, getDateOrdinal } from '@Utils/dateUtils';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '@Utils/scalingUtils';
import Modal from "react-native-modal";
import { CalendarList } from 'react-native-calendars';
import {DEVICE_WIDTH,DEVICE_HEIGHT,SHORT_DATE_NAME,FULL_DATE_NAME} from '@Constants/constant';
import CalendarModal from '@ExpensesCommonComponents/CalendarModal';
import DeleteModal from '@ExpensesCommonComponents/DeleteModal';
import TransactionDetailModal from '@ExpensesCommonComponents/TransactionDetailModal';
import Swipeable from 'react-native-swipeable';
import hasNoRecord from '@HOC/hasNoRecord';
import * as monthlyCategoryTransactionAction from "@ExpensesAction/monthlyCategoryTransaction";
import * as transactionDetailAction from "@ExpensesAction/transactionDetail";
import * as monthlyTransactionAction from "@ExpensesAction/monthlyTransaction";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

const DefaultView = hasNoRecord(View);

class CategoryDetails extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      categoryDetail: this.props.navigation.getParam('monthlyCategoryDetail', ''),
      isCalendarModalVisible: false,
      isOptionModalVisible: false,
      isDeleteModalVisible: false,
      isMonthModalVisible: false,
      isDeleteTrigger: false,
      currentYear: new Date().getFullYear(),
      currentMonth: getMonthName(SHORT_DATE_NAME, new Date().getMonth()),
      currentDate: new Date().getDate(),
      currentDay: getDayName(SHORT_DATE_NAME, new Date().getDay()),
      markedTodayDate: new Date().getFullYear() + "-" + getMonthInTwoDigit((new Date().getMonth()+1)) + "-" + getDateInTwoDigit(new Date().getDate()),
      selectedDate: new Date().setHours(0, 0, 0, 0),
      selectedMonth: this.props.navigation.getParam('selectedMonth', ''),
      initialIndex: this.props.navigation.getParam('initialIndex', ''),
      monthlyCategoryTransaction: [],
      monthList: [],
      transactionDetail: {},
      monthlyTransaction: [],
      currentlyOpenSwipeable: null
    }
  }

  componentDidMount(){
    const {categoryDetail, selectedMonth} = this.state;
    //this.props.actions.monthlyTransactionRequest({month: selectedMonth});
    this.props.actions.monthlyCategoryTransactionRequest({
      month: selectedMonth,
      name: categoryDetail.name,
      icon: categoryDetail.icon
    })
  }

  componentDidUpdate(prevProps){
    const {selectedMonth} = this.state;
    if(prevProps.monthlyCategoryTransaction !== this.props.monthlyCategoryTransaction){
      const monthlyCategoryTransaction = this.props.monthlyCategoryTransaction.response;
      const categoryTransactionMonthList = this.props.monthlyCategoryTransaction.categoryTransactionMonthList;
      const monthList = categoryTransactionMonthList.sort((a,b) => (a.transactionMonth > b.transactionMonth) ? 1 : ((b.transactionMonth > a.transactionMonth) ? -1 : 0))

      this.setState({
        monthlyCategoryTransaction: monthlyCategoryTransaction,
        //monthList: categoryTransactionMonthList
        monthList: monthList,
      })

      //snap to specific month
      monthList.map((data, index) => {
        if(data.transactionMonth == selectedMonth){
          setTimeout(() => {this.monthCarousel.snapToItem(index,false)},500 );
        }
      })
    }

    if(prevProps.transactionDetail !== this.props.transactionDetail){
      //alert(JSON.stringify(this.props.transactionDetail))
      this.setState({
        transactionDetail: this.props.transactionDetail.response
      })
    }

    if (prevProps.monthlyTransaction !== this.props.monthlyTransaction){
      const { response } = this.props.monthlyTransaction;
      //console.log('response:', Array.from(response));
      this.setState({
        monthlyTransaction: response,
      })
    }
  }

  //for swipe to delete
  confirmDelete = (item) => {
    console.log('confirmDelete', item);
    const { isDeleteModalVisible, currentlyOpenSwipeable } = this.state;
    this.setState({
      isDeleteModalVisible: !isDeleteModalVisible,
      transactionDetail: item
    });
    currentlyOpenSwipeable.recenter();
  }
  //for swipe to edit
  editTransaction = (item) => {
    this.state.currentlyOpenSwipeable.recenter();
    this.props.navigation.navigate('NewExpenses', {transactionDetail: item})
  }

  toggleTransactionDetailModal = (item) => {
    this.setState({ isOptionModalVisible: !this.state.isOptionModalVisible });
    this.props.actions.transactionDetailRequest({
      id: item.id
    })
  }
  onTransactionDetailBackdropPress = () => this.setState({ isOptionModalVisible: !this.state.isOptionModalVisible });
  onTransactionDetailBackButtonPress = () => this.setState({ isOptionModalVisible: !this.state.isOptionModalVisible });
  onTransactionDetailModalHide = () => {
    if(this.state.isDeleteTrigger){
      this.setState({
        isDeleteModalVisible: !this.state.isDeleteModalVisible
      })
    }
  }

  toggleDeleteModal = () => {
    this.setState({
      isOptionModalVisible: !this.state.isOptionModalVisible, //to trigger option modal hide and show delete modal
      isDeleteTrigger: !this.state.isDeleteTrigger
    });
  }
  onDeleteBackdropPress = () => {
    this.setState({
      isDeleteModalVisible: !this.state.isDeleteModalVisible,
      isDeleteTrigger: false
    });
  }
  onDeleteBackButtonPress = () => this.setState({
    isDeleteModalVisible: !this.state.isDeleteModalVisible,
    isDeleteTrigger: false
  });

  toggleCalendarModal = () => this.setState({ isCalendarModalVisible: !this.state.isCalendarModalVisible });
  onCalendarBackdropPress = () => this.setState({ isCalendarModalVisible: !this.state.isCalendarModalVisible });
  onCalendarBackButtonPress = () => this.setState({ isCalendarModalVisible: !this.state.isCalendarModalVisible });

  toggleMonthModal = () => this.setState({ isMonthModalVisible: !this.state.isMonthModalVisible });
  onMonthBackdropPress = () => this.setState({ isMonthModalVisible: !this.state.isMonthModalVisible });
  onMonthBackButtonPress = () => this.setState({ isMonthModalVisible: !this.state.isMonthModalVisible });

  renderCarouselItem = ({ item, index }) => {
    const month = getMonthName(SHORT_DATE_NAME, new Date(item.transactionMonth).getMonth()) + ' ' + new Date(item.transactionMonth).getFullYear();
    return (
      <Animatable.View animation={'slideInLeft'}>
        <Text style={{fontSize: hp(3), letterSpacing: 0.5,textAlign: 'center', color: 'white'}}>{month}</Text>
      </Animatable.View>
    );
  };

  renderCategoryTitle = ({ item, index }) => {
    return (
      <Animatable.View animation={'fadeInDown'} style={{width:wp(100)}}>
        <View style={{flex:3, flexDirection:'row'}}>
          <Text style={{flex: 1, paddingLeft: wp(3), paddingBottom: hp(2), fontSize: hp(4), textAlign:'left', letterSpacing: 5, color:'white'}}>{item.iconLabel}</Text>
          <View style={{justifyContent:'center', alignItems:'center'}}>
            <FontAwesome5 name={item.iconName} color="white" size={hp(6)} style={{opacity:0.7}}/>
          </View>
        </View>
      </Animatable.View>
    )
  }

  renderDate = ({item, index}) => {
    return (
        <Animatable.View animation={'slideInLeft'}>
          <Text style={{fontSize: 18, letterSpacing: 0.5, color: 'white', textAlign: 'center'}}>{new Date(item.date).getDate()}</Text>
          <Text style={{fontSize: 15, letterSpacing: 0.5, color: 'white', textAlign: 'center'}}>{getDayName(SHORT_DATE_NAME, new Date(item.date).getDay())}</Text>
        </Animatable.View>
    );
  }

  navigateToDailyCategoryTransaction = (item) => {
    this.props.navigation.navigate('DailyCategoryDetails', {
      dailyCategoryDetail: item
    })
  }

  onRightButtonsOpenRelease = (event, gestureState, swipeable) => {
    const { currentlyOpenSwipeable } = this.state;
    if (currentlyOpenSwipeable && currentlyOpenSwipeable !== swipeable) {
      currentlyOpenSwipeable.recenter();
    }
    this.setState({currentlyOpenSwipeable: swipeable});
  }

  onRightButtonsCloseRelease = () => {this.setState({currentlyOpenSwipeable: null})}

  renderTransactionItem = ({ item, index }) => {
    const symbol = item.transactionType == 'expense' ? '-' : '+';
    return (
      <Swipeable
        onRef={ref => this.swipeable = ref}
        onRightButtonsOpenRelease={this.onRightButtonsOpenRelease}
        onRightButtonsCloseRelease ={this.onRightButtonsCloseRelease}
        rightButtons={[
          <View
            style={{
              flex: 1,
              backgroundColor:'transparent',
              flexDirection: 'row',
              alignItems:'center',
              marginBottom: hp(1),
            }}>
            <TouchableOpacity
              onPress={()=>this.editTransaction(item)}
              style={{
                alignItems: 'center',
                justifyContent:'center',
                marginLeft: wp(3),
                width: wp(15),
                height: wp(15),
                borderRadius: wp(15)/2,
                backgroundColor:'white'}}>
              <FontAwesome5 name={'edit'} size={hp(3)} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={()=>this.confirmDelete(item)}
              style={{
                alignItems: 'center',
                justifyContent:'center',
                marginLeft: wp(3),
                width: wp(15),
                height: wp(15),
                borderRadius: wp(15)/2,
                backgroundColor:'#ED5E68'}}>
              <FontAwesome5 name={'trash-alt'} size={hp(3)} color={'white'}/>
            </TouchableOpacity>
          </View>
        ]}
        rightButtonWidth={Math.floor(wp(40))}>
        <View
          style={{
            elevation: 5,
            flex:1,
            flexDirection: 'row',
            backgroundColor:'white',
            paddingTop: hp(2),
            paddingBottom: hp(2),
            paddingLeft: wp(4),
            paddingRight: wp(4),
            borderRadius: wp(3),
            marginBottom: hp(1)}}>
          <View style={{paddingBottom:hp(2)}}>
            <Text style={{fontSize: hp(2,5), alignSelf: 'center', color: 'black'}}>{new Date(item.transactionDate).getDate()}</Text>
            <Text style={{fontSize: hp(2.5), alignSelf: 'center', color: '#757575'}}>{getDayName(SHORT_DATE_NAME, new Date(item.transactionDate).getDay())}</Text>
          </View>
          <View style={{paddingRight: wp(4), paddingLeft: wp(4)}}>
            <View style={{width: 1, height:'100%', backgroundColor:'#E0E0E0'}}/>
          </View>
          <View style={{flex: 1}}>
            <View style={{flex:1, flexDirection: 'row', marginBottom: hp(1), alignItems: 'center', justifyContent:'center', }}>
              <FontAwesome5 name={item.categoryIcon} color= 'black' size={hp(3)} style={{paddingRight: wp(4)}}/>
              <Text style={{flex: 1, fontSize: hp(2), color: 'black'}}>{item.categoryName}</Text>
              {//<Text style={{fontSize: 15, color: item.transactionType == 'expense' ? '#f44336' : '#4CAF50'}}>{symbol}$ {item.amount}</Text>
              }
              <Text style={{fontSize: hp(2), color: item.transactionType == 'expense' ? '#f44336' : '#4CAF50'}}>$ {item.amount}</Text>
            </View>
            <View style={{flex:1, flexDirection: 'row', alignItems: 'center', justifyContent:'center', }}>
              <Text style={{flex: 1, fontSize: hp(2), color: '#757575'}}>{item.notes}</Text>
              <View style={{flex: 0.3}}/>
            </View>
          </View>
        </View>
      </Swipeable>
    )
  }

  // renderCarouselItem = ({ item, index }) => {
  //   let isBold = this.state.selectedDate == index ? '500' : 'normal'
  //   return (
  //     <Animatable.View animation={'slideInLeft'} style={{paddingTop: 10, paddingBottom: 10 }}>
  //       <Text style={{fontSize: 15, fontWeight: isBold, letterSpacing: 0.5,textAlign: 'center', color: 'white'}}>{item.a} 2018</Text>
  //     </Animatable.View>
  //   );
  // };

  monthRequest = (item) => {
    this.setState({selectedMonth: item.transactionMonth})
    this.props.actions.monthlyCategoryTransactionRequest({
      month: item.transactionMonth,
      name: item.categoryName,
      icon: item.categoryIcon
    })
    this.toggleMonthModal()
  }

  renderMonthList = ({ item, index }) => {
    return (
      <TouchableOpacity onPress={()=>this.monthRequest(item)} style={{flexDirection:'row', alignItems: 'center'}}>
        <Text style={{fontSize: 15, textAlign:'left', letterSpacing: 2, color:'black', paddingTop: 30, paddingLeft: 20}}>
          {getMonthName(SHORT_DATE_NAME, new Date(item.transactionMonth).getMonth()) + ' ' + new Date(item.transactionMonth).getFullYear()}
        </Text>
      </TouchableOpacity>
    )
  }

  formatSelectedDateDisplay = (selectedDate) => {
    let currentDate = new Date().setHours(0, 0, 0, 0);
    let displayDay = new Date(selectedDate).setHours(0, 0, 0, 0) == currentDate ? "TODAY" : getDayName(FULL_DATE_NAME, (new Date(selectedDate).getDay()));
    let displayMonth = new Date(selectedDate).getMonth();
    let displayDate = new Date(selectedDate).getDate()
    return displayDay + ", " + getMonthName(FULL_DATE_NAME, displayMonth) + " " + displayDate;
  }

  onDayPress = (day) => {
    this.setState({
      selectedDate: day.dateString,
      isCalendarModalVisible: !this.state.isCalendarModalVisible})
  }

  formatData = (data, numColumns) => {
    const numberOfFullRows = Math.floor(data.length / numColumns);
    let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
    while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
      data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true });
      numberOfElementsLastRow++;
    }

    return data;
  };

  navigateTo = () => {
    this.setState({
      isOptionModalVisible: !this.state.isOptionModalVisible
    }, () => {
      this.props.navigation.navigate('NewExpenses')
    })
  }

  // onMomentumScrollEnd = (e) => {
  //   let contentOffset = e.nativeEvent.contentOffset;
  //   let viewSize = e.nativeEvent.layoutMeasurement;
  //
  //   // Divide the horizontal offset by the width of the view to see which page is visible
  //   let pageNum = Math.floor(contentOffset.x / viewSize.width);
  //   this.setState({
  //     selectedDate: pageNum,
  //     categoryTitle: this.state.flatListData[pageNum].iconLabel
  //   })
  // }

  convertToAbbrev = (number, precision) => {
    const abbrev = ['', 'k', 'm', 'b', 't'];
    const unrangifiedOrder = Math.floor(Math.log10(Math.abs(number)) / 3)
    const order = Math.max(0, Math.min(unrangifiedOrder, abbrev.length -1 ))
    const suffix = abbrev[order];
    return (number / Math.pow(10, order * 3)).toFixed(precision) + suffix;
  }

  renderCategory = ({item, index}) => {
    const { monthlyTransaction } = this.state
    if(item.amount == 0){
      return;
    }
    return (
        <TouchableOpacity
          onPress={()=>alert(index)}
          style = {{
            //borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
            borderRadius: 35,
            flex: 1,
            width: Dimensions.get('window').width * 0.25,
            height: Dimensions.get('window').width * 0.25,
            backgroundColor: 'white',
            justifyContent: 'center',
            alignItems: 'center',
            marginRight: index == monthlyTransaction.length-1 ? 0 : 10,
            elevation: 5,
            marginBottom: 10,
          }}>
          <FontAwesome5 style={{paddingBottom: 10, alignSelf:'center'}} name={item.icon} color="#616161" size={25}/>
          <Text style={{alignSelf:'center', color: 'black'}}>$ {this.convertToAbbrev(parseFloat(item.amount),2)}</Text>
      </TouchableOpacity>
    )
  }

  getItemLayout = (data, index) => (
    { length: Dimensions.get('window').width * 0.26, offset: Dimensions.get('window').width * 0.26 * index, index }
  )

  render() {
    const {flatListData, selectedDate, carouselData, categoryTitle, categoryIconName, transactionData,
           currentYear,currentMonth,currentDate,currentDay, markedTodayDate, categoryDetail,
           monthlyCategoryTransaction, monthList, selectedMonth, isMonthModalVisible, monthlyTransaction, initialIndex} = this.state
    const { monthlyCategoryTotalAmount } = this.props.monthlyCategoryTransaction;
    return (
      <LinearGradient
        colors={GRADIENT_COLOR} style={{flex: 1}}>

        <DeleteModal
          toggleModal={this.toggleDeleteModal}
          onBackdropPress={this.onDeleteBackdropPress}
          onBackButtonPress={this.onDeleteBackButtonPress}
          showModal={this.state.isDeleteModalVisible}
          transactionDetail={this.state.transactionDetail}
        />

        <TransactionDetailModal
          toggleModal={this.onTransactionDetailBackdropPress}
          onBackdropPress={this.onTransactionDetailBackdropPress}
          onBackButtonPress={this.onTransactionDetailBackButtonPress}
          showModal={this.state.isOptionModalVisible}
          toggleDeleteModal={this.toggleDeleteModal}
          onModalHide={this.onTransactionDetailModalHide}
          transactionDetail={this.state.transactionDetail}
          {...this.props}
        />

        {// <Modal
        //   animationIn={'zoomIn'}
        //   animationOut={'zoomOut'}
        //   onBackdropPress={this.onMonthBackdropPress}
        //   onBackButtonPress={this.onMonthBackButtonPress}
        //   isVisible={isMonthModalVisible}
        //   style={{flex: 1, alignSelf: 'center'}}>
        //   <View style={{
        //     backgroundColor: 'white',
        //     borderRadius: 10,
        //     borderColor: 'white',
        //     justifyContent: 'center',
        //     flex: 0.7,
        //     width: DEVICE_WIDTH*0.7
        //   }}>
        //   <View style={{flex: 1}}>
        //     <FlatList
        //       ref={(ref) => { this.monthList = ref }}
        //       data={monthList}
        //       renderItem={this.renderMonthList}
        //       keyExtractor={(item, index) => item+index}
        //       showsVerticalScrollIndicator={false}
        //     />
        //   </View>
        //   <View style={{flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center'}}>
        //       <TouchableOpacity onPress={this.toggleMonthModal}>
        //         <Text style={{fontSize: 12, padding: 20, paddingTop: 0}}>CANCEL</Text>
        //       </TouchableOpacity>
        //   </View>
        //   </View>
        // </Modal>
        }
        <View>
          <Carousel
            initialNumToRender={monthList.length}
            ref={(c) => { this.monthCarousel = c; }}
            data={monthList}
            renderItem={this.renderCarouselItem}
            sliderWidth={wp(100)}
            itemWidth={wp(28)}
            layout={'default'}
            inactiveSlideOpacity={0.3}
            enableMomentum={true}
            onSnapToItem={(index) => {
                this.setState({selectedMonth: monthList[index].transactionMonth})
                this.props.actions.monthlyCategoryTransactionRequest({
                  month: monthList[index].transactionMonth,
                  name: monthList[index].categoryName,
                  icon: monthList[index].categoryIcon
                })
                //this.setState({ activeSlide: index })
              }
            }
            //firstItem={renderMonth.length-1}
          />
        </View>

        <View>
          {// {monthlyCategoryTransaction.length > 0  && <TouchableOpacity onPress={this.toggleMonthModal}  style={{paddingLeft: 15, paddingBottom: 10}}>
          //   <Animatable.View animation={'fadeInDown'} style={{flexDirection:'row', alignItems: 'center'}}>
          //     <Text style={{fontSize: 20, textAlign:'left', letterSpacing: 2, color:'white', paddingRight: 10}}>
          //       {getMonthName(SHORT_DATE_NAME, new Date(selectedMonth).getMonth()) + ' ' + new Date(selectedMonth).getFullYear()}
          //     </Text>
          //     <FontAwesome5 name={'angle-down'} color="white" size={15} style={{flex:1}}/>
          //   </Animatable.View>
          // </TouchableOpacity>}
          }

          <Animatable.View animation={'fadeInDown'} style={{paddingLeft: wp(3)}}>
            {/**<FontAwesome5 name={categoryIconName} color="black" size={30} style={{paddingRight:5}}/>**/}
            <View style={{flexDirection: 'row', justifyContent:'flex-start', alignItems:'center'}}>
              <Text style={{fontSize: hp(5), textAlign:'left', letterSpacing: 2, color:'white', fontWeight: 'bold'}}>{categoryDetail.name}</Text>
            </View>
            <Text style={{fontSize: hp(4), textAlign:'left', letterSpacing: 2, color: 'white', paddingBottom: hp(1)}}>$ {parseFloat(monthlyCategoryTotalAmount).toFixed(2)}</Text>
          </Animatable.View>
        </View>


        <View style={{flex:1}}>
          <Animatable.View animation={'slideInUp'} style={{flex: 1, paddingLeft: wp(3), paddingRight: wp(3)}}>
            <DefaultView hasTransaction={monthlyCategoryTransaction.length > 0 ? true : false} size={hp(20)} fontSize={hp(2)} style={{flex:1}}>
              <FlatList
                  data={monthlyCategoryTransaction}
                  style={{flex: 1}}
                  renderItem={this.renderTransactionItem}
                  keyExtractor={(item, index) => item+index}
                  showsVerticalScrollIndicator={false}
              />
            </DefaultView>
          </Animatable.View>
        </View>
      </LinearGradient>
    );
  }
}

mapStateToProps = (state) => {
  const { monthlyCategoryTransaction, transactionDetail, monthlyTransaction } = state;
  return {
    monthlyCategoryTransaction,
    transactionDetail,
    monthlyTransaction
  }
}

mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Object.assign({},monthlyCategoryTransactionAction, transactionDetailAction, monthlyTransactionAction), dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryDetails);
