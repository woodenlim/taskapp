import React, { PureComponent } from "react";
import {
  View,
  Text,
  Dimensions,
  FlatList,
  TouchableOpacity,
  ScrollView
} from "react-native";
import { CalendarList } from 'react-native-calendars';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {getMonthName, getDayName, getMonthInTwoDigit, getDateInTwoDigit } from '@Utils/dateUtils';
import Modal from "react-native-modal";
import {SHORT_DATE_NAME, FULL_DATE_NAME} from '@Constants/constant';

class Calendar extends PureComponent {
  constructor(props) {
    super(props);
    let today = new Date().getFullYear() + "-" + getMonthInTwoDigit((new Date().getMonth()+1)) + "-" + getDateInTwoDigit(new Date().getDate());
    this.state = {
      flatListData:[{iconName: 'utensils', iconLabel: 'Foods and Drinks', amount: '20.00'},
                      {iconName: 'shopping-cart', iconLabel: 'Shopping', amount: '20.00'},
                      {iconName: 'shopping-cart', iconLabel: 'Shopping', amount: '20.00'},
                      {iconName: 'shopping-cart', iconLabel: 'Shopping', amount: '20.00'},
                      {iconName: 'shopping-cart', iconLabel: 'Shopping', amount: '20.00'},
                      {iconName: 'shopping-cart', iconLabel: 'Shopping', amount: '20.00'},
                      {iconName: 'shopping-cart', iconLabel: 'Shopping', amount: '20.00'},
                      {iconName: 'shopping-cart', iconLabel: 'Shopping', amount: '20.00'},
                      {iconName: 'shopping-cart', iconLabel: 'Shopping', amount: '20.00'},
                      {iconName: 'shopping-cart', iconLabel: 'Shopping', amount: '20.00'},
                      {iconName: 'utensils', iconLabel: 'Foods and Drinks', amount: '20.00'},
                      {iconName: 'utensils', iconLabel: 'Foods and Drinks', amount: '20.00'}],
      modalHeaderColor: "",
      modalHeaderText: "",
      scrollOffset: 0,
      currentYear: new Date().getFullYear(),
      currentMonth: getMonthName(FULL_DATE_NAME, new Date().getMonth()),
      currentDate: new Date().getDate(),
      currentDay: getDayName(FULL_DATE_NAME, new Date().getDay()),
      markedDate: {
        [today]: {
          customStyles: {
            container: {
              borderWidth: 1,
              borderRadius: 0,
              borderColor: '#70a1ff'
            }
          }
        }
      },
      isModalVisible: false,
      selectedDate: new Date()
    }
  }

  toggleModal = (type) => {
    const {isModalVisible} = this.state;
    this.setState({
      isModalVisible: !isModalVisible,
      modalHeaderColor: type == 'expenses' ? '#F06292' :  '#4DB6AC',
      modalHeaderText: type == 'expenses' ? 'Expenses' : 'Income'
    });
  }

  closeModal = () => this.setState({isModalVisible: !this.state.isModalVisible});

  selectType = (value) => {
    this.setState({
      expensesBorderBottomColor: value == "expenses" ? '#ff4757':'transparent',
      incomeBorderBottomColor: value == "income" ? '#2ed573':'transparent',
    })
  }

  onDayPress = (day) => {
    const {markedDate,selectedDate}=this.state;
    let today = new Date().getFullYear() + "-" + getMonthInTwoDigit((new Date().getMonth()+1)) + "-" + getDateInTwoDigit(new Date().getDate());
    let date = day.dateString;
    this.setState({
      markedDate: {
        [date]: {
          customStyles: {
            container: {
              borderWidth: 1,
              borderRadius: 0,
              borderColor: '#70a1ff'
            }
          }
        }
      },
      selectedDate: date,
    })
  }

  renderItem = ({ item, index }) => {
    return (
      <View style={{flex:1, flexDirection: 'row', marginTop: 3, marginBottom: 3, padding: 10, backgroundColor: index%2 == 0 ? 'white':'#ECEFF1'}}>
        <View style={{alignItems: 'center', justifyContent: 'center', width: 40, height: 40}}>
          <FontAwesome5 name={item.iconName} color="grey" size={20}/>
        </View>
        <Text style={{flex: 1, fontSize: 15, alignSelf: 'center', textAlignVertical: 'center'}}>{item.iconLabel}</Text>
        <Text style={{textAlignVertical: 'center', alignSelf: 'center', fontSize: 15}}>MYR {item.amount}</Text>
      </View>
    )
  }

  formatSelectedDateDisplay = (selectedDate) => {
    let currentDate = new Date().setHours(0, 0, 0, 0);
    let displayDay = new Date(selectedDate).setHours(0, 0, 0, 0) == currentDate ? "Today" : getDayName(FULL_DATE_NAME, (new Date(selectedDate).getDay()));
    let displayMonth = new Date(selectedDate).getMonth();
    let displayDate = new Date(selectedDate).getDate()
    return getMonthName(FULL_DATE_NAME, displayMonth) + " " + displayDate;
  }

  handleOnScroll = event => {
    this.setState({
      scrollOffset: event.nativeEvent.contentOffset.y
    });
  };

  handleScrollTo = p => {
    if (this.scrollViewRef) {
      this.scrollViewRef.scrollTo(p);
    }
  };

  render() {
    const {flatListData, currentDay, currentMonth, currentDate, currentYear,markedDate,isModalVisible, flatListHeight,selectedDate,
            modalHeaderColor,modalHeaderText, scrollOffset} = this.state;
    return (
      <View style={{flex: 1, backgroundColor:'white'}}>

        <Modal
          onBackdropPress={this.closeModal}
          onBackButtonPress={this.closeModal}
          isVisible={isModalVisible}
          onSwipe={this.closeModal}
          swipeDirection="down"
          scrollTo={this.handleScrollTo}
          scrollOffset={scrollOffset}
          scrollOffsetMax={scrollOffset} // content height - ScrollView height
          style={{margin: 0, justifyContent: 'flex-end'}}
          swipeThreshold={DEVICE_HEIGHT * 0.2}
        >
          <View style={{flex: 1, backgroundColor: 'white', overflow: 'hidden', borderTopLeftRadius: 20, borderTopRightRadius: 20}}>
            <View style={{flexDirection: 'row', backgroundColor:modalHeaderColor, padding: 20, borderTopLeftRadius: 20, borderTopRightRadius: 20}}>
              <Text style={{flex:1, fontSize: 25, color:'white'}}>{modalHeaderText}</Text>
            </View>
            <ScrollView
              ref={ref => (this.scrollViewRef = ref)}
              onScroll={this.handleOnScroll}
              scrollEventThrottle={200}>
                <FlatList
                  data={flatListData}
                  style={{flex: 1}}
                  renderItem={this.renderItem}
                  keyExtractor={(item, index) => item+index}
                />
            </ScrollView>
          </View>
        </Modal>

        <View style={{flex: 6.5}}>
          <CalendarList
            onVisibleMonthsChange={(months) => {console.log('now these months are visible', months[0])}}
            markingType={'custom'}
            horizontal={true}
            pagingEnabled={true}
            // hideArrows={false}
            // hideExtraDays={false}
            onDayPress={this.onDayPress}
            markedDates={this.state.markedDate}
          />
        </View>
        <View style={{flex: 3}}>
          <Text style={{fontSize: 25, paddingLeft: 10, color: 'black'}}>{this.formatSelectedDateDisplay(selectedDate)}</Text>
          <View style={{flex: 1, flexDirection: 'row', padding: 10}}>
              <TouchableOpacity onPress={() => this.toggleModal('expenses')} style={{flex: 1, alignItems: 'center', borderRadius: 20, justifyContent: 'center',backgroundColor: '#F06292', marginRight: 5}}>
                <Text style={{fontSize: 20, paddingLeft: 10, color: 'white'}}>Expenses</Text>
                <Text style={{fontSize: 20, color: 'white'}}>MYR 200.00</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.toggleModal('income')} style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#4DB6AC', borderRadius: 20, marginLeft: 5}}>
                <Text style={{fontSize: 20, paddingLeft: 10, color: 'white'}}>Income</Text>
                <Text style={{fontSize: 20, color: 'white'}}>MYR 200.00</Text>
              </TouchableOpacity>
          </View>
        </View>

      </View>
    );
  }
}

export default Calendar;
