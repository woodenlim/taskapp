import React, { PureComponent } from "react";
import {
  View,
  Text,
  SectionList,
  Switch,
  TouchableOpacity,
  Platform,
  DatePickerIOS
} from "react-native";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from 'react-native-linear-gradient';
import LottieView from 'lottie-react-native';
import {GRADIENT_COLOR} from '@Constants/constant';
import {DEVICE_WIDTH,DEVICE_HEIGHT} from '@Constants/constant';
import * as Animatable from 'react-native-animatable';
import Modal from "react-native-modal";
import { WheelPicker, DatePicker, TimePicker } from 'react-native-wheel-picker-android';
import * as remindTimeListAction from "@ExpensesAction/remindTimeList";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Swipeable from 'react-native-swipeable';
import Toast from 'react-native-root-toast';
import firebase from 'react-native-firebase';
import uuidv1 from 'uuid/v1';
import * as userSettingAction from "@ExpensesAction/userSetting";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '@Utils/scalingUtils';

class Remind extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      hour: ['01','02','03','04','05','06','07','08','09','10','11', '12'],
      minute: ['00','01','02','03','04','05','06','07','08','09',
               '10','11','12','13','14','15','16','17','18','19',
               '20','21','22','23','24','25','26','27','28','29',
               '30','31','32','33','34','35','36','37','38','39',
               '40','41','42','43','44','45','46','47','48','49',
               '50','51','52','53','54','55','56','57','58','59'],
      time: ['AM', 'PM'],
      showTimePickerModal: false,
      selectedTime: new Date(new Date().setHours(6,0,0,0)),
      selectedHour: 6, //default time
      selectedMinute: 0, //default time
      remindTimeList: this.props.remindTimeList.response,
      action: 'add', //default action
      updateIndex: 0,
      currentlyOpenSwipeable: null,
    }
  }

  componentDidMount() {
    const {remindTimeList} = this.state
    if(!(remindTimeList.length > 0)){
      this.animation.play();
    }
    //this.props.actions.remindTimeListRequest()
    //alert(JSON.stringify(this.props.remindTimeList))
  }

  componentDidUpdate(prevProps){
    if(prevProps.addRemindTime !== this.props.addRemindTime){
      if(this.props.addRemindTime.status == 'Success'){
        // this.setState({
        //   remindTimeList: this.props.addRemindTime.response.sort((a,b) => (a.time > b.time) ? 1 : ((b.time > a.time) ? -1 : 0))
        // })
        this.props.actions.remindTimeListSuccess({
          refresh: false,
          response: this.props.addRemindTime.response
        })
      }
    }

    if(prevProps.remindTimeList !== this.props.remindTimeList){
      //alert('Hi')
      this.setState({
        remindTimeList: this.props.remindTimeList.response.sort((a,b) => (a.time > b.time) ? 1 : ((b.time > a.time) ? -1 : 0))
      }, () => {
        this.scheduleNotification(this.state.remindTimeList)
      })
    }
  }

  renderAndroidPicker = () => {
    const { hour, minute, time, selectedHour, selectedMinute } = this.state;
    const date = new Date()
    date.setHours(selectedHour,selectedMinute,0,0) //set to 6AM
    return (
      <View style={{flex:8, flexDirection: 'row'}}>
        <TimePicker
          minutes={minute}
          onTimeSelected={(date)=>this.onTimeSelected(date)}
          initDate={date.toString()}/>
      </View>
    )
  }

  onTimeSelected = (date) => {
    this.setState({
      selectedTime: new Date(date)
    })
  }

  renderIosPicker = () => {
    return (
      <View style={{flex:8, flexDirection: 'row', marginTop: hp(1), marginBottom: hp(1)}}>
        <DatePickerIOS
          style={{flex:1}}
          date={new Date()}
          //onDateChange={this.setDate}
          mode={'time'}
        />
      </View>
    )
  }

  toggleTimePicker = () => {
    const date = new Date()
    date.setHours(6,0,0,0) //set to 6AM
    this.setState({
      showTimePickerModal: !this.state.showTimePickerModal,
      selectedTime: new Date(date),
      action: 'add',
    })
  }

  toggleUpdateTimePicker = (item, index) => {
    const { remindTimeList, showTimePickerModal } = this.state;
    let targetRemindTime = remindTimeList[index];
    let timeSplit = item.time.split(':')
    this.setState({
      selectedHour: parseInt(timeSplit[0]),
      selectedMinute: parseInt(timeSplit[1]),
      showTimePickerModal: !showTimePickerModal,
      action: 'update',
      updateIndex: index
    })
  }

  updateRemindTime = () => {
    this.isNotificationEnable(true);
    const { updateIndex, remindTimeList, selectedTime, showTimePickerModal, currentlyOpenSwipeable } = this.state;
    let updatedItem = remindTimeList[updateIndex]
    let timeExist = false;
    const hour = new Date(selectedTime).getHours() < 10 ? '0' + new Date(selectedTime).getHours() : new Date(selectedTime).getHours();
    const min = new Date(selectedTime).getMinutes() < 10 ? '0' + new Date(selectedTime).getMinutes() : new Date(selectedTime).getMinutes();;
    const result = hour + ':' + min;
    remindTimeList.map((data)=>{
      if(result == data.time){
        this.showToast('Time already add')
        timeExist = true;
        return;
      }
    })
    if(!timeExist){
      updatedItem.time = result;
      this.setState({
        showTimePickerModal: !showTimePickerModal,
        selectedHour: 6, //default time
        selectedMinute: 0, //default time
      }, () => {
        this.state.currentlyOpenSwipeable.recenter();
        this.showToast('Time Updated')
      })
      this.props.actions.remindTimeListSuccess({
        refresh: true,
        response: remindTimeList.sort((a,b) => (a.time > b.time) ? 1 : ((b.time > a.time) ? -1 : 0))
      })
    }
    this.toggleTimePicker()
    currentlyOpenSwipeable.recenter()
  }

  getTimeResult = (selectedTime) => {
    const hour = new Date(selectedTime).getHours() < 10 ? '0' + new Date(selectedTime).getHours() : new Date(selectedTime).getHours();
    const min = new Date(selectedTime).getMinutes() < 10 ? '0' + new Date(selectedTime).getMinutes() : new Date(selectedTime).getMinutes();;
    const result = hour + ':' + min;
    this.props.actions.remindTimeListSuccess({
      refresh: false,
      response: {
        time:result,
        notificationId: uuidv1()
      }
    })
  }

  cancelNotification = async (notificationId) => {
    await firebase.notifications().cancelNotification(notificationId)
  }

  updateNotification = (notificationId) => {
    let updatedNotification =
      new firebase.notifications.Notification()
      .setNotificationId(notificationId)
      .setTitle('B Y B')
      //.setSubtitle('BYB')
      .setBody('Do not forget to add your transaction.')
      .android.setChannelId('bybChannelId') // e.g. the id you chose above
      .android.setSmallIcon('notification_icon') // create this icon in Android Studio
      .android.setLargeIcon('@mipmap/ic_launcher') // create this icon in Android Studio
      .android.setColor('#b06ab3') // you can set a color here
      .android.setPriority(firebase.notifications.Android.Priority.High);

      const date = new Date();
      let timeSplit = data.time.split(':')
      date.setHours(timeSplit[0],timeSplit[1],0,0)

      firebase.notifications().scheduleNotification(scheduleTimeArray, {
        fireDate: date.getTime(),
        repeatInterval: 'day'
      })
  }

  scheduleNotification = (remindTimeList) => {
    remindTimeList.map((data, index) => {
      let scheduleTime =
        new firebase.notifications.Notification()
        .setNotificationId(data.notificationId)
        .setTitle('B Y B')
        //.setSubtitle('BYB')
        .setBody('Do not forget to add your transaction.')
        //.setBody(data.time)
        .android.setChannelId('bybChannelId') // e.g. the id you chose above
        .android.setSmallIcon('notification_icon') // create this icon in Android Studio
        .android.setLargeIcon('@mipmap/ic_launcher') // create this icon in Android Studio
        .android.setColor('#b06ab3') // you can set a color here
        .android.setPriority(firebase.notifications.Android.Priority.High);

        const date = new Date();
        let timeSplit = data.time.split(':')
        date.setHours(timeSplit[0],timeSplit[1],0,0)

        firebase.notifications().scheduleNotification(scheduleTime, {
          fireDate: date.getTime(),
          repeatInterval: 'day'
        })
    })
  }

  isNotificationEnable = (enabled) => {
    const { userSetting } = this.props;
    this.props.actions.userSettingSuccess({
      isNotificationEnable: enabled,
      // isPasscodeEnable: userSetting.passcodeSetting.isEnable,
      // userPasscode: userSetting.passcodeSetting.value,
      // isFingerprintEnable: userSetting.isFingerprintEnable,
    })
  }

  addRemindTime = () => {
    this.isNotificationEnable(true);
    const { selectedTime, remindTimeList, action, currentlyOpenSwipeable } = this.state;
    let timeExist = false;
    remindTimeList.map((data)=>{
      const hour = new Date(selectedTime).getHours() < 10 ? '0' + new Date(selectedTime).getHours() : new Date(selectedTime).getHours();
      const min = new Date(selectedTime).getMinutes() < 10 ? '0' + new Date(selectedTime).getMinutes() : new Date(selectedTime).getMinutes();;
      const result = hour + ':' + min;
      if(result == data.time){
        this.showToast('Time already add')
        timeExist = true;
        return;
      }
    })
    if(!timeExist){
      this.showToast('Time added')
      this.getTimeResult(selectedTime);
    }
    this.toggleTimePicker()
    currentlyOpenSwipeable && currentlyOpenSwipeable.recenter()
  }

  showToast = (text) => {
    this.toast && this.toast.destroy();
    this.toast = Toast.show(text, {
      duration: Toast.durations.SHORT,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true,
      onHidden: () => {
        this.toast.destroy();
        this.toast = null;
      }
    });
  };

  deleteRemindTime = (deletedItem, deletedIndex) => {
    const { remindTimeList, currentlyOpenSwipeable } = this.state;
    if(remindTimeList.length == 0){
      this.isNotificationEnable(false);
    }
    this.setState({
      remindTimeList: remindTimeList.filter( (item, index) => index !== deletedIndex)
    }, () => {
      //alert(JSON.stringify(this.state.remindTimeList))
      //currentlyOpenSwipeable.recenter();
      this.showToast('Time deleted')
      if(this.state.remindTimeList.length == 0){
        this.isNotificationEnable(false);
      }
      this.cancelNotification(deletedItem.notificationId)
      this.props.actions.remindTimeListSuccess({
        refresh: true,
        response: this.state.remindTimeList
      })
    })
  }

  handleScroll = () => {
    const { currentlyOpenSwipeable } = this.state;

    if (currentlyOpenSwipeable) {
      currentlyOpenSwipeable.recenter();
    }
  }

  onRightButtonsOpenRelease = (event, gestureState, swipeable) => {
    const { currentlyOpenSwipeable } = this.state;
    if (currentlyOpenSwipeable && currentlyOpenSwipeable !== swipeable) {
      currentlyOpenSwipeable.recenter();
    }
    this.setState({currentlyOpenSwipeable: swipeable});
  }

  onRightButtonsCloseRelease = () => {this.setState({currentlyOpenSwipeable: null})}


  renderItem = ({ item, index }) => {
    //alert(JSON.stringify(item))
    return (
      <Swipeable
        onRef={ref => this.swipeable = ref}
        onRightButtonsOpenRelease={this.onRightButtonsOpenRelease}
        onRightButtonsCloseRelease ={this.onRightButtonsCloseRelease}
        rightButtons={[
          <View
            style={{
              flex: 1,
              backgroundColor:'transparent',
              flexDirection: 'row',
              alignItems:'center',
              marginBottom: hp(1),
            }}>
            <TouchableOpacity
              onPress={()=>this.toggleUpdateTimePicker(item, index)}
              style={{
                alignItems: 'center',
                justifyContent:'center',
                marginLeft: wp(3),
                width: wp(15),
                height: wp(15),
                borderRadius: wp(15)/2,
                backgroundColor:'white'}}>
              <FontAwesome5 name={'edit'} size={20} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={()=>this.deleteRemindTime(item, index)}
              style={{
                alignItems: 'center',
                justifyContent:'center',
                marginLeft: wp(3),
                width: wp(15),
                height: wp(15),
                borderRadius: wp(15)/2,
                backgroundColor:'#ED5E68'}}>
              <FontAwesome5 name={'trash-alt'} size={20} color={'white'}/>
            </TouchableOpacity>
          </View>
        ]}
        rightButtonWidth={Math.floor(wp(40))}>
        <TouchableOpacity onPress={()=>this.toggleUpdateTimePicker(item, index)}
          style={{
            flex:1,
            flexDirection: 'row',
            paddingTop: hp(2),
            paddingBottom: hp(2),
            paddingLeft: wp(4),
            paddingRight: wp(4),
            backgroundColor: '#F0F8FF',
            marginBottom: hp(1),
            borderRadius: wp(3),
            alignItems:'center',
            justifyContent:'flex-start'}}>
          <Text style={{fontSize: hp(5), color: 'black', letterSpacing: 1}}>{item.time}</Text>
        </TouchableOpacity>
      </Swipeable>
    )
  }

  sectionHeader = ({section: {title}}) => {
    return (
      <View style={{paddingBottom: hp(2)}}>
        <Text style={{fontSize: hp(3), letterSpacing:3, fontWeight: 'bold', color:'white'}}>{title}</Text>
      </View>
    )
  }

  render() {
    const { remindTimeList, action } = this.state
    return (
      <LinearGradient
        colors={GRADIENT_COLOR} style={{flex: 1}}>

        <Modal
          animationIn={'slideInUp'}
          onBackdropPress={this.toggleTimePicker}
          onBackButtonPress={this.toggleTimePicker}
          isVisible={this.state.showTimePickerModal}>
          <View style={{
            overflow: 'hidden',
            flex:0.5,
            backgroundColor: 'white',
            borderRadius: wp(3),
            borderColor: 'white'}}>
            <LinearGradient
              colors={GRADIENT_COLOR}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{
                paddingTop: hp(2),
                paddingBottom: hp(2),
                paddingLeft: wp(4),
                paddingRight: wp(4),
                borderBottomWidth: 0.5}}>
              <Text style={{fontSize: hp(3), color: 'white'}}>Select Time</Text>
            </LinearGradient>
            {
              Platform.OS == 'ios' ? this.renderIosPicker() : this.renderAndroidPicker()
            }
            <View style={{flexDirection: 'row', alignItems:'flex-end', justifyContent:'flex-end'}}>
                <TouchableOpacity
                  onPress={this.toggleTimePicker}
                  style={{
                    flex:1,
                    backgroundColor:'#E0E0E0',
                    borderRadius: hp(1),
                    justifyContent:'center',
                    alignItems:'center',
                    marginTop: hp(3),
                    marginBottom: hp(2),
                    marginLeft: wp(4),
                    marginRight: wp(2)}}>
                  <Text style={{fontSize: hp(2), letterSpacing: 1, color:'black', paddingTop: hp(1), paddingBottom:hp(1)}}>CANCEL</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={action == 'add' ? this.addRemindTime : this.updateRemindTime}
                  style={{
                    flex:1,
                    backgroundColor:'#48D563',
                    borderRadius: hp(1),
                    justifyContent:'center',
                    alignItems:'center',
                    marginTop: hp(3),
                    marginBottom: hp(2),
                    marginLeft: wp(2),
                    marginRight: wp(4)}}>
                  <Text style={{fontSize: hp(2), letterSpacing: 1, color:'white', paddingTop: hp(1), paddingBottom:hp(1)}}>OK</Text>
                </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <View style={{flex:1, paddingLeft: wp(3), paddingRight: wp(3)}}>
          {
            remindTimeList.length > 0 &&
            <SectionList
              renderItem={this.renderItem}
              renderSectionHeader={this.sectionHeader}
              sections={[
                {
                  title: 'Remind Me',
                  data: remindTimeList
                },
              ]}
              keyExtractor={(item, index) => item + index}
              showsVerticalScrollIndicator={false}
            />
          }
          {
            remindTimeList.length > 0 &&
            <TouchableOpacity onPress={this.toggleTimePicker}
              style={{margin: wp(3), position:'absolute', bottom: 0, right: 0, justifyContent:'center', alignSelf:'center', backgroundColor:'white', borderRadius: wp(50), height: wp(15), width: wp(15)}}>
              <Animatable.View animation={'zoomIn'} >
                <FontAwesome5 name={'plus'} color= 'black' size={hp(4)} style={{alignSelf:'center'}} />
              </Animatable.View>
            </TouchableOpacity>
          }
          {
            remindTimeList.length == 0 &&
            <Animatable.View animation={'zoomIn'} style={{alignItems: 'center'}}>
              <TouchableOpacity onPress={this.toggleTimePicker}>
                <LottieView
                  speed={1}
                  style={{width: wp(40), height: hp(50)}}
                  ref={animation => {
                    this.animation = animation;
                  }}
                  source={require('@Lottie/no_notifications.json')}/>
              </TouchableOpacity>
              <Text onPress={this.toggleTimePicker} style={{fontSize: hp(3), color: 'white', letterSpacing: 1}}>Wake me up?</Text>
            </Animatable.View>
          }
        </View>

      </LinearGradient>
    );
  }
}

mapStateToProps = (state) => {
  const { addRemindTime, remindTimeList, userSetting } = state;
  return {
    addRemindTime,
    remindTimeList,
    userSetting
  }
}

mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Object.assign({}, userSettingAction, remindTimeListAction), dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Remind);
