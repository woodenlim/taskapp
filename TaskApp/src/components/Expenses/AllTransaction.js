import React, { PureComponent } from "react";
import {
  View,
  Text,
  FlatList,
  Switch,
  TouchableOpacity,
  Alert,
  BackHandler,
  Animated,
  Easing,
  StyleSheet,
  Platform
} from "react-native";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import LinearGradient from 'react-native-linear-gradient';
import {GRADIENT_COLOR, EXPENSE_GRADIENT_COLOR, INCOME_GRADIENT_COLOR, GRADIENT_START, GRADIENT_END, DEVICE_WIDTH,DEVICE_HEIGHT,SHORT_DATE_NAME,FULL_DATE_NAME, EXPENSES_CATEGORY, INCOME_CATEGORY} from '@Constants/constant';
import {getMonthName, getDayName, getMonthInTwoDigit, getDateInTwoDigit, getDateOrdinal } from '@Utils/dateUtils';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '@Utils/scalingUtils';
import * as Animatable from 'react-native-animatable';
import { CalendarList, Calendar } from 'react-native-calendars';
import { HeaderBackButton } from 'react-navigation';
import DeleteModal from '@ExpensesCommonComponents/DeleteModal';
import TransactionDetailModal from '@ExpensesCommonComponents/TransactionDetailModal';
import Swipeable from 'react-native-swipeable';
import SlidingUpPanel from 'rn-sliding-up-panel';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import LottieView from 'lottie-react-native';
import * as transactionDetailAction from "@ExpensesAction/transactionDetail";
import * as dailyTransactionAction from "@ExpensesAction/dailyTransaction";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

const AnimatedView = Animatable.createAnimatableComponent(View);

class AllTransaction extends PureComponent {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props);
    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
    this.state = {
      selectedDate: this.props.navigation.getParam('selectedDate', ''),
      transactionType: this.props.navigation.getParam('transactionType', null),
      backToHome: this.props.navigation.getParam('backToHome', false),
      isExpenses: true,
      dailyTransaction: [],
      currentlyOpenSwipeable: null,
      isOptionModalVisible: false,
      isDeleteModalVisible: false,
      isDeleteTrigger: false,
      transactionDetail: {},
      isHeightAnimated: false,
      isSwiping: true
    }
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: (<HeaderBackButton onPress={()=>navigation.popToTop()} tintColor={'white'}/>),
    };
  };

  componentDidMount(){
    this.animatedHeightValue = new Animated.Value(hp(35));
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
    const { selectedDate, transactionType } = this.state;
    this.props.actions.dailyTransactionRequest({
      date:selectedDate,
      type:transactionType
    });
  }

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
  }

  onBackButtonPressAndroid = () => {
    if(this.state.backToHome && !this.state.isHeightAnimated){
      this.props.navigation.navigate('Dashboard')
      return true
    }else if(this.state.isHeightAnimated){
      this.animatedHeight()
      return true
    }
    return false
  };


  componentDidUpdate(prevProps) {
    if(prevProps.dailyTransaction !== this.props.dailyTransaction){
      const {response} = this.props.dailyTransaction;
      if(response.length == 0){
        // this.setState({
        //   dailyTransaction: response
        // })
        // this.animation.play();
      }

    }
  }

  onDayPress = (day) => {
    const { isExpenses } = this.state;
    this.setState({
      selectedDate: day.dateString
    }, () => {
      this.props.actions.dailyTransactionRequest({
        date:this.state.selectedDate,
        type: isExpenses ? 'expense' : 'income'
      });
    })
  }

  selectedTab = (type) => {
    this.setState({
      isExpenses: type == 'expense' ? true : false
    }, () => {
      this.props.actions.dailyTransactionRequest({
        date: this.state.selectedDate,
        type: this.state.isExpenses ? 'expense' : 'income'
      });
    })
  }

  toggleTransactionDetailModal = (item) => {
    this.setState({ isOptionModalVisible: !this.state.isOptionModalVisible });
    this.props.actions.transactionDetailRequest({
      id: item.id
    })
  }
  onTransactionDetailBackdropPress = () => this.setState({ isOptionModalVisible: !this.state.isOptionModalVisible });
  onTransactionDetailBackButtonPress = () => this.setState({ isOptionModalVisible: !this.state.isOptionModalVisible });
  onTransactionDetailModalHide = () => {
    if(this.state.isDeleteTrigger){
      this.setState({
        isDeleteModalVisible: !this.state.isDeleteModalVisible
      })
    }
  }

  toggleDeleteModal = () => {
    this.setState({
      isOptionModalVisible: !this.state.isOptionModalVisible, //to trigger option modal hide and show delete modal
      isDeleteTrigger: !this.state.isDeleteTrigger
    });
  }
  onDeleteBackdropPress = () => {
    this.setState({
      isDeleteModalVisible: !this.state.isDeleteModalVisible,
      isDeleteTrigger: false
    });
  }
  onDeleteBackButtonPress = () => this.setState({
    isDeleteModalVisible: !this.state.isDeleteModalVisible,
    isDeleteTrigger: false
  });

  //for swipe to delete
  confirmDelete = (item) => {
    this.setState({
      isDeleteModalVisible: !this.state.isDeleteModalVisible,
      transactionDetail: item,
      currentlyOpenSwipeable: null
    });
  }
  //for swipe to edit
  editTransaction = (item) => {
    this.state.currentlyOpenSwipeable.recenter();
    this.props.navigation.navigate('NewExpenses', {transactionDetail: item})
  }

  handleScroll = () => {
    const { currentlyOpenSwipeable } = this.state;

    if (currentlyOpenSwipeable) {
      currentlyOpenSwipeable.recenter();
    }
  }

  onRightButtonsOpenRelease = (event, gestureState, swipeable) => {
    const { currentlyOpenSwipeable } = this.state;
    if (currentlyOpenSwipeable && currentlyOpenSwipeable !== swipeable) {
      currentlyOpenSwipeable.recenter();
    }
    this.setState({currentlyOpenSwipeable: swipeable});
  }

  onRightButtonsCloseRelease = () => {this.setState({currentlyOpenSwipeable: null})}

  renderTransactionItem = ({ item, index }) => {
    const { dailyTransaction } = this.props;
    const symbol = item.transactionType == 'expense' ? '-' : '+';
    return (
      <Swipeable
        onRef={ref => this.swipeable = ref}
        onRightButtonsOpenRelease={this.onRightButtonsOpenRelease}
        onRightButtonsCloseRelease ={this.onRightButtonsCloseRelease}
        onSwipeStart={() => this.setState({isSwiping: false})}
        onSwipeRelease={() => this.setState({isSwiping: true})}
        rightButtons={[
          <View
            style={{
              flex: 1,
              backgroundColor:'#E0E0E0',
              flexDirection: 'row',
              alignItems:'center',
              marginBottom: hp(1),
            }}>
            <TouchableOpacity
              onPress={()=>this.editTransaction(item)}
              style={{
                alignItems: 'center',
                justifyContent:'center',
                marginLeft: wp(3),
                width: wp(15),
                height: wp(15),
                borderRadius: wp(7.5),
                backgroundColor:'white'}}>
              <FontAwesome5 name={'edit'} size={hp(3)} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={()=>this.confirmDelete(item)}
              style={{
                alignItems: 'center',
                justifyContent:'center',
                marginLeft: wp(3),
                width: wp(15),
                height: wp(15),
                borderRadius: wp(7.5),
                backgroundColor:'#ED5E68'}}>
              <FontAwesome5 name={'trash-alt'} size={hp(3)} color={'white'}/>
            </TouchableOpacity>
          </View>
        ]}
        rightButtonWidth={Math.floor(wp(40))}>
        <TouchableOpacity
          style={{
            elevation: 5,
            flex:1,
            flexDirection: 'row',
            backgroundColor:'white',
            marginTop: hp(1),
            marginBottom: index == dailyTransaction.response.length - 1 ? hp(1) : 0,
            margin: wp(2),
            paddingTop: hp(2),
            paddingBottom: hp(2),
            paddingLeft: wp(4),
            paddingRight: wp(4),
            borderRadius: wp(3)}}>
          <View style={{paddingBottom:hp(2)}}>
            <Text style={{fontSize: hp(2.5), alignSelf: 'center', color: 'black'}}>{new Date(item.transactionDate).getDate()}</Text>
            <Text style={{fontSize: hp(2.5), alignSelf: 'center', color: '#757575'}}>{getDayName(SHORT_DATE_NAME, new Date(item.transactionDate).getDay())}</Text>
          </View>
          <View style={{paddingRight: wp(4), paddingLeft: wp(4)}}>
            <View style={{width: 1, height:'100%', backgroundColor:'#E0E0E0'}}/>
          </View>
          <View style={{flex: 1}}>
            <View style={{flex:1, flexDirection: 'row', marginBottom: hp(1), alignItems: 'center', justifyContent:'center', }}>
              <FontAwesome5 name={item.categoryIcon} color= 'black' size={hp(3)} style={{paddingRight: wp(4)}}/>
              <Text style={{flex: 1, fontSize: hp(2), color: 'black'}}>{item.categoryName}</Text>
              {//<Text style={{fontSize: 15, color: item.transactionType == 'expense' ? '#f44336' : '#4CAF50'}}>{symbol}$ {item.amount}</Text>
              }
              <Text style={{fontSize: hp(2), color: item.transactionType == 'expense' ? '#f44336' : '#4CAF50'}}>$ {item.amount}</Text>
            </View>
            <View style={{flex:1, flexDirection: 'row', alignItems: 'center', justifyContent:'center', }}>
              <Text style={{flex: 1, fontSize: hp(2), color: '#757575'}}>{item.notes}</Text>
              <View style={{flex: 0.3}}/>
            </View>
          </View>
        </TouchableOpacity>
      </Swipeable>
    )
  }

  onDragEnd = (height) => {
    if(height > DEVICE_HEIGHT * 0.75){
      this.slidingUpPanel.transitionTo(DEVICE_HEIGHT, {
        duration: 0
      })
    }else{
      this.slidingUpPanel.transitionTo(DEVICE_HEIGHT*0.25, {
        duration: 0
      })
    }
    return true;
  }

  animatedHeight = () => {
    const { isHeightAnimated, currentlyOpenSwipeable } = this.state;
    Animated.timing(this.animatedHeightValue, {
      toValue: isHeightAnimated ? hp(35) : hp(91),
      duration: 500,
      easing: Easing.easeOutCubic
    }).start()

    this.setState({
      isHeightAnimated: !isHeightAnimated
    })
    currentlyOpenSwipeable && currentlyOpenSwipeable.recenter();
  }

  render() {
    const { selectedDate, isExpenses, isHeightAnimated, isSwiping } = this.state;
    const { dailyTransaction } = this.props;
    return (
      <LinearGradient
        colors={GRADIENT_COLOR} style={{flex: 1}}>

        <DeleteModal
          toggleModal={this.toggleDeleteModal}
          onBackdropPress={this.onDeleteBackdropPress}
          onBackButtonPress={this.onDeleteBackButtonPress}
          showModal={this.state.isDeleteModalVisible}
          transactionDetail={this.state.transactionDetail}
        />

        <TransactionDetailModal
          toggleModal={this.onTransactionDetailBackdropPress}
          onBackdropPress={this.onTransactionDetailBackdropPress}
          onBackButtonPress={this.onTransactionDetailBackButtonPress}
          showModal={this.state.isOptionModalVisible}
          toggleDeleteModal={this.toggleDeleteModal}
          onModalHide={this.onTransactionDetailModalHide}
          transactionDetail={this.state.transactionDetail}
          {...this.props}
        />

        <View style={{flex: 1}}>
          <Calendar
            current={selectedDate}
            //style={{backgroundColor:'blue', height: hp(50)}}
            futureScrollRange={0}
            maxDate={new Date()}
            horizontal={true}
            pagingEnabled={true}
            calendarWidth={wp(30)}
            scrollEnabled={true}
            hideArrows={false}
            hideExtraDays={false}
            onDayPress={this.onDayPress}
            markedDates={{[selectedDate]: {selected: true, selectedColor: 'white'}}}
            theme={{
              'stylesheet.calendar.header': {
                header: {
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingLeft: wp(3),
                  paddingRight: wp(3),
                  alignItems: 'center'
                },
                monthText: {
                  fontSize: hp(2),
                  fontFamily: 'System',
                  fontWeight: '300',
                  color: 'white',
                  margin: wp(3),
                  marginTop: 0,
                  marginBottom: hp(1)
                },
                // arrow: {
                //   padding: wp(3),
                //   paddingTop: hp(1),
                //   paddingBottom: hp(1)
                // },
                week: {
                  marginTop: 0,
                  flexDirection: 'row',
                  justifyContent: 'space-around'
                },
                dayHeader: {
                  marginTop: 0,
                  marginBottom: hp(1),
                  flex: 1,
                  textAlign: 'center',
                  fontSize: hp(2),
                  fontFamily: 'System',
                  color: 'white'
                },
              },
              'stylesheet.day.basic': {
                base: {
                  height: hp(4),
                  width: hp(4),
                  alignItems: 'center',
                  justifyContent: 'center'
                },
                text: {
                  fontSize: hp(2.5),
                  fontFamily: 'System',
                  fontWeight: '300',
                  color: 'white',
                  backgroundColor: 'rgba(255, 255, 255, 0)'
                },
                // alignedText: {
                //   marginTop: Platform.OS === 'android' ? 4 : 6
                // },
                selected: {
                  //backgroundColor: appStyle.selectedDayBackgroundColor,
                  borderRadius: hp(2)
                },
              },
              'stylesheet.calendar.main': {
                week: {
                  marginTop: hp(1),
                  marginBottom: hp(1),
                  flexDirection: 'row',
                  justifyContent: 'space-around'
                },
              },
              calendarBackground: 'transparent',
              textSectionTitleColor: 'white',
              selectedDayBackgroundColor: 'black',
              selectedDayTextColor: 'black',
              todayTextColor: 'white',
              dayTextColor: 'white',
              textDisabledColor: '#BDBDBD',
              //dotColor: '#00adf5',
              selectedDotColor: '#ffffff',
              arrowColor: 'white',
              monthTextColor: 'white',
              //textDayFontFamily: 'monospace',
              //textMonthFontFamily: 'monospace',
              //textDayHeaderFontFamily: 'monospace',
              //textMonthFontWeight: 'bold',
              //textDayFontSize: 16,
              //textMonthFontSize: 16,
              //textDayHeaderFontSize: 16
            }}/>
        </View>

        <AnimatedView
          style={{height: this.animatedHeightValue, overflow: 'hidden', backgroundColor:'white', borderTopRightRadius: wp(3), borderTopLeftRadius: wp(3)}}>
          <TouchableOpacity onPress={this.animatedHeight} style={{paddingTop: hp(1), justifyContent:'center', alignItems:'center'}}>
            <FontAwesome5 onPress={this.animatedHeight} name={ !isHeightAnimated ? 'chevron-up' : 'chevron-down'} color="#BDBDBD" size={wp(4)}/>
          </TouchableOpacity>
          <View style={{flexDirection: 'row', }}>
            <View style={{flex: 1}}>
              <TouchableOpacity
                onPress={() => this.selectedTab('expense')}
                style={{
                  justifyContent:'center',
                  alignItems:'center',
                  paddingTop: 0,
                  padding: wp(3),
                  borderBottomWidth: isExpenses ? 1 : 0.5,
                  borderColor: isExpenses ? '#FF416C' : '#BDBDBD'}}>
                <Text style={{fontSize: hp(2), color: isExpenses ? '#FF416C' : '#BDBDBD' }}>EXPENSES</Text>
                <Text style={{fontSize: hp(2), color: isExpenses ? '#FF416C' : '#BDBDBD'}}>$ {dailyTransaction.dailyTotalExpense ? dailyTransaction.dailyTotalExpense.toFixed(2) : 0}</Text>
              </TouchableOpacity>
          </View>
            <View style={{flex: 1}}>
              <TouchableOpacity
                onPress={() => this.selectedTab('income')}
                style={{
                  justifyContent:'center',
                  alignItems:'center',
                  paddingTop: 0,
                  padding: wp(3),
                  borderBottomWidth: !isExpenses ? 1 : 0.5,
                  borderColor: !isExpenses ? '#02aab0' : '#BDBDBD'}}>
                <Text style={{fontSize: hp(2), color: !isExpenses ? '#02aab0' : '#BDBDBD'}}>INCOME</Text>
                <Text style={{fontSize: hp(2), color: !isExpenses ? '#02aab0' : '#BDBDBD'}}>$ {dailyTransaction.dailyTotalIncome ? dailyTransaction.dailyTotalIncome.toFixed(2) : 0}</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{flex: 1, backgroundColor: dailyTransaction.response.length > 0 ? '#E0E0E0' : 'white'}}>
            {
              dailyTransaction.response.length > 0 ? <FlatList
                scrollEnabled={isSwiping}
                onScroll = {this.handleScroll}
                data={dailyTransaction.response}
                style={{flex: 1, flexGrow: 1}}
                renderItem={this.renderTransactionItem}
                keyExtractor={(item, index) => item+index}
                showsVerticalScrollIndicator={false}
              /> :
              <View style={{alignItems: 'center'}}>
                <LottieView
                  loop
                  autoPlay
                  speed={0.5}
                  style={{width: wp(15), height: hp(15)}}
                  source={require('@Lottie/empty_status.json')}/>
                  <Text style={{fontSize: hp(2), color: '#BDBDBD', letterSpacing: 1}}>Uh-ohhh, no record found</Text>
              </View>
            }
          </View>
        </AnimatedView>
      </LinearGradient>
    );
  }
}

// const styles = StyleSheet.create({
//   calendarHeader:
// });

mapStateToProps = (state) => {
  const { dailyTransaction, transactionDetail } = state;
  return {
    dailyTransaction,
    transactionDetail
  }
}

mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Object.assign({},
      transactionDetailAction, dailyTransactionAction), dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AllTransaction);
