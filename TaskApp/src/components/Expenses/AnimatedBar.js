import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Animated,
  Dimensions,
  Text,
  View,
  TouchableOpacity
} from 'react-native';

const DEVICE_WIDTH = DEVICE_WIDTH * 0.95;
const DEVICE_HEIGHT =DEVICE_HEIGHT * 0.3;

class AnimatedBar extends Component {
  constructor(props) {
    super(props);
    this.animateWidth = new Animated.Value(0);
    this.animateHeight = new Animated.Value(0)
  }

  componentDidMount() {
    this.animateTo(this.props.delay, this.props.value);
  }

  animateTo = (delay, value) => {
    let deviceDimenstion = this.props.vertical ? DEVICE_HEIGHT : DEVICE_WIDTH;
    let animation = this.props.vertical ? this.animateHeight : this.animateWidth;
    Animated.sequence([
      Animated.delay(delay),
      Animated.timing(animation, {
        toValue: deviceDimenstion * (value / 500), //500 is total amount spent
        duration: 500
      }),
    ]).start();
  }

  displayTextInBarChart = (value) => {
    let result = value / 500
    return result >= 0.5 ? true : false
  }

  render() {
    const {color, vertical} = this.props
    const barStyles = {
      backgroundColor: color,
      height: vertical ? this.animateHeight : 40,
      width: vertical ? DEVICE_WIDTH/7 : this.animateWidth,
      borderTopRightRadius: vertical ? 0 : 4,
      borderBottomRightRadius: vertical ? 0 : 4,
      marginLeft: vertical ? 0 : 5,
      paddingLeft: 5,
      justifyContent: 'center',
      // borderColor: vertical ? 'white' : 'transparent',
      // borderBottomColor: 'transparent',
      // borderWidth: vertical ? 1 : 0
    };
    return (
      <TouchableOpacity style={{paddingBottom: 5, justifyContent:'center', }}>
        <Animated.View style={barStyles}>
          {this.displayTextInBarChart(this.props.value) && !vertical? <Text style={{fontSize: 15, color: 'white', letterSpacing: 0.5, textAlign: 'center'}}>MYR {this.props.value}</Text> : null}
        </Animated.View>
        {
          !vertical &&
          <View style={{justifyContent: 'center', paddingLeft: 5}}>
            {!this.displayTextInBarChart(this.props.value) ? <Text style={{fontSize: 15, color: 'black', letterSpacing: 0.5}}>MYR {this.props.value}</Text> : null}
          </View>
        }
      </TouchableOpacity>
    );
  }
}


AnimatedBar.propTypes = {
  vertical: PropTypes.bool
}

AnimatedBar.defaultProps = {
  vertical: false
}

export default AnimatedBar;
