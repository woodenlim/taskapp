import React, { PureComponent } from "react";
import {
  View,
  Text,
  FlatList,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  Alert
} from "react-native";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Carousel from 'react-native-snap-carousel';
import LinearGradient from 'react-native-linear-gradient';
import {GRADIENT_COLOR} from '@Constants/constant';
import * as Animatable from 'react-native-animatable';
import {getMonthName, getDayName, getMonthInTwoDigit, getDateInTwoDigit, getDateOrdinal } from '@Utils/dateUtils';
import Modal from "react-native-modal";
import { CalendarList } from 'react-native-calendars';
import {DEVICE_WIDTH,DEVICE_HEIGHT,SHORT_DATE_NAME,FULL_DATE_NAME} from '@Constants/constant';
import CalendarModal from '@ExpensesCommonComponents/CalendarModal';
import DeleteModal from '@ExpensesCommonComponents/DeleteModal';
import TransactionDetailModal from '@ExpensesCommonComponents/TransactionDetailModal';
import hasNoRecord from '@HOC/hasNoRecord';
import * as dailyCategoryTransactionAction from "@ExpensesAction/dailyCategoryTransaction";
import * as transactionDetailAction from "@ExpensesAction/transactionDetail";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

const DefaultView = hasNoRecord(View);

class TransactionDetails extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      categoryDetail: this.props.navigation.getParam('dailyCategoryDetail', ''),
      isCalendarModalVisible: false,
      isOptionModalVisible: false,
      isDeleteModalVisible: false,
      isDateModalVisible: false,
      isDeleteTrigger: false,
      currentYear: new Date().getFullYear(),
      currentMonth: getMonthName(SHORT_DATE_NAME, new Date().getMonth()),
      currentDate: new Date().getDate(),
      currentDay: getDayName(SHORT_DATE_NAME, new Date().getDay()),
      markedTodayDate: new Date().getFullYear() + "-" + getMonthInTwoDigit((new Date().getMonth()+1)) + "-" + getDateInTwoDigit(new Date().getDate()),
      selectedDate: '',
      selectedMonth: this.props.navigation.getParam('selectedMonth', ''),
      dailyCategoryTransaction: [],
      dateList: [],
      transactionDetail: {},
      totalAmount: '',
    }
  }

  componentDidMount(){
    const {categoryDetail} = this.state;
    this.props.actions.dailyCategoryTransactionRequest({
      month: categoryDetail.transactionMonth,
      date: categoryDetail.transactionDate,
      categoryName: categoryDetail.categoryName,
      categoryIcon: categoryDetail.categoryIcon
    })
  }

  componentDidUpdate(prevProps){
    if(prevProps.dailyCategoryTransaction !== this.props.dailyCategoryTransaction){
      const dailyCategoryTransaction = this.props.dailyCategoryTransaction.response;
      const dateList = this.props.dailyCategoryTransaction.categoryTransactionDateList;
      this.setState({
        dailyCategoryTransaction: dailyCategoryTransaction.length > 0 ? dailyCategoryTransaction : [],
        dateList: dailyCategoryTransaction.length > 0 ? dateList : [],
        selectedDate: dailyCategoryTransaction.length > 0 ? dailyCategoryTransaction[0].transactionDate : '',
        totalAmount: dailyCategoryTransaction.length > 0 ? dailyCategoryTransaction[dailyCategoryTransaction.length-1].totalAmount : '',
      }, () => {
        if(dailyCategoryTransaction.length == 0 ){
          this.props.navigation.navigate('Dashboard')
        }
      })
    }

    if(prevProps.transactionDetail !== this.props.transactionDetail){
      //alert(JSON.stringify(this.props.transactionDetail))
      this.setState({
        transactionDetail: this.props.transactionDetail.response
      })
    }
  }

  toggleTransactionDetailModal = (item) => {
    this.setState({ isOptionModalVisible: !this.state.isOptionModalVisible });
    this.props.actions.transactionDetailRequest({
      id: item.id
    })
  }
  onTransactionDetailBackdropPress = () => this.setState({ isOptionModalVisible: !this.state.isOptionModalVisible });
  onTransactionDetailBackButtonPress = () => this.setState({ isOptionModalVisible: !this.state.isOptionModalVisible });
  onTransactionDetailModalHide = () => {
    if(this.state.isDeleteTrigger){
      this.setState({
        isDeleteModalVisible: !this.state.isDeleteModalVisible
      })
    }
  }

  toggleDeleteModal = () => {
    this.setState({
      isOptionModalVisible: !this.state.isOptionModalVisible, //to trigger option modal hide and show delete modal
      isDeleteTrigger: !this.state.isDeleteTrigger
    });
  }
  onDeleteBackdropPress = () => {
    this.setState({
      isDeleteModalVisible: !this.state.isDeleteModalVisible,
      isDeleteTrigger: false
    });
  }
  onDeleteBackButtonPress = () => this.setState({
    isDeleteModalVisible: !this.state.isDeleteModalVisible,
    isDeleteTrigger: false
  });

  toggleCalendarModal = () => this.setState({ isCalendarModalVisible: !this.state.isCalendarModalVisible });
  onCalendarBackdropPress = () => this.setState({ isCalendarModalVisible: !this.state.isCalendarModalVisible });
  onCalendarBackButtonPress = () => this.setState({ isCalendarModalVisible: !this.state.isCalendarModalVisible });

  toggleDateModal = () => this.setState({ isDateModalVisible: !this.state.isDateModalVisible });
  onDateBackdropPress = () => this.setState({ isDateModalVisible: !this.state.isDateModalVisible });
  onDateBackButtonPress = () => this.setState({ isDateModalVisible: !this.state.isDateModalVisible });


  renderCategoryTitle = ({ item, index }) => {
    return (
      <Animatable.View animation={'fadeInDown'} style={{width:DEVICE_WIDTH}}>
        <View style={{flex:3, flexDirection:'row'}}>
          <Text style={{flex: 1, padding: 10, fontSize: 40, textAlign:'left', letterSpacing: 5, color:'white'}}>{item.iconLabel}</Text>
          <View style={{justifyContent:'center', alignItems:'center'}}>
            <FontAwesome5 name={item.iconName} color="white" size={55} style={{opacity:0.7}}/>
          </View>
        </View>
      </Animatable.View>
    )
  }

  renderDate = ({item, index}) => {
    return (
        <Animatable.View animation={'slideInLeft'}>
          <Text style={{fontSize: 18, letterSpacing: 0.5, color: 'white', textAlign: 'center'}}>{new Date(item.date).getDate()}</Text>
          <Text style={{fontSize: 15, letterSpacing: 0.5, color: 'white', textAlign: 'center'}}>{getDayName(SHORT_DATE_NAME, new Date(item.date).getDay())}</Text>
        </Animatable.View>
    );
  }

  renderTransactionItem = ({ item, index }) => {
    let backgroundColor = item.amount ? '#F0F8FF' : "transparent";
    return (
        <TouchableOpacity disabled={item.amount?false:true}
          style={{flex: 1, backgroundColor: '#F0F8FF', borderRadius: 10, marginBottom: 10}}
          onPress={()=>this.toggleTransactionDetailModal(item)}>

            <View style={{flexDirection:'row', padding: 15}}>
              <FontAwesome5 name={item.categoryIcon} color="black" size={20} />
              <View style={{paddingLeft: 15, flex: 1}}>
                <Text style={{fontSize: 15, color:'black'}}>{item.notes}</Text>
              </View>
              <View style={{paddingLeft: 15, justifyContent:'flex-end'}}>
                <Text style={{fontSize: 15, color:'black'}}>$ {item.amount}</Text>
              </View>
            </View>
        </TouchableOpacity>
    )
  }

  dateRequest = (item) => {
    this.setState({selectedDate: item.transactionDate})
    this.props.actions.dailyCategoryTransactionRequest({
      month: item.transactionMonth,
      date: item.transactionDate,
      categoryName: item.categoryName,
      categoryIcon: item.categoryIcon
    })
    this.toggleDateModal()
  }

  renderDateList = ({ item, index }) => {
    //alert(JSON.stringify(item))
    return (
      <TouchableOpacity onPress={()=>this.dateRequest(item)} style={{flexDirection:'row', alignItems: 'center'}}>
        <Text style={{fontSize: 15, textAlign:'left', letterSpacing: 2, color:'black', paddingTop: 30, paddingLeft: 20}}>
          {item.transactionDate}
        </Text>
      </TouchableOpacity>
    )
  }

  formatSelectedDateDisplay = (selectedDate) => {
    let currentDate = new Date().setHours(0, 0, 0, 0);
    let displayDay = new Date(selectedDate).setHours(0, 0, 0, 0) == currentDate ? "TODAY" : getDayName(FULL_DATE_NAME, (new Date(selectedDate).getDay()));
    let displayMonth = new Date(selectedDate).getMonth();
    let displayDate = new Date(selectedDate).getDate()
    return displayDay + ", " + getMonthName(FULL_DATE_NAME, displayMonth) + " " + displayDate;
  }

  onDayPress = (day) => {
    this.setState({
      selectedDate: day.dateString,
      isCalendarModalVisible: !this.state.isCalendarModalVisible})
  }

  navigateTo = () => {
    this.setState({
      isOptionModalVisible: !this.state.isOptionModalVisible
    }, () => {
      this.props.navigation.navigate('NewExpenses')
    })
  }

  // onMomentumScrollEnd = (e) => {
  //   let contentOffset = e.nativeEvent.contentOffset;
  //   let viewSize = e.nativeEvent.layoutMeasurement;
  //
  //   // Divide the horizontal offset by the width of the view to see which page is visible
  //   let pageNum = Math.floor(contentOffset.x / viewSize.width);
  //   this.setState({
  //     selectedDate: pageNum,
  //     categoryTitle: this.state.flatListData[pageNum].iconLabel
  //   })
  // }

  render() {
    const {flatListData, selectedDate, carouselData, categoryTitle, categoryIconName, transactionData,
           currentYear,currentMonth,currentDate,currentDay, markedTodayDate, categoryDetail,
           dailyCategoryTransaction, dateList, isDateModalVisible, totalAmount} = this.state
    return (
      <LinearGradient
        colors={GRADIENT_COLOR} style={{flex: 1}}>

        <CalendarModal
          toggleModal={this.toggleCalendarModal}
          onBackdropPress={this.onCalendarBackdropPress}
          onBackButtonPress={this.onCalendarBackButtonPress}
          showModal={this.state.isCalendarModalVisible}
          onDayPress={this.onDayPress}
        />

        <DeleteModal
          toggleModal={this.toggleDeleteModal}
          onBackdropPress={this.onDeleteBackdropPress}
          onBackButtonPress={this.onDeleteBackButtonPress}
          showModal={this.state.isDeleteModalVisible}
          transactionDetail={this.state.transactionDetail}
        />

        <TransactionDetailModal
          toggleModal={this.onTransactionDetailBackdropPress}
          onBackdropPress={this.onTransactionDetailBackdropPress}
          onBackButtonPress={this.onTransactionDetailBackButtonPress}
          showModal={this.state.isOptionModalVisible}
          toggleDeleteModal={this.toggleDeleteModal}
          onModalHide={this.onTransactionDetailModalHide}
          transactionDetail={this.state.transactionDetail}
          {...this.props}
        />

        <Modal
          animationIn={'zoomIn'}
          animationOut={'zoomOut'}
          onBackdropPress={this.onDateBackdropPress}
          onBackButtonPress={this.onDateBackButtonPress}
          isVisible={isDateModalVisible}
          style={{flex: 1, alignSelf: 'center'}}>
          <View style={{
            backgroundColor: 'white',
            borderRadius: 10,
            borderColor: 'white',
            justifyContent: 'center',
            flex: 0.7,
            width: DEVICE_WIDTH*0.7
          }}>
          <View style={{flex: 1}}>
            <FlatList
              ref={(ref) => { this.dateList = ref }}
              data={dateList}
              renderItem={this.renderDateList}
              keyExtractor={(item, index) => item+index}
              showsVerticalScrollIndicator={false}
            />
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center'}}>
              <TouchableOpacity onPress={this.toggleDateModal}>
                <Text style={{fontSize: 12, padding: 20, paddingTop: 0}}>CANCEL</Text>
              </TouchableOpacity>
          </View>
          </View>
        </Modal>

        <View style={{flex:1.5}}>
          { selectedDate !== ''  && <Animatable.View animation={'fadeInDown'} style={{flex: 0.5, paddingLeft: 20, paddingBottom: 0}}>
            <TouchableOpacity onPress={this.toggleDateModal} style={{flexDirection:'row', alignItems: 'center'}}>
              <Text style={{fontSize: 20, textAlign:'left', letterSpacing: 5, color:'white', paddingRight: 10, fontWeight:'bold'}}>
                {selectedDate}
              </Text>
              <FontAwesome5 name={'angle-down'} color="white" size={15} style={{flex:1}}/>
            </TouchableOpacity>
          </Animatable.View>}

          <Animatable.View animation={'fadeInDown'} style={{flex: 1, flexDirection:'row', justifyContent:'flex-start', alignItems:'center', padding: 10, paddingLeft: 20, paddingBottom: 10}}>
            {/**<FontAwesome5 name={categoryIconName} color="black" size={30} style={{paddingRight:5}}/>**/}
            <Text style={{fontSize: 25, textAlign:'left', letterSpacing: 5, color:'white', }}>{categoryDetail.categoryName}</Text>
            {/** <FontAwesome5 name={categoryDetail.categoryIcon} color="white" size={25} style={{flex:1, }}/> **/}
          </Animatable.View>
        </View>

        <View style={{flex:9}}>
          <Animatable.View animation={'slideInUp'} style={{flex: 1, paddingLeft: 20, paddingRight: 20}}>
            <DefaultView hasTransaction={dailyCategoryTransaction.length > 0 ? true : false} size={0.4} style={{flex:1}}>
              <View style={{padding: 10, marginTop:0, marginBottom: 10, flexDirection:'row', borderRadius: 10, backgroundColor:'#E6E6FA'}}>
                <Text style={{fontSize: 15, textAlign:'left', color:'black', letterSpacing: 1}}>{categoryDetail.transactionType == 'expense' ? 'SPENT' : 'EARNED'}</Text>
                <Text style={{flex:1, fontSize: 15, textAlign:'right', color:'black', letterSpacing: 1}}>$ {totalAmount}</Text>
              </View>
              <FlatList
                data={dailyCategoryTransaction}
                style={{flex: 1}}
                renderItem={this.renderTransactionItem}
                keyExtractor={(item, index) => item+index}
                showsVerticalScrollIndicator={false}
              />
            </DefaultView>
          </Animatable.View>
        </View>

        <TouchableOpacity
          onPress={()=>this.props.navigation.navigate('NewExpenses', {
            categoryIcon: categoryDetail.categoryIcon,
            categoryName: categoryDetail.categoryName,
            categoryType: categoryDetail.transactionType,
            transactionDate: selectedDate
          })}
          style={{marginBottom: 10, position:'absolute', bottom: 0, justifyContent:'center', alignSelf:'center', backgroundColor:'white', borderRadius: DEVICE_WIDTH/2, height: DEVICE_WIDTH*0.15, width: DEVICE_WIDTH*0.15}}>
          <Animatable.View animation={'zoomIn'} >
            <FontAwesome5 name={'plus'} color= 'black' size={25} style={{alignSelf:'center'}} />
          </Animatable.View>
        </TouchableOpacity>

      </LinearGradient>
    );
  }
}

mapStateToProps = (state) => {
  const { transactionDetail, dailyCategoryTransaction } = state;
  return {
    transactionDetail,
    dailyCategoryTransaction
  }
}

mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Object.assign({},
      dailyCategoryTransactionAction,
      transactionDetailAction), dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TransactionDetails);
