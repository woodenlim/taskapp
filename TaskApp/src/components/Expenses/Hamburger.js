import React, { PureComponent } from "react";
import {
  View,
  Text,
  SectionList,
  Switch,
  TouchableOpacity
} from "react-native";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Modal from "react-native-modal";
import LinearGradient from 'react-native-linear-gradient';
import {GRADIENT_COLOR,GRADIENT_START,GRADIENT_END} from '@Constants/constant';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '@Utils/scalingUtils';
import * as userSettingAction from "@ExpensesAction/userSetting";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import FingerprintScanner from 'react-native-fingerprint-scanner';
import firebase from 'react-native-firebase';

class Hamburger extends PureComponent {
  constructor(props) {
    super(props);
    const { userSetting } = this.props;
    this.state = {
      isNotificationEnable: userSetting.isNotificationEnable,
      isPasscodeEnable: userSetting.passcodeSetting.isEnable,
      isFingerprintEnable: userSetting.isFingerprintEnable || false,
      isFingerprintModalVisible: false,
      settingMenu: [
        {icon: 'bell', label: 'Notify Me'},
        {icon: 'key', label: 'Passcode'}],
      fingerprintModalTitle: 'Set up Passcode',
      fingerprintModalDescription: "You'll use Passcode to verify your identity when you can't use your fingerprint.",
    }
  }

  // static navigationOptions = {
  //   // headerTitle instead of title
  //   headerTitle: <Text>Hi</Text>,
  // };

  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: (<Text>Hi</Text>),
    };
  };

  componentDidMount(){
    FingerprintScanner.isSensorAvailable()
      .then(biometryType => {
        this.setState({
          settingMenu: [...this.state.settingMenu, {
            icon: 'fingerprint', label: 'Fingerprint'
          }]
        })
      })
      .catch(error => {
        //no fingerprint
      });

    this._subscribe = this.props.navigation.addListener('didFocus', () => {
      this.setState({
        isNotificationEnable: this.props.userSetting.isNotificationEnable
      })
    });
  }

  componentWillUnmount() {
    FingerprintScanner.release();
  }

  componentDidUpdate(prevProps){
    if(prevProps.userSetting.isFingerprintEnable !== this.props.userSetting.isFingerprintEnable){
      this.setState({
        isFingerprintEnable: this.props.userSetting.isFingerprintEnable
      })
    }

    if(prevProps.userSetting.passcodeSetting !== this.props.userSetting.passcodeSetting){
      this.setState({
        isPasscodeEnable: this.props.userSetting.passcodeSetting.isEnable,
        userPasscode: this.props.userSetting.passcodeSetting.value,
      })
    }
  }

  toggleFingerprintModal = () => {
    this.setState({
      isFingerprintModalVisible: !this.state.isFingerprintModalVisible
    })
  }

  updateUserSettingProps = (isNotificationEnable) => {
    const { userSetting } = this.props
    this.props.actions.userSettingSuccess({
      isNotificationEnable: isNotificationEnable
    })
  }

  enableScheduleNotification = () => {
    const { remindTimeList } = this.props
    remindTimeList.response.map((data, index) => {
      let scheduleTime =
        new firebase.notifications.Notification()
        .setNotificationId(data.notificationId)
        .setTitle('B Y B')
        //.setSubtitle('BYB')
        //.setBody('Remember to record your transaction.')
        .setBody('Do not forget to add your transaction.')
        .android.setChannelId('bybChannelId') // e.g. the id you chose above
        .android.setSmallIcon('notification_icon') // create this icon in Android Studio
        .android.setLargeIcon('@mipmap/ic_launcher') // create this icon in Android Studio
        .android.setColor('#b06ab3') // you can set a color here
        .android.setPriority(firebase.notifications.Android.Priority.High);

        const date = new Date();
        let timeSplit = data.time.split(':')
        date.setHours(timeSplit[0],timeSplit[1],0,0)

        firebase.notifications().scheduleNotification(scheduleTime, {
          fireDate: date.getTime(),
          repeatInterval: 'day'
        })
    })
  }

  toggleSwitch = (type) => {
    const {switchValue, isNotificationEnable, isPasscodeEnable, isFingerprintEnable, isFingerprintModalVisible} = this.state
    const { remindTimeList, userSetting } = this.props
    switch (type) {
      case 'notification':
        if(remindTimeList.response.length == 0){
          this.navigateTo('Remind Me')
        }else{
          if(isNotificationEnable){
            //alert('Cancel ALL Notification')
            this.setState({isNotificationEnable: !isNotificationEnable})
            this.updateUserSettingProps(!isNotificationEnable)
            firebase.notifications().cancelAllNotifications()
          }else{
            //alert('Re-enable Notification')
            this.setState({isNotificationEnable: !isNotificationEnable})
            this.updateUserSettingProps(!isNotificationEnable)
            this.enableScheduleNotification()
          }

        }

        break;
      case 'passcode':
        if(isPasscodeEnable && isFingerprintEnable){
          this.setState({
            fingerprintModalTitle: 'Remove Authentication',
            fingerprintModalDescription: "Fingerprint authentication will be remove also, are you sure?",
            isFingerprintModalVisible: !isFingerprintModalVisible
          })
        }else if(isPasscodeEnable){
          this.setState({
            fingerprintModalTitle: 'Disable Passcode',
            fingerprintModalDescription: "Are you sure want to disable Passcode Verification?",
            isFingerprintModalVisible: !isFingerprintModalVisible
          })
        }else{//Enable Passcode
          this.navigateTo('Passcode')
        }
        break;
      case 'fingerprint':
        if(!isPasscodeEnable){//Set Up All
          this.setState({
            fingerprintModalTitle: 'Set up Passcode',
            fingerprintModalDescription: "You'll use Passcode to verify your identity when you can't use your fingerprint.",
          }, () => {
            this.toggleFingerprintModal()
          })
        }else{//Disable or Enable Fingerprint
          this.navigateTo('Passcode', 'fingerprint')
        }
        break;
      default:

    }
  }

  settingHeader = ({section: {title}}) => {
    return (
      <View style={{padding: wp(4), paddingTop: 0, paddingBottom: hp(3)}}>
        <Text style={{fontSize: hp(3), letterSpacing:3, fontWeight: 'bold', color:'white'}}>{title}</Text>
      </View>
    )
  }

  navigateTo = (item, param) => {
    switch (item) {
      case 'Notify Me':
        this.props.navigation.navigate('Remind')
        break;
      case 'Passcode':
        this.props.navigation.navigate('Passcode', { action: param})
        break;
      default:

    }
  }

  renderItem = ({item, index, section}) => {
    let renderOption
    {
      switch(item.label) {
        case 'Notify Me':
          renderOption = (
            <Switch
              onValueChange={()=>this.toggleSwitch('notification')}
              value={this.state.isNotificationEnable}
            />
          )
          break;
        case 'Passcode':
          renderOption = (
            <Switch
              onValueChange={()=>this.toggleSwitch('passcode')}
              value={this.state.isPasscodeEnable}
            />
          )
          break;
        case 'Fingerprint':
          renderOption = (
            <Switch
              onValueChange={()=>this.toggleSwitch('fingerprint')}
              value={this.state.isFingerprintEnable}
            />
          )
          break;
        default:
      }
    }
    return (
      <TouchableOpacity
        disabled={item.label == 'Notify Me' ? false : true}
        onPress={()=>this.navigateTo(item.label)}
        style={{
          flex: 1,
          flexDirection: 'row',
          paddingTop: 0,
          paddingBottom: hp(4),
          paddingLeft: wp(6),
          paddingRight: wp(4)
        }}>
        <View style={{alignItems: 'center', justifyContent: 'center'}}>
          <FontAwesome5 name={item.icon} color="white" size={hp(3)}/>
        </View>
        <View style={{flex: 1, alignItems: 'flex-start', justifyContent: 'center', paddingLeft: wp(6)}}>
          <Text style={{fontSize: hp(2.5), letterSpacing: 3, color:'white'}}>{item.label}</Text>
        </View>
        {renderOption}
      </TouchableOpacity>
    )
  }

  render() {
    const { settingMenu, isFingerprintEnable, isPasscodeEnable,
            isFingerprintModalVisible, fingerprintModalTitle, fingerprintModalDescription } = this.state
    return (
      <LinearGradient
        start={GRADIENT_START} end={GRADIENT_END}
        colors={GRADIENT_COLOR} style={{flex: 1}}>
        <Modal
          animationIn={'zoomIn'}
          animationOut={'zoomOut'}
          onBackdropPress={this.toggleFingerprintModal}
          onBackButtonPress={this.toggleFingerprintModal}
          isVisible={isFingerprintModalVisible}>
          <View style={{
            flex: 0.3,
            backgroundColor: 'white',
            borderRadius: wp(3),
            borderColor: 'white'}}>
            <View style={{flex: 1}}>
              <Text style={{
                fontSize: hp(3),
                letterSpacing: 1,
                color:'black',
                paddingTop: hp(2),
                paddingBottom: hp(2),
                paddingLeft: wp(4),
                paddingRight: wp(4),
              }}>{fingerprintModalTitle}</Text>
              <Text style={{fontSize: hp(2), letterSpacing: 1, color:'black', paddingBottom: hp(3), paddingLeft: wp(4), paddingRight: wp(4)}}>{fingerprintModalDescription}</Text>
              <View style={{flex: 1, flexDirection: 'row', alignItems:'flex-end', justifyContent:'flex-end'}}>
                <TouchableOpacity
                  onPress={this.toggleFingerprintModal}
                  style={{
                    flex:1,
                    backgroundColor:'#E0E0E0',
                    borderRadius: hp(1),
                    justifyContent:'center',
                    alignItems:'center',
                    marginTop: hp(3),
                    marginBottom: hp(2),
                    marginLeft: wp(4),
                    marginRight: wp(2)}}>
                  <Text style={{fontSize: hp(2), letterSpacing: 1, color:'black', paddingTop: hp(1), paddingBottom:hp(1)}}>CANCEL</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    flex:1,
                    backgroundColor:'#48D563',
                    borderRadius: hp(1),
                    justifyContent:'center',
                    alignItems:'center',
                    marginTop: hp(3),
                    marginBottom: hp(2),
                    marginLeft: wp(2),
                    marginRight: wp(4)}}
                  onPress={()=>{
                    this.setState({
                      isFingerprintModalVisible: !isFingerprintModalVisible
                    }, () => {
                      if(!isPasscodeEnable){//Disable Passcode
                        this.navigateTo('Passcode', 'passcode')
                      }if(isPasscodeEnable && !isFingerprintEnable){
                        this.navigateTo('Passcode', 'passcode')
                      }else if(isPasscodeEnable && isFingerprintEnable){//Disable Passcode and Fingerprint
                        this.navigateTo('Passcode', 'removeAll')//Remove ALL Authentication
                      }else if(!isPasscodeEnable && !isFingerprintEnable){
                        this.navigateTo('Passcode', 'setUpAll')//set up new passcode purpose
                      }
                    })
                  }}>
                  <Text style={{fontSize: hp(2), letterSpacing: 1, color:'white', paddingTop: hp(1), paddingBottom:hp(1)}}>CONFIRM</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
        <SectionList
          renderItem={this.renderItem}
          renderSectionHeader={this.settingHeader}
          sections={[
            {
              title: 'Settings',
              data: settingMenu
            },
          ]}
          keyExtractor={(item, index) => item + index}
        />
      </LinearGradient>
    );
  }
}

mapStateToProps = (state) => {
  const { userSetting, remindTimeList } = state;
  return {
    userSetting,
    remindTimeList
  }
}

mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Object.assign({},userSettingAction), dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Hamburger);
