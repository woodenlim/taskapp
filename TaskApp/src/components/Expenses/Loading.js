import React, { PureComponent } from "react";
import {
  View,
  Text,
  FlatList,
  Switch,
  TouchableOpacity
} from "react-native";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import LinearGradient from 'react-native-linear-gradient';
import {GRADIENT_COLOR,DEVICE_HEIGHT} from '@Constants/constant';
import * as Animatable from 'react-native-animatable';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import firebase from 'react-native-firebase';
import FingerprintScanner from 'react-native-fingerprint-scanner';
import * as userSettingAction from "@ExpensesAction/userSetting";

class Loading extends PureComponent {
  constructor(props) {
    super(props);
    console.log('Loading', this.props);
    const { userSetting } = this.props
    if(userSetting.isFingerprintEnable){
      FingerprintScanner.isSensorAvailable()
        .then(biometryType => {
          this.props.navigation.navigate('Fingerprint');
        })
        .catch(error => {
          //no fingerprint
          this.props.navigation.navigate('Passcode');
          this.props.actions.userSettingSuccess({
            isFingerprintEnable: false
          })
        });
    }else if(userSetting.passcodeSetting.isEnable){
      this.props.navigation.navigate('Passcode');
    }else{
      this.props.navigation.navigate('App');
    }
  }

  componentDidMount(){
    //const all = await firebase.notifications().getScheduledNotifications()
    //alert(JSON.stringify(all))
    // Build and create channel
    const channel = new firebase.notifications.Android.Channel('bybChannelId', 'BYB Channel', firebase.notifications.Android.Importance.Max)
      .setDescription('BYB Remind Notification');
    firebase.notifications().android.createChannel(channel);

    this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification) => {
        // Process your notification as required
        // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
        //alert('onNotificationDisplayed')
    });

    this.notificationListener = firebase.notifications().onNotification((notification) => {
        // Process your notification as required
        // Display the notifications
        //alert('onNotification')
        firebase.notifications()
          .displayNotification(notification)
          .catch(err => console.error(err));
    });
  }

  render() {
    return null;
  }
}

mapStateToProps = (state) => {
  const { userSetting, remindTimeList } = state;
  return {
    userSetting,
    remindTimeList
  }
}

mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Object.assign({},userSettingAction), dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Loading);
