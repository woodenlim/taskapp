import React, { PureComponent } from "react";
import {
  View,
  Text,
  FlatList,
  Switch,
  TouchableOpacity,
  Alert
} from "react-native";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from 'react-native-linear-gradient';
import {GRADIENT_COLOR,DEVICE_HEIGHT} from '@Constants/constant';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from '@Utils/scalingUtils';
import * as userSettingAction from "@ExpensesAction/userSetting";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Animatable from 'react-native-animatable';

class Passcode extends PureComponent {
  constructor(props) {
    super(props);
    const { userSetting } = this.props;
    this.state = {
      keyboardDisplayText: [
        {value: '1'},{value: '2'},{value: '3'},
        {value: '4'},{value: '5'},{value: '6'},
        {value: '7'},{value: '8'},{value: '9'},
        {value: ''},{value: '0'},{value: 'backspace'}
      ],
      isFingerprintEnable: userSetting.isFingerprintEnable,
      isPasscodeEnable: userSetting.passcodeSetting.isEnable,
      userPasscode: userSetting.passcodeSetting.value,
      passcodeInput: userSetting.passcodeSetting.value,
      isNewPasscode: false,
      tempPasscode: '',
      input: '',
      passcodeAction: this.props.navigation.getParam('action', ''),
      isWrongPasscode: false,
      shakeAnimation: ''
    }
  }

  onAnimationEnd = () => {
    this.setState({
      shakeAnimation: '',
    })
  }

  selectKeyboardInputValue = (value) => {
    let newInputValue = this.state.input + ((value !== 'times-circle' && value !== 'backspace') ? value : '');
    const { navigate } = this.props.navigation;
    const { isPasscodeEnable, isFingerprintEnable, passcodeInput, isNewPasscode, tempPasscode, passcodeAction, userPasscode } = this.state;
    switch (value) {
      case 'backspace':
        this.setState({
          input: newInputValue.slice(0, newInputValue.length-1)
        })
        break;
      default:
        this.setState({input: newInputValue})
        break;
    }

    if(newInputValue.length == 4){//Auto Submit
      if(isPasscodeEnable && !passcodeAction){ //Initial Route, Verify Passcode
        if(passcodeInput == newInputValue){
          //success and go to dashboard
          navigate('Dashboard')
        }else{
          this.setState({
            isWrongPasscode: true,
            input: '',
            shakeAnimation: 'shake'
          })
        }
      }else if(isPasscodeEnable && passcodeAction){//Disable Passcode or Fingerprint based on Action
        if(userPasscode == newInputValue){
          this.props.actions.userSettingSuccess({
            isPasscodeEnable: passcodeAction == 'fingerprint' ? isPasscodeEnable : passcodeAction == 'removeAll' || passcodeAction == 'passcode' ? !isPasscodeEnable : isPasscodeEnable,
            userPasscode: passcodeAction == 'fingerprint' ? userPasscode : passcodeAction == 'removeAll' || passcodeAction == 'passcode' ? !userPasscode : userPasscode,
            isFingerprintEnable: passcodeAction == 'fingerprint' || passcodeAction == 'removeAll' ? !isFingerprintEnable : isFingerprintEnable, //to check whether enable fingerprint or not
          })
          this.setState({
            isWrongPasscode: false,
          }, () => navigate('Dashboard'))
        }else{
          this.setState({
            isWrongPasscode: true,
            input: '',
            shakeAnimation: 'shake'
          })
        }
      }else{ //Set new passcode
        if(isNewPasscode){ //Check user is it in Confirm Passcode Screen then redirect
          if(tempPasscode !== newInputValue){
            this.setState({
              input: '',
              shakeAnimation: 'shake'
            })
            return;
          }else{
            this.props.actions.userSettingSuccess({
              isPasscodeEnable: true,
              userPasscode: tempPasscode,
              isFingerprintEnable: passcodeAction == 'setUpAll' ? !isFingerprintEnable : isFingerprintEnable, //to check whether enable fingerprint or not
            })

            this.setState({
              isNewPasscode: !isNewPasscode,
              tempPasscode: '',
              input: ''
            })
            navigate('Dashboard')
            return;
          }
        }

        this.setState({
          isNewPasscode: !isNewPasscode,
          tempPasscode: newInputValue,
          input: '',
        })
      }
      return;
    }
  }

  renderNumber = ({item, index}) => {
    let renderComponent
    if(item.value == 'backspace'){
      renderComponent = <FontAwesome5 name={item.value} size={hp(4)} color='white'/>
    }else{
      renderComponent = <Text style={{fontSize: hp(4), color:'white'}}>{item.value}</Text>
    }
    return (
      <TouchableOpacity style={{flex: 1 , height: hp(10), alignItems: 'center', justifyContent: 'center'}}
        onPress={()=>this.selectKeyboardInputValue(item.value)}>
        {renderComponent}
      </TouchableOpacity>
    )
  }


  render() {
    const { input, isNewPasscode, isWrongPasscode, shakeAnimation } = this.state
    return (
      <LinearGradient
        colors={GRADIENT_COLOR} style={{flex: 1}}>
        <View style={{flex:2, alignItems: 'center', justifyContent: 'center'}}>
          <Text style={{fontSize: hp(3), color:'white', fontWeight: 'bold', letterSpacing: 2}}>
            {isWrongPasscode ? 'TRY AGAIN' : isNewPasscode ? 'CONFIRM PASSCODE' : 'ENTER PASSCODE'}
          </Text>
          <Text style={{fontSize: hp(2), color:'rgba(255,255,255,0.6)', fontWeight: 'bold', letterSpacing: 1}}>
            Please enter your passcode
          </Text>
        </View>
        <Animatable.View
          onAnimationEnd={this.onAnimationEnd}
          animation={shakeAnimation}
          style={{flex:2, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
          <FontAwesome5 name='circle' solid={input.length >= 1 ? true : false} size={hp(2)} color='white' style={{padding: wp(3)}}/>
          <FontAwesome5 name='circle' solid={input.length >= 2 ? true : false} size={hp(2)} color='white' style={{padding: wp(3)}}/>
          <FontAwesome5 name='circle' solid={input.length >= 3 ? true : false} size={hp(2)} color='white' style={{padding: wp(3)}}/>
          <FontAwesome5 name='circle' solid={input.length >= 4 ? true : false} size={hp(2)} color='white' style={{padding: wp(3)}}/>
        </Animatable.View>
        <View style={{flex: 8}}>
          <FlatList
            data={this.state.keyboardDisplayText}
            renderItem={this.renderNumber}
            style={{flex:1}}
            numColumns={3}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item, index) => item+index}/>
        </View>
      </LinearGradient>
    );
  }
}

mapStateToProps = (state) => {
  const { userSetting } = state;
  return {
    userSetting,
  }
}

mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Object.assign({},userSettingAction), dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Passcode);
