const router = require('express').Router();
const Task = require('../models/Task.js');
const Member = require('../models/Member.js')

const INITIAL_RESPONSE = {
  message: "",
  status: "",
  response: {
    data:[]
  }
}

//Get ALL Task List
router.get("/", (req, res) => {
  Task.find({}).sort({dueDate: 1}).
  then(data => {
    if(data.length > 0){
      INITIAL_RESPONSE.message = "Get Task List Successfully",
      INITIAL_RESPONSE.status = "success",
      INITIAL_RESPONSE.response.data = data
    }
    res.send(INITIAL_RESPONSE)
  })
})

//Create Task
router.post("/", (req, res) => {
  let memberName = ""
  let memberEmail = ""
  let body = {
    name: req.body.name,
    description: req.body.description,
    dueDate: req.body.dueDate
  }
  if(req.body.memberId !== ""){
    Member.findById({_id : req.body.memberId})
	   .then(data => {
       body.member = [{
          _id: data._id,
          name: data.name,
          email: data.email
       }]
       console.log(body);
       Task.create(body).then(data => {
         if(data){
           INITIAL_RESPONSE.message = "Create Task Successfully",
           INITIAL_RESPONSE.status = "success",
           INITIAL_RESPONSE.response.data = data
         }
         res.send(INITIAL_RESPONSE)
      })
     })
  }else{
    Task.create(body).then(data => {
      if(data){
        INITIAL_RESPONSE.message = "Create Task Successfully",
        INITIAL_RESPONSE.status = "success",
        INITIAL_RESPONSE.response.data = data
      }
      res.send(INITIAL_RESPONSE)
     })
  }
})

//Update Task Details (PATCH or PUT)
router.put("/:id", (req, res) => {

  let body = {
    name: req.body.name,
    description: req.body.description,
    dueDate: req.body.dueDate
  }

  if(req.body.memberId !== ""){
    Member.findById({_id : req.body.memberId})
	   .then(data => {
       console.log(data);
       body.member = [{
          _id: data._id,
          name: data.name,
          email: data.email
       }]
       console.log(body);
       Task.findByIdAndUpdate({
         _id: req.params.id
       },{
         $set: {
           name: req.body.name,
           description: req.body.description,
           dueDate: req.body.dueDate,
           member: body.member
         }
       }).then((data)=>{
         // Task.findById({_id : req.params.id})
     	  //  .then(data => res.send(data))
         if(data){
           INITIAL_RESPONSE.message = "Update Task Successfully",
           INITIAL_RESPONSE.status = "success",
           INITIAL_RESPONSE.response.data = data
         }
         res.send(INITIAL_RESPONSE)
       })
     })
  }else{
    Task.findByIdAndUpdate({
      _id: req.params.id
    },{
      $set: {
        name: req.body.name,
        description: req.body.description,
        dueDate: req.body.dueDate,
        member: [{
          name: '',
          email: ''
        }]
      }
    }).then((data)=>{
      if(data){
        INITIAL_RESPONSE.message = "Update Task Successfully",
        INITIAL_RESPONSE.status = "success",
        INITIAL_RESPONSE.response.data = data
      }
      res.send(INITIAL_RESPONSE)
    })
  }
})

//Delete Task
router.delete ("/:id", (req, res) => {
  const id = req.params.id
	Task.findByIdAndRemove(id)
	.then(data => {
    if(data){
      INITIAL_RESPONSE.message = "Delete Task Successfully",
      INITIAL_RESPONSE.status = "success",
      INITIAL_RESPONSE.response.data = data
    }
    res.send(INITIAL_RESPONSE)
  })
})

//Assigned Task
router.put("/:taskId/member/:memberId", (req, res) => {
  const taskId = req.params.taskId
  const memberId = req.params.memberId
  Member.findById({_id: memberId}).
  then(data => {
    console.log(data);
    // Task.findByIdAndUpdate({
    //   _id: taskId
    // }, {
    //   $push: {
    //     member: [{
    //       name: data.name,
    //       email: data.email
    //     }]
    //   }
    // }).then(data => {
    //   res.send('Task Assigned')
    // })
  })

})

module.exports = router;
