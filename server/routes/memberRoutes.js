const router = require('express').Router();
const Member = require('../models/Member.js')

const INITIAL_RESPONSE = {
  message: "",
  status: "",
  response: {
    data: []
  }
}

//Get ALL Member List
router.get("/", (req, res) => {
  Member.find({}).sort({name: 1}).
  then(data => {
    if(data.length > 0){
      INITIAL_RESPONSE.message = "Get Member List Successfully",
      INITIAL_RESPONSE.status = "success",
      INITIAL_RESPONSE.response.data = data
    }
    res.send(INITIAL_RESPONSE)
  })
})

//Create Member
router.post("/", (req, res) => {
 Member.create(req.body).
 then(data => {
   if(data){
     INITIAL_RESPONSE.message = "Created Member Successfully",
     INITIAL_RESPONSE.status = "success",
     INITIAL_RESPONSE.response.data = data
   }
   res.send(INITIAL_RESPONSE)
 })
})

//Update Member
router.put("/:id", (req, res) => {
  Member.findByIdAndUpdate({
    _id: req.params.id
  },{
    $set: {
      name: req.body.name,
      email: req.body.email
    }
  }).then((data)=>{
    // Member.findById({_id : req.params.id})
	  //  .then(data => res.send(data))
    if(data){
      INITIAL_RESPONSE.message = "Update Member Successfully",
      INITIAL_RESPONSE.status = "success",
      INITIAL_RESPONSE.response.data = data
    }
    res.send(INITIAL_RESPONSE)
  })

})

//Delete Member
router.delete ("/:id", (req, res) => {
  const id = req.params.id
	Member.findByIdAndRemove(id)
	.then(data => {
    if(data){
      INITIAL_RESPONSE.message = "Delete Member Successfully",
      INITIAL_RESPONSE.status = "success",
      INITIAL_RESPONSE.response.data = data
    }
    res.send(INITIAL_RESPONSE)
  })
})

module.exports = router;
