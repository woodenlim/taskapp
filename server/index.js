const express = require ('express');
const cors = require('cors')
// Routes File
const taskRoutes = require ("./routes/taskRoutes");
const memberRoutes = require ("./routes/memberRoutes");
const bodyParser = require ("body-parser");
const mongoose = require ('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/task",{useNewUrlParser: true})
.then(() => console.log("Connected to Mongo"))
.catch(err => console.log(err))

const app = express ();

// Bodyparser Middleware
app.use(bodyParser.json());
// Routes Middleware
app.use(cors())
app.use("/task", taskRoutes)
app.use("/member", memberRoutes)

// Port
const port = 5000 || process.env.PORT;
app.listen(port, () =>
  console.log(`Listen to the port of ${port}`)
)
