const mongoose = require ("mongoose");
const Schema = mongoose.Schema;
const MemberSchema = new Schema ({
 name: {
   type: String,
   required: true
 },
 email: {
   type: String,
   required: true
 }
})
const Member = mongoose.model("member", MemberSchema);
module.exports = Member;
